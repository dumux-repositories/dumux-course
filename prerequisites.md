# Prerequisites for the DuMu<sup>x</sup> course

For mastering the DuMu<sup>x</sup> course, we recommend the following prerequisites:

- **CLI:** Knowledge of basic Unix shell commands and interaction with a corresponding terminal is essential for the installation process as well as the compiliation and execution of the exercise programs. We can recommend this [Software Carpentry course](https://swcarpentry.github.io/shell-novice/) for learning the basics and this [cheat sheet](https://phoenixnap.com/kb/wp-content/uploads/2022/11/linuxCommandsAllUsersShouldKnow.pdf) for remembering them.

- **C++:** DuMu<sup>x</sup> is a module on top of the C++ code base DUNE and itself contains several more advanced C++ language features. We recommend to be familiar with C++ templates and can recommend the following educational resources:
 
  In general we recommend most CppCon Back to Basics videos, but here is a list of taylored suggestions for this course.
  - OOP:
    - https://www.youtube.com/watch?v=32tDTD9UJCE
  - Compiling and Linking:
    - https://www.youtube.com/watch?v=cpkDQaYttR4
  - Classes:
    - https://www.youtube.com/watch?v=motLOioLJfg&t=2685s
    - https://www.youtube.com/watch?v=O65lEiYkkbc-
  - Functions:
    - https://www.youtube.com/watch?v=CpHX1Du5R0Q&list=PLHTh1InhhwT7gQEuYznhhvAYTel0qzl72&index=45
  - Templates:
    - https://www.youtube.com/watch?v=XN319NYEOcE&list=PLHTh1InhhwT4TJaHBVWzvBOYhp27UO7mI&index=13
    - https://www.youtube.com/watch?v=2Y9XbltAfXs&list=PLHTh1InhhwT4TJaHBVWzvBOYhp27UO7mI&index=14
  - Lambdas:
    - https://www.youtube.com/watch?v=IgNUBw3vcO4&list=PLHTh1InhhwT4TJaHBVWzvBOYhp27UO7mI&index=6
  - Smart Pointers:
    - https://www.youtube.com/watch?v=YokY6HzLkXs
  - Memory:
    - https://www.youtube.com/watch?v=rqVWj0aVSxg
  - STL:
    - https://www.youtube.com/watch?v=tXUXl_RzkAk

- **Git:** Some of the exercises require the use of Git. This [Software Carpentry course](https://swcarpentry.github.io/git-novice/) teaches Git for beginners, while this [cheat sheet](https://about.gitlab.com/images/press/git-cheat-sheet.pdf) lists the most common Git commands.
