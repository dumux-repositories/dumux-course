// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Course contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \ingroup Chemistry
 * \brief The source and sink terms due to reactions are calculated in this class.
 */
#ifndef DUMUX_BIOMIN_REACTIONS_HH
#define DUMUX_BIOMIN_REACTIONS_HH

#include <dumux/common/parameters.hh>

namespace Dumux {

/*!
 * \ingroup Chemistry
 * \brief The source and sink terms due to reactions are calculated in this class.
 */
template< class NumEqVector, class VolumeVariables >
class SimpleBiominReactions
{
    using SolidSystem = typename VolumeVariables::SolidSystem;
    using FluidSystem = typename VolumeVariables::FluidSystem;
    using Scalar = typename FluidSystem::Scalar;

public:

    SimpleBiominReactions()
    {   //ureolysis kinetic parameters
        kub_ = getParam<Scalar>("UreolysisCoefficients.Kub");
        kUrease_ = getParam<Scalar>("UreolysisCoefficients.KUrease");
        kUrea_ = getParam<Scalar>("UreolysisCoefficients.KUrea");
    }

    static constexpr int liquidPhaseIdx = FluidSystem::liquidPhaseIdx;
    static constexpr int numComponents = FluidSystem::numComponents;

    static constexpr int H2OIdx = FluidSystem::H2OIdx;
    static constexpr int CO2Idx = FluidSystem::CO2Idx;
    static constexpr int CaIdx = FluidSystem::CaIdx;
    static constexpr int UreaIdx = FluidSystem::UreaIdx;

    // phase indices when used for the SolidSystem context (e.g. density)
    static constexpr int BiofilmPhaseIdx = SolidSystem::BiofilmIdx;
    // overall indices when used in the problem context (source term)
    static constexpr int BiofilmIdx = SolidSystem::BiofilmIdx+numComponents;
    static constexpr int CalciteIdx = SolidSystem::CalciteIdx+numComponents;

    /*!
     * \brief Returns the molality of a component x (mol x / kg solvent) for a given
     * mole fraction (mol x / mol solution)
     * The salinity and the mole Fraction of CO2 are considered
     *
     */
    static Scalar moleFracToMolality(Scalar moleFracX, Scalar moleFracSalinity, Scalar moleFracCTot)
    {
        const Scalar molalityX = moleFracX / (1 - moleFracSalinity - moleFracCTot) / FluidSystem::molarMass(H2OIdx);
        return molalityX;
    }

    /*!
     * \brief Calculate the source/sink term due to reactions.
     *
     * \param Source The source
     * \param volVars The volume variables
     */
    void reactionSource(NumEqVector &q, const VolumeVariables &volVars)
    {
        //   define and compute some parameters for convenience:
        const Scalar xwCa = volVars.moleFraction(liquidPhaseIdx,CaIdx);
        const Scalar densityBiofilm = volVars.solidComponentDensity(BiofilmPhaseIdx);
        using std::max;
        const Scalar volFracBiofilm = max(volVars.solidVolumeFraction(BiofilmPhaseIdx),0.0);

        // TODO: dumux-course-task 2:
        // implement mass of biofilm
        const Scalar massBiofilm = 0.0;
        const Scalar molalityUrea = moleFracToMolality(volVars.moleFraction(liquidPhaseIdx,UreaIdx),
                                                 xwCa,
                                                 volVars.moleFraction(liquidPhaseIdx,CO2Idx));  // [mol_urea/kg_H2O]

        // TODO: dumux-course-task 2:
        // compute rate of ureolysis by implementing Zub and rurea
        const Scalar Zub = 0.0;
        const Scalar rurea = 0.0;

        // TODO: dumux-course-task 2:
        // compute/set dissolution and precipitation rate of calcite
        const Scalar rprec = 0.0;

        // set source terms
        // TODO: dumux-course-task 2:
        // set the source terms using the reaction rates defined above and signs based on the stoichiometry of the reactions.
        // Hint: Ammonium occurs in the reaction equation, but is not accounted for in this simplified setup.
        // Additionally, the consumption of water by the reaction may be neglected, as water is present in excess.
        q[H2OIdx]     += 0.0;
        q[CO2Idx]     += 0.0;
        q[CaIdx]      += 0.0;
        q[UreaIdx]    += 0.0;
        q[BiofilmIdx] += 0.0;
        q[CalciteIdx] += 0.0;
    }

private:
    // urease parameters
    Scalar kub_;
    Scalar kUrease_;
    Scalar kUrea_;
};

} //end namespace Dumux

#endif
