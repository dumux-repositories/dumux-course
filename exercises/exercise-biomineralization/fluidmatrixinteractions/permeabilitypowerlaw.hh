// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Course contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \ingroup Fluidmatrixinteractions
 * \brief The Power-Law relationship for the calculation of a porosity-dependent permeability.
 */
#ifndef DUMUX_PERMEABILITY_POWER_LAW_HH
#define DUMUX_PERMEABILITY_POWER_LAW_HH

#include <cmath>
#include <dune/common/fmatrix.hh>
#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>

namespace Dumux {

/*!
 * \ingroup Fluidmatrixinteractions
 * \brief The Power-Law relationship for the calculation of a porosity-dependent permeability.
 *        When the porosity is implemented as solution-independent, using this relationship for the
 *        permeability leads to unnecessary overhead.
 */
template<class PermeabilityType>
class PermeabilityPowerLaw
{
public:
    /*!
     * \brief calculates the permeability for a given sub-control volume
     * \param refPerm Reference permeability before porosity changes
     * \param refPoro The porosity corresponding to the reference permeability
     * \param poro The porosity for which permeability is to be evaluated
     */
    template<class Scalar>
    PermeabilityType evaluatePermeability(PermeabilityType refPerm, Scalar refPoro, Scalar poro) const
    {
        using std::pow;
        // TODO: dumux-course-task 6:
        // read the exponent for the power law from the input file

        // TODO: dumux-course-task 6:
        // return the updated permeability according to K=K_0*(poro/refPoro)^exponent
        return refPerm;
    }
};

} // namespace Dumux

#endif
