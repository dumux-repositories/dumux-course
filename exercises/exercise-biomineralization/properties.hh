// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Course contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 *
 * \brief The two-phase porousmediumflow properties file for exercise biomin
 */

#ifndef DUMUX_EXERCISE_FOUR_PROPERTIES_HH
#define DUMUX_EXERCISE_FOUR_PROPERTIES_HH

#include <dune/grid/yaspgrid.hh>

#include <dumux/discretization/cctpfa.hh>
#include <dumux/porousmediumflow/2pncmin/model.hh>
#include <dumux/porousmediumflow/problem.hh>
#include <dumux/material/components/simpleco2.hh> //!< Simplified CO2 component based on ideal gas law
// TODO: dumux-course-task 7:
// include the CO2 component and tabulated values from DuMux
#include "solidsystems/biominsolidphase.hh" // The biomineralization solid system

#include "fluidsystems/biomin.hh" // The biomineralization fluid system

#include "biominspatialparams.hh" // Spatially dependent parameters
#include "biominproblem.hh"

namespace Dumux {
namespace Properties {

//! Create new type tag for the problem
// Create new type tags
namespace TTag {
struct ExerciseBioMin { using InheritsFrom = std::tuple<TwoPNCMin>; };
struct ExerciseBioMinCCTpfa { using InheritsFrom = std::tuple<ExerciseBioMin, CCTpfaModel>; };
} // end namespace TTag

//! Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::ExerciseBioMin> { using type = BioMinProblem<TypeTag>; };

//! Set grid and the grid creator to be used
template<class TypeTag>
struct Grid<TypeTag, TTag::ExerciseBioMin> { using type = Dune::YaspGrid<2>; };

//! Set the fluid system type
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::ExerciseBioMin>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    // TODO: dumux-course-task 7:
    // use the CO2 component with tabulated values
    using CO2Impl = Components::SimpleCO2<Scalar>;
    using H2OType = Components::TabulatedComponent<Components::H2O<Scalar>>;
public:
    using type = FluidSystems::BioMin<Scalar, CO2Impl, H2OType>;
};

template<class TypeTag>
struct SolidSystem<TypeTag, TTag::ExerciseBioMin>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = SolidSystems::BiominSolidPhase<Scalar>;
};

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::ExerciseBioMin> {
    using MT = GetPropType<TypeTag, ModelTraits>;
    static constexpr int numFluidComps = MT::numFluidComponents();
    static constexpr int numActiveSolidComps = MT::numSolidComps() - MT::numInertSolidComps();
    using type = BioMinSpatialparams<GetPropType<TypeTag, GridGeometry>, GetPropType<TypeTag, Scalar>, numFluidComps, numActiveSolidComps>;
};

template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::ExerciseBioMin> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::ExerciseBioMin> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::ExerciseBioMin> { static constexpr bool value = true; };

} // end namespace properties
} // end namespace Dumux

#endif
