# Exercise Basics (DuMuX course)

## Problem set-up

N$_2$ is injected in an aquifer previously saturated with water with an injection rate of 0.0001 kg/(s*m$^2$).
The aquifer is situated 2700 m below sea level and the domain size is 60 m x 40 m. It consists of two layers, a moderately permeable one ($\Omega_1$) and a lower permeable one ($\Omega_2$).

<img src="https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-course/raw/master/slides/img/exercise_basic_setup.png" width="1000">

## Preparing the exercise

* Navigate to the directory `dumux-course/exercises/exercise-basic`

This exercise deals with two problems: a two-phase immiscible problem (__2p__) and a two-phase non-isothermal problem (__2pni__). They both set up the same scenario with the difference that the 2pni model introduces an extra energy equation.

## Task 1: Getting familiar with the code

Locate all the files you will need for this exercise
* The __main file__ for the __2p__ problem : `2pmain.cc`
* The __problem file__ for the __2p__ problem: `injection2pproblem.hh`
* The __problem file__ for the __2pni__ problem: `injection2pniproblem.hh`
* The __properties file__ for the __2p__ problem: `properties2p.hh`
* The __properties file__ for the __2pni__ problem: `properties2pni.hh`
* The shared __spatial parameters file__: `injection2pspatialparams.hh`
* The shared __input file__: `params.input`

## Task 2: Compiling and running an executable

* Change to the build-directory

```bash
cd ../../build-cmake/exercises/exercise-basic
```

* Compile the executable `exercise_basic_2p`

```bash
make exercise_basic_2p
```

* Execute the problem and inspect the result

```bash
./exercise_basic_2p params.input
```

* you can look at the results with paraview

```bash
paraview injection-2p.pvd
```

## Task 3: Setting up a new executable (for a non-isothermal simulation)

* Copy the main file `2pmain.cc` and rename it to `2pnimain.cc`
* In  `2pnimain.cc`, include the header `properties2pni.hh` instead of `properties2p.hh`.
* In  `2pnimain.cc`, change `Injection2pCC` to `Injection2pNICC` in the line `using TypeTag = Properties::TTag::Injection2pNICC;`
* Add a new executable in `CMakeLists.txt` by adding the lines

```cmake
# the two-phase non-isothermal simulation program
dumux_add_test(NAME exercise_basic_2pni
               SOURCES 2pnimain.cc)
```

* In the respective build-cmake folder, test that everything compiles without error

```bash
make # should rerun cmake
make exercise_basic_2pni # builds new executable
```

## Task 4: Setting up a non-isothermal __2pni__ test problem

* Open the files `injection2pniproblem.hh` and `properties2pni.hh`.
These are copies of the `injection2pproblem.hh` and `properties2p.hh` files, with some useful comments on how to implement a non-isothermal model.
Look for comments containing

```c++
// TODO: dumux-course-task 4
```

* The following set-up should be realized:

  __Initial conditions:__ For the primary variable __temperature__ use a varying temperature of <br/>
$`\displaystyle T(y) = 283~\text{K} + 0.03~\frac{\text{K}}{\text{m}} \cdot \left( d_\text{aquifer} - y \right)`$, <br/>
with the aquifer depth
$\displaystyle d_\text{aquifer}=2700~\text{m}$. Additionally, add a subdomain (20 < x < 30, 5 < y < 35), where you assign a constant initial temperature of 380 K.

  __Boundary conditions:__ Dirichlet boundary conditions at the left boundary with the same temperature gradient as in the initial conditions. For the Neumann conditions, assign an energy flux at the injection point of N$_2$ and no-flow conditions for the energy balance to the rest of the boundaries.

<img src="https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-course/raw/master/slides/img/exercise_basic_nonisothermal.png" width="800">

The non-isothermal model requires additional parameters like the thermal conductivity of the solid component. They are already implemented and set in `params.input`, you just need to _uncomment_ them.
