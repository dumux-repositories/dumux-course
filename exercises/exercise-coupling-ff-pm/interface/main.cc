// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Course contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 *
 * \brief A test problem for the coupled Stokes/Darcy problem (1p)
 */
#include <config.h>

#include <iostream>

#include <dumux/common/initialize.hh>
#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>

#include <dumux/common/partial.hh>

#include <dumux/linear/istlsolvers.hh>
#include <dumux/linear/linearalgebratraits.hh>
#include <dumux/linear/linearsolvertraits.hh>

#include <dumux/assembly/fvassembler.hh>
#include <dumux/assembly/diffmethod.hh>
#include <dumux/discretization/method.hh>
#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/io/staggeredvtkoutputmodule.hh>
#include <dumux/io/grid/gridmanager_yasp.hh>

#include <dumux/multidomain/staggeredtraits.hh>
#include <dumux/multidomain/fvassembler.hh>
#include <dumux/multidomain/newtonsolver.hh>

#include <dumux/multidomain/boundary/stokesdarcy/couplingmanager.hh>

#include "properties.hh"

int main(int argc, char** argv)
{
    using namespace Dumux;

    // initialize MPI+x, finalize is done automatically on exit
    Dumux::initialize(argc, argv);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    // Define the sub problem type tags
    using FreeflowTypeTag = Properties::TTag::FreeflowOneP;
    using PorousMediumTypeTag = Properties::TTag::PorousMediumFlowOneP;

    //TODO: dumux-course-task 1.C:
    // ******************** comment-out this section for the last exercise **************** //

    // create two individual grids (from the given grid file or the input file)
    // for both sub-domains
    using PorousMediumGridManager = Dumux::GridManager<GetPropType<PorousMediumTypeTag, Properties::Grid>>;
    PorousMediumGridManager porousMediumGridManager;
    porousMediumGridManager.init("PorousMedium"); // pass parameter group

    using FreeflowGridManager = Dumux::GridManager<GetPropType<FreeflowTypeTag, Properties::Grid>>;
    FreeflowGridManager freeflowGridManager;
    freeflowGridManager.init("Freeflow"); // pass parameter group

    // we compute on the leaf grid view
    const auto& porousMediumGridView = porousMediumGridManager.grid().leafGridView();
    const auto& freeflowGridView = freeflowGridManager.grid().leafGridView();

    // ************************************************************************************ //


    // ******************** uncomment this section for the last exercise ****************** //

//     // use dune-subgrid to create the individual grids
//     static constexpr int dim = 2;
//     using HostGrid = Dune::YaspGrid<2, Dune::TensorProductCoordinates<double, dim> >;
//     using HostGridManager = Dumux::GridManager<HostGrid>;
//     HostGridManager hostGridManager;
//     hostGridManager.init();
//     auto& hostGrid = hostGridManager.grid();
//
//     struct Params
//     {
//         double amplitude = getParam<double>("Grid.Amplitude");
//         double baseline = getParam<double>("Grid.Baseline");
//         double offset = getParam<double>("Grid.Offset");
//         double scaling = getParam<double>("Grid.Scaling");
//     };
//
//     Params params;
//
//     auto elementSelectorFreeflow = [&](const auto& element)
//     {
//         double interface = params.amplitude * std::sin(( element.geometry().center()[0] -params.offset) / params.scaling * 2.0 * M_PI) + params.baseline;
//         return element.geometry().center()[1] > interface;
//     };
//
//     auto elementSelectorPorousMedium = [&](const auto& element)
//     {
//         double interface  =  params.amplitude * std::sin(( element.geometry().center()[0] - params.offset) / params.scaling * 2.0 * M_PI) + params.baseline;
//         return element.geometry().center()[1] < interface;
//     };
//
//     using SubGrid = Dune::SubGrid<dim, HostGrid>;
//
//     Dumux::GridManager<SubGrid> subGridManagerFreeflow;
//     Dumux::GridManager<SubGrid> subGridManagerPorousMedium;
//
//     // initialize subgrids
//     subGridManagerFreeflow.init(hostGrid, elementSelectorFreeflow, "Freeflow");
//     subGridManagerPorousMedium.init(hostGrid, elementSelectorPorousMedium, "PorousMedium");
//
//     // we compute on the leaf grid view
//     const auto& porousMediumGridView = subGridManagerPorousMedium.grid().leafGridView();
//     const auto& freeflowGridView = subGridManagerFreeflow.grid().leafGridView();

    // ************************************************************************************ //


    // create the finite volume grid geometry
    using FreeflowFVGridGeometry = GetPropType<FreeflowTypeTag, Properties::GridGeometry>;
    auto freeflowFvGridGeometry = std::make_shared<FreeflowFVGridGeometry>(freeflowGridView);
    using PorousMediumFVGridGeometry = GetPropType<PorousMediumTypeTag, Properties::GridGeometry>;
    auto porousMediumFvGridGeometry = std::make_shared<PorousMediumFVGridGeometry>(porousMediumGridView);

    using Traits = StaggeredMultiDomainTraits<FreeflowTypeTag, FreeflowTypeTag, PorousMediumTypeTag>;

    // the coupling manager
    using CouplingManager = StokesDarcyCouplingManager<Traits>;
    auto couplingManager = std::make_shared<CouplingManager>(freeflowFvGridGeometry, porousMediumFvGridGeometry);

    // the indices
    constexpr auto freeflowCellCenterIdx = CouplingManager::stokesCellCenterIdx;
    constexpr auto freeflowFaceIdx = CouplingManager::stokesFaceIdx;
    constexpr auto porousMediumIdx = CouplingManager::darcyIdx;

    // the problem (initial and boundary conditions)
    using FreeflowProblem = GetPropType<FreeflowTypeTag, Properties::Problem>;
    auto freeflowProblem = std::make_shared<FreeflowProblem>(freeflowFvGridGeometry, couplingManager);
    using PorousMediumProblem = GetPropType<PorousMediumTypeTag, Properties::Problem>;
    auto porousMediumProblem = std::make_shared<PorousMediumProblem>(porousMediumFvGridGeometry, couplingManager);

    // the solution vector
    Traits::SolutionVector sol;
    sol[freeflowCellCenterIdx].resize(freeflowFvGridGeometry->numCellCenterDofs());
    sol[freeflowFaceIdx].resize(freeflowFvGridGeometry->numFaceDofs());
    sol[porousMediumIdx].resize(porousMediumFvGridGeometry->numDofs());

    auto freeflowSol = partial(sol, freeflowFaceIdx, freeflowCellCenterIdx);

    couplingManager->init(freeflowProblem, porousMediumProblem, sol);

    // the grid variables
    using FreeflowGridVariables = GetPropType<FreeflowTypeTag, Properties::GridVariables>;
    auto freeflowGridVariables = std::make_shared<FreeflowGridVariables>(freeflowProblem, freeflowFvGridGeometry);
    freeflowGridVariables->init(freeflowSol);
    using PorousMediumGridVariables = GetPropType<PorousMediumTypeTag, Properties::GridVariables>;
    auto porousMediumGridVariables = std::make_shared<PorousMediumGridVariables>(porousMediumProblem, porousMediumFvGridGeometry);
    porousMediumGridVariables->init(sol[porousMediumIdx]);

    // intialize the vtk output module
    const auto freeflowName = getParam<std::string>("Problem.Name") + "_" + freeflowProblem->name();
    const auto porousMediumName = getParam<std::string>("Problem.Name") + "_" + porousMediumProblem->name();

    StaggeredVtkOutputModule<FreeflowGridVariables, decltype(freeflowSol)> freeflowVtkWriter(*freeflowGridVariables, freeflowSol, freeflowName);
    GetPropType<FreeflowTypeTag, Properties::IOFields>::initOutputModule(freeflowVtkWriter);

    //TODO: dumux-course-task 1.B:
    //****** uncomment the add analytical solution of v_x *****//
    // freeflowVtkWriter.addField(freeflowProblem->getAnalyticalVelocityX(), "analyticalV_x");

    using PorousMediumSolutionVector = GetPropType<PorousMediumTypeTag, Properties::SolutionVector>;
    VtkOutputModule<PorousMediumGridVariables, PorousMediumSolutionVector> porousMediumVtkWriter(*porousMediumGridVariables,
                                                                                                 sol[porousMediumIdx],
                                                                                                 porousMediumName);

    using PorousMediumVelocityOutput = GetPropType<PorousMediumTypeTag, Properties::VelocityOutput>;
    porousMediumVtkWriter.addVelocityOutput(std::make_shared<PorousMediumVelocityOutput>(*porousMediumGridVariables));
    GetPropType<PorousMediumTypeTag, Properties::IOFields>::initOutputModule(porousMediumVtkWriter);

    // the assembler for a stationary problem
    using Assembler = MultiDomainFVAssembler<Traits, CouplingManager, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(std::make_tuple(freeflowProblem, freeflowProblem, porousMediumProblem),
                                                 std::make_tuple(freeflowFvGridGeometry->faceFVGridGeometryPtr(),
                                                                 freeflowFvGridGeometry->cellCenterFVGridGeometryPtr(),
                                                                 porousMediumFvGridGeometry),
                                                 std::make_tuple(freeflowGridVariables->faceGridVariablesPtr(),
                                                                 freeflowGridVariables->cellCenterGridVariablesPtr(),
                                                                 porousMediumGridVariables),
                                                 couplingManager);

    // the linear solver
    using LinearSolver = UMFPackIstlSolver<SeqLinearSolverTraits, LinearAlgebraTraitsFromAssembler<Assembler>>;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = MultiDomainNewtonSolver<Assembler, LinearSolver, CouplingManager>;
    NewtonSolver nonLinearSolver(assembler, linearSolver, couplingManager);

    std::cout << "\nSolving the coupled problem..." << std::endl;

    // solve the non-linear system
    nonLinearSolver.solve(sol);

    // write vtk output
    freeflowVtkWriter.write(1.0);
    porousMediumVtkWriter.write(1.0);

    Parameters::print();

    return 0;
} // end main
