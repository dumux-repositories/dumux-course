# SPDX-FileCopyrightText: Copyright © DuMux-Course contributors, see AUTHORS.md in root folder
# SPDX-License-Identifier: GPL-3.0-or-later

import json
import matplotlib.pyplot as plt

# JSON file
jsonFilePath = "./flux_models_coupling.json"
file = open(jsonFilePath)
fullEvaporationData = json.loads(file.read())

for key in fullEvaporationData:
    print("developing plot " + key)

    fig, ax = plt.subplots(1, 1, figsize=(6, 6))
    plt.title('Interfacial Evaporation Rate: ' + key, fontsize=24)
    plt.xlabel('Interface x')
    plt.ylabel('Evaporation flux')
    evaporationData = fullEvaporationData[key]
    coordinates = evaporationData["FaceCenterCoordinates"]
    timeSeries = evaporationData["Time"]
    xCoords = [x for x,y in coordinates]
    facewiseEvaporationResults = evaporationData["FaceEvaporation"]

    count = 0
    for time, results in zip(timeSeries, facewiseEvaporationResults):
        if (count%2 == 0 and time >= 1.0): # plot every 5th time step
            ax.plot(xCoords, results, label="{:0.3f}".format(time))
        count = count+1

    ax.legend(title="Time (s)")
    outputFig = plt.gcf()
    outputFig.savefig('./EvaporationOverInterface_' + key + '.png', bbox_inches='tight')
    plt.clf()
    plt.close()
