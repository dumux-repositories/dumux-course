## 2. Changing the porous medium model

In this part of the exercise, the coupled system will be extended such that the transport of components
in both domains is included and the presence of a second phase in the porous medium domain is considered.
This enables the simulation of the drying of a porous medium (however no energy balance is included yet).

Compared to exercise `1. Changing the interface` account now for the gravity effect as well as solving for the transient problem.

This part of the exercise consists of the following steps:
* replacing the 1pnc porous-medium model by a 2pnc porous-medium  model,
* adding the output for fluxes and storage terms (having gnuplot is recommended),
* enable a variable switch for a complete drying.

We start with an example in which the transport of water vapor in the gas phase is considered.
The porous medium is filled with gas, initially the mole fraction of water vapor is $`x^w_g = 0.1`$.
Above the porous medium, a dry gas flows and by diffusion, the porous medium dries out.
(Don't mind the compiler warning, we will deal with it in task B.)


### Task A: Change the model

In the first task, the porous-medium model will be changed from a 1p2c system to a 2p2c system.
Although a 2p2c system is used, we still want to simulate the same situation as before, i.e., air with water vapor in both domains.
The following changes have to be made in the porous-medium model (`models/properties.hh`):
* Include the 2pnc model: include the respective headers and inherit from the new model `TwoPNC`
* Exchange the spatial parameters for the 1-phase system by those for a 2-phase system.
* Since two phases are involved now, we do not need to use the `OnePAdapter` anymore. Change the property of the `FluidSystem` such that `H2OAir` is used directly.
  Afterwards, set the `transportCompIdx` to `Indices::switchIdx` in `porousmediumsubproblem.hh`.

One big difference between the 1p and 2p model is, that the primary variables for which
the problem is solved, are not fixed.
It is possible to use different formulations, e.g. with the gas pressure and the
liquid saturation as primary variables (p_g-S_l -> `p1s0`) or vice versa.
* Set the property

```
template<class TypeTag>
struct Formulation<TypeTag, TTag::PorousMediumOnePNC>
{ static constexpr auto value = TwoPFormulation::p1s0; };
```
  in the properties file.

In contrast to the formulation, which remains the same during one simulation,
the primary variables may vary during one simulation.
In the case under investigation, we want to use the gas pressure and the liquid saturation as primary variables.
However, if only the gas phase is present, the liquid saturation is always zero.
In this case, the chosen formulation will set the given value as the mole fraction of water vapor in the gas phase.
* To tell the program which phases are present in which parts of the domain at the beginning of the simulation,
  you have to call `values.setState(MY_PHASE_PRESENCE);` in `initialAtPos()`. `MY_PHASE_PRESENCE` should be replaced with the correct value for the case where only a gas-phase (second phase of the fluid system) is present. For finding out if a gas or liquid phase is the first of second phase of a fluidsystem (here `h20air`), take a look at your fluidsystem (e.g. [h2oair](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux/-/blob/master/dumux/material/fluidsystems/h2oair.hh?ref_type=heads#L75) ).
  Moreover, have a look at the [indices.hh](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux/-/blob/master/dumux/porousmediumflow/2pnc/indices.hh?ref_type=heads#L22) in the `2pnc` model (as the 2p2c model is a special case of the 2pnc model) in the subfolder `porousmediumflow` in your DuMuX directory to see which value to set for the gas-phase (here: second phase of our fluidsystem). 

Make sure your code is compiling and running. You can have a look at the results with paraview. How do the mass or mole fractions of water in air change over time?

### Task B: Add output

In the next step, we want to add some output to the simulation. The standard method for providing simulation output for visualization is via a `VtkWriter`. These tools take the grid geometries of each domain, as well as the solutions and write spatial output that one can view in visualization tools such as paraview.

Although this Vtk output is very useful, some output is more suited for other forms of visualization.
Two examples of data output formats are `.csv` files and `.json` files.
From these files, data can be visualized using many different tools.
In this exercise, data exported to a `.csv` file will be plotted using gnuplot, and data exported to a `.json` output will be plotted using the matplotlib python library.

First as example output data, we want to investigate the water mass in the (porous-medium) system.
There are two ways to evaluate this: (1) the total storage of water mass in the porous medium domain, and (2) the water vapor flux along the interface.
The total storage can be plotted over time, whereas the water vapor flux will vary spatially along the interface as well as over time.

First, we evaluate the storage term of the water component.
* Have a look at the function `evaluateWaterMassStorageTerm()` in the `porousmediumsubproblem.hh`.
  Then implement a calculation of the total water mass:
  $`M^\text{w}:=\sum_{\alpha \in \textrm{g,l}} \left( \phi S_\alpha X^\text{w}_\alpha \varrho_\alpha V_\textrm{scv} \right)`$.
  Hint: the volume is a property of a subcontrolvolume (`scv`), the other properties can be obtained through the volume variables (`volVars`). The functions for the `volVars` can be found in the header [dumux/porousmediumflow/2pnc/volumevariables.hh](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux/-/blob/master/dumux/porousmediumflow/2pnc/volumevariables.hh?ref_type=heads)
* Afterwards, adapt the method `startStorageEvaluation()` such that the variable `initialWaterContent_` is
  initialized correctly using the `evaluateWaterMassStorageTerm()` method and assign that value also to the variable `lastWaterMass_`.

After these functions are correctly implemented, the evaporation in [mm/d] is calculated in the code as
$`e = \frac{\textrm{d}\, M^\text{w}}{\textrm{d}\, t} / A_\textrm{interface}`$.
(all boundaries have Neumann no-flow conditions and no sources are present, meaning the water can only leave the system
via the porous-medium free-flow interface.)

During the simulation a `.csv` file is created (enable by setting `[Problem.ExportStorage] = true` in the `params.input` file). In addition, a gnuplot script `StorageOverTime.gp` is written and the mass loss and cumulative mass are plotted over time (`StorageOverTime.png`), when plotting is enabled with `[Problem.PlotStorage] = true`.

Second, we want to know the distribution of the water mass fluxes across the interface.
* Have a look at the function `evaluateInterfaceFluxes()` in the porous medium problem.
  Use the facilities therein to return the values of `massCouplingCondition()` from the `couplingManager`
  for each coupling scvf. Multiply this with the relevant face areas, extrusion factor, molar Mass, and seconds per day to get the [mm/d] units as seen in the storage evaluation.
* When the `[Problem.ExportFluxes] = true` parameter is enabled, simulation data will be exported for this simulation to a `.json` file.
  This file can flexibly handle data of different types, which in this case is helpful as we have both temporal and spatial data.
* Use the python plotting script `plotFluxes.py` to visualize the flux distribution across the surface at different times.
  This script uses `matplotlib`, a very popular python based visualization library and reads-in the data stored in the file `flux_models_coupling.json`.
  You can execute the script by
  ```
  python3 ./plotFluxes.py
  ```

Compile and run the simulation and take a look at the results.

### Task C: Let it dry out

In this exercise we want to completely dry an initial water-filled porous medium.
- Change the initial condition such that it is started with a two-phase system.
  Set the initial saturation to $`S_\text{l} = 0.1`$.

If one wants to simulate the complete drying of a porous medium, the standard capillary pressure-saturation
relationships have to be regularized. If no regularization is used, then the capillary pressure would
approach infinity when the liquid saturation goes to zero. This means that water can flow to the
interface from any region of the porous medium and of course this poses numerical problems.
Therefore, the capillary pressure-saturation relationship has to be regularized for low saturations.
The regularization has a strong influence on how long liquid water is present at the interface, see
[Mosthaf (2014)](http://dx.doi.org/10.18419/opus-519). The regularization of the capillary pressure-saturation relationship with using the vanGenuchten model can be found in the file [dumux/material/fluidmatrixinteractions/2p/vangenuchten.hh](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux/-/blob/master/dumux/material/fluidmatrixinteractions/2p/vangenuchten.hh?ref_type=heads#L304).
*  See how adapting the parameter for `VanGenuchtenPcLowSweThreshold` and `VanGenuchtenPcHighSweThreshold` affects the results.

The porous-medium model can now reach a liquid saturation of zero. As already explained above,
for this case the liquid saturation cannot serve as primary variable anymore. However,
manually adapting the primary variable states and values is inconvenient.
[Class et al. (2002)](http://dx.doi.org/10.1016/S0309-1708(02)00014-3)
describe an algorithm to switch the primary variables, if phases should appear or disappear during a simulation.

Now you are able to simulate a complete drying of the porous medium. Have a look the resulting liquid saturation distribution within the porous medium (with using paraview).
