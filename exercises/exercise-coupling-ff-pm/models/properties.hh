// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Course contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 *
 * \brief Properties file for coupled models example.
 */
#ifndef DUMUX_EXERCISE_COUPLED_MODELS_PROPERTIES_HH
#define DUMUX_EXERCISE_COUPLED_MODELS_PROPERTIES_HH

// Both Domains
#include <dune/grid/yaspgrid.hh>
#include <dumux/multidomain/staggeredtraits.hh>
#include <dumux/multidomain/boundary/stokesdarcy/couplingmanager.hh>

#include <dumux/io/gnuplotinterface.hh>
#include <dumux/material/fluidsystems/1padapter.hh>
#include <dumux/material/fluidsystems/h2oair.hh>

// Porous medium flow domain
#include <dumux/discretization/cctpfa.hh>
#include <dumux/material/fluidmatrixinteractions/diffusivityconstanttortuosity.hh>

// TODO: dumux-course-task 2.A:
// Include 2pnc model here
#include <dumux/porousmediumflow/1pnc/model.hh>
// TODO: dumux-course-task 2.A:
// Include spatial params for a 2-phase system
#include "../1pspatialparams.hh"
#include "porousmediumsubproblem.hh"

// Free-flow
#include <dumux/discretization/staggered/freeflow/properties.hh>
#include <dumux/freeflow/compositional/navierstokesncmodel.hh>

#include "freeflowsubproblem.hh"

namespace Dumux::Properties {

// Create new type tags
namespace TTag {
struct FreeflowNC { using InheritsFrom = std::tuple<NavierStokesNC, StaggeredFreeFlowModel>; };
// TODO: dumux-course-task 2.A:
// Change the inheritance such that the correct model is used.
struct PorousMediumOnePNC { using InheritsFrom = std::tuple<OnePNC, CCTpfaModel>; };
} // end namespace TTag

// Set the coupling manager
template<class TypeTag>
struct CouplingManager<TypeTag, TTag::FreeflowNC>
{
    using Traits = StaggeredMultiDomainTraits<TypeTag, TypeTag, Properties::TTag::PorousMediumOnePNC>;
    using type = Dumux::StokesDarcyCouplingManager<Traits>;
};
template<class TypeTag>
struct CouplingManager<TypeTag, TTag::PorousMediumOnePNC>
{
    using Traits = StaggeredMultiDomainTraits<Properties::TTag::FreeflowNC, Properties::TTag::FreeflowNC, TypeTag>;
    using type = Dumux::StokesDarcyCouplingManager<Traits>;
};

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::PorousMediumOnePNC> { using type = Dumux::PorousMediumSubProblem<TypeTag>; };
template<class TypeTag>
struct Problem<TypeTag, TTag::FreeflowNC> { using type = Dumux::FreeFlowSubProblem<TypeTag> ; };

// The fluid system
// TODO: dumux-course-task 2.A:
// Change to property of the `FluidSystem` such that `H2OAir` is used directly.
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::PorousMediumOnePNC>
{
    using H2OAir = FluidSystems::H2OAir<GetPropType<TypeTag, Properties::Scalar>>;
    using type = FluidSystems::OnePAdapter<H2OAir, H2OAir::gasPhaseIdx>;
};
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::FreeflowNC>
{
    using H2OAir = FluidSystems::H2OAir<GetPropType<TypeTag, Properties::Scalar>>;
    using type = FluidSystems::OnePAdapter<H2OAir, H2OAir::gasPhaseIdx>;
};

// Use moles
template<class TypeTag>
struct UseMoles<TypeTag, TTag::PorousMediumOnePNC> { static constexpr bool value = true; };
template<class TypeTag>
struct UseMoles<TypeTag, TTag::FreeflowNC> { static constexpr bool value = true; };

// Do not replace one equation with a total mass balance
template<class TypeTag>
struct ReplaceCompEqIdx<TypeTag, TTag::PorousMediumOnePNC> { static constexpr int value = 3; };
template<class TypeTag>
struct ReplaceCompEqIdx<TypeTag, TTag::FreeflowNC> { static constexpr int value = 3; };

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::PorousMediumOnePNC> { using type = Dune::YaspGrid<2>; };
template<class TypeTag>
struct Grid<TypeTag, TTag::FreeflowNC> { using type = Dune::YaspGrid<2, Dune::EquidistantOffsetCoordinates<GetPropType<TypeTag, Properties::Scalar>, 2> >; };

//! Use a model with constant tortuosity for the effective diffusivity
template<class TypeTag>
struct EffectiveDiffusivityModel<TypeTag, TTag::PorousMediumOnePNC>
{ using type = DiffusivityConstantTortuosity<GetPropType<TypeTag, Properties::Scalar>>; };

// TODO: dumux-course-task 2.A:
// Define new formulation for primary variables here.

// Set the spatial parameters type
// TODO: dumux-course-task 2.A:
// Adapt the spatial params here.
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::PorousMediumOnePNC> {
    using type = OnePSpatialParams<GetPropType<TypeTag, GridGeometry>, GetPropType<TypeTag, Scalar>>;
};

template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::FreeflowNC> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::FreeflowNC> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::FreeflowNC> { static constexpr bool value = true; };

} // end namespace Dumux::Properties

#endif
