## 3. Use a turbulence model in the free flow domain
As in the previous exercise, we account for the gravity effect as well as solving for the transient problem hereafter.

Several RANS (Reynolds Averaged Navier Stokes) turbulence models are implemented in DuMu<sup>x</sup>.
This part of the exercise consists of the following steps:
* replacing the Navier-Stokes model by the [K-Omega SST (shear stress transport) turbulence model](https://www.cfd-online.com/Wiki/SST_k-omega_model) of [Menter (1994)](https://doi.org/10.2514/3.12149), which is a two-equation RANS turbulence model,
* switching to a symmetry boundary condition,
* applying a grid refinement towards the interface,
* subsequently refining the grid (convergence study).

We will work with a `1p2cni/2p2cni` coupled problem, where `ni` stands for non-isothermal.
All the prepared files can be found in the subfolder `exercise-coupling-ff-pm/turbulence`.

### Task A: Use a $k$-$\omega$ turbulence model

For using the compositional $k$-$\omega$ turbulence model, the following header files need to be included in properties file (`properties.hh`):
```
#include <dumux/freeflow/compositional/sstncmodel.hh>
```
and in free-flow problem file (`freeflowsubproblem.hh`):
```
#include <dumux/freeflow/turbulenceproperties.hh>
#include <dumux/freeflow/rans/twoeq/sst/problem.hh>
#include <dumux/freeflow/rans/boundarytypes.hh>
```

The includes for the NavierStokesNC model and the NavierStokesProblem are no longer needed and can be removed.

Make sure your free flow problem inherits from the correct parent type:
* Change the entry in the `FreeflowModel` definition accordingly (multi-component non-isothermal K-Omega SST model, `SSTNCNI`) in the properties file,
* Adapt the inheritance of the problem class in problem file (Use `RANSProblem<TypeTag>` rather than `NavierStokesStaggeredProblem<TypeTag>`).

Additionally make sure that the alias `BoundaryTypes` uses the new boundary types for the RANS model (as included by the respective header above) instead of `NavierStokesBoundaryTypes`.

Here, the turbulent free flow is wall-bounded, meaning shear stress and turbulence in the flow develop primarily due to the walls.
The porous medium at the bottom and also the channel wall (upper boundary) act here as such walls.
Within the problem the location of boundaries representing walls need to be defined.
To do this, add the following function to the upper and lower boundaries within the `boundaryTypes` function in `freeflowsubproblem.hh`:
```cpp
  values.setWall();
```

With the locations of the wall designated, and the problem initialized, a series of constant spatial properties,
in particular the distance to the nearest wall from each cell center, should be initialized.
To do this, add the following to the `main.cc` file.
```cpp
 freeflowProblem->updateStaticWallProperties();
```

In addition, there is also a non-local solution-dependent aspect of the turbulence models which is to be updated at the end of each step.
An example of this is a stencil extended velocity gradient, and other flow properties in relation to the wall.
These dynamic interactions are to be initialized in the mainfile directly after the `updateStaticWallProperties()` call,
as well as in the time loop after `// Update dynamic wall properties`:
```cpp
freeflowProblem->updateDynamicWallProperties(freeflowSol);
```

In addition to designating the locations of walls,
additional boundary conditions and initial conditions need to be set for the two new primary variables, the turbulent kinetic energy $k$ and the  specific tubulent dissipation rate/turbulence frequency $\omega$.

In the `boundaryTypes` function, set both variables ($k$ and $\omega$) on all free-flow domain boundaries to be Dirichlet, except for the right boundary, which should have outflow conditions. The name of the indices for those two variables that have to be used for setting the boundary conditions types can be found in `dumux/freeflow/rans/twoeq/indices.hh`.

Within the Dirichlet function for cell faces (`dirichlet(element, scvf)`),
we also need to specify that these variables ($k$ and $\omega$) should be fixed to 0 at the wall.
In addition, Dirichlet cell constraints for the dissipation or $\omega$ variable will be set for all wall adjacent cells.
This is done in the `isDirichletCell` function, as well as the `dirichlet` function already, and requires no further changes.

For the initial conditions, Reynolds number specific base conditions should be applied everywhere.
In the problem constructor, uncomment the code calculating these terms,
then apply the `turbulentKineticEnergy_`and `dissipation_` variables to their primary variables in the initial state for all spatial locations.

Before starting to run your modified code make sure to include the parameter `Rans.IsFlatWallBounded` in your input file and set this parameter to `true`.

Compile and run your new coupled problem and take a look at the results in Paraview.
In addition to the standard variables and parameters, you can now analyze turbulence model specific quantities
(e.g. the turbulent viscosity $`\nu_\textrm{t}`$ or the turbulent diffusivity $`D_\textrm{t}`$) for the free flow domain.
In paraview you may compare the magnitude of $`D`$ and $`D_\textrm{t}`$ to see where the transport is affected by turbulence.
The result for the turbulent diffusivity should look like this:

![](../../../slides/img/exercise_ffpm_turb_diffusivity.png)

### Task B: Use symmetry boundary conditions

Instead of computing the whole cross-section of a channel,
you can use symmetric boundary conditions at the top boundary of your free flow domain by replacing all previous boundary conditions at the top with
```c++
values.setAllSymmetry();
```

In addition, you have to remove the condition `onUpperBoundary_(globalPos)` from the `initialAtPos(globalPos)` method and the `dirichlet(element, scvf)` method.

Now it is sufficient to only calulate half of the domain height for the free-flow domain. For this, adapt the grid coordinates as well as the number of cells in y-direction in your `params.input` - file accordingly.
After recompiling and running your simulation with the new boundary condition, check your results with paraview.
Does your velocity profile at the symmetry boundary look reasonable?

Before closing paraview, Choose `Surface With Edges` instead of `Surface` in the Paraview toolbar to see the discretization grid. Until now we used a regular structured lattice grid.

### Task C: Refine grid towards the interface

We will refine the grid now in order to better resolve the processes at the coupling interface.
Since not much is happening at the upper and lower boundaries of the whole domain, we want to keep the resolution low in these areas to save some computation time.

A grid refinement towards the interface is called _grading_.
Try different gradings by changing the values in the respective sections in the input file:
```c++
Grading0 = 1.0
Grading1 = 1.0
```

Rerun your simulation with the new grid and check your refined grid with paraview.

### Task D: Perform a grid convergence study

For the grid convergence study, run various simulations with the following grading parameters:
```c++
* [Freeflow.Grid] Grading1 = 1.2, [PorousMedium.Grid] Grading1 = -1.2
* [Freeflow.Grid] Grading1 = 1.3, [PorousMedium.Grid] Grading1 = -1.3
* [Freeflow.Grid] Grading1 = 1.4, [PorousMedium.Grid] Grading1 = -1.4
* [Freeflow.Grid] Grading1 = 1.5, [PorousMedium.Grid] Grading1 = -1.5
* [Freeflow.Grid] Grading1 = 1.6, [PorousMedium.Grid] Grading1 = -1.6
```

By changing the parameter `Problem.Name` for each grading factor, you avoid losing the `.vtu` and `.pvd` files of the previous simulation runs.
Check the first lines of the output in your terminal to see how the grading factors change the height of your grid cells.

Compare the velocity fields with Paraview. In Paraview you can e.g. use the tool `PlotOverLine` to plot the velocity in $x$-direction ($v_x$) over the oulet on the right hand side. After choosing `PlotOverLine` make sure to give the Point 1 as (0.25, 0.25, 0) and the Point 2 as (0.25, 0.5, 0) to define the line a the output. Apply this and choose to only visualize `velocity_gas(m/s)_X`. You can do this for all your refinements and plot them in the same diagram to compare.
