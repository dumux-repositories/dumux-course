// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Course contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \brief The coupled exercise properties file or the turbulent case.
 */
#ifndef DUMUX_EXERCISE_COUPLED_TURBULENCE_PROPERTIES_HH
#define DUMUX_EXERCISE_COUPLED_TURBULENCE_PROPERTIES_HH

// Both domains
#include <dune/grid/yaspgrid.hh>
#include <dumux/multidomain/staggeredtraits.hh>
#include <dumux/multidomain/boundary/stokesdarcy/couplingmanager.hh>

#include <dumux/material/fluidsystems/h2oair.hh>
#include <dumux/material/fluidsystems/1padapter.hh>

// Porous medium flow domain
#include <dumux/porousmediumflow/2p2c/model.hh>
#include <dumux/discretization/cctpfa.hh>

#include"porousmediumsubproblem.hh"
#include "../2pspatialparams.hh"

// Free-flow domain
#include <dumux/discretization/staggered/freeflow/properties.hh>
// TODO: dumux-course-task 3.A:
// Include headers for compositional k-\omega SST turbulence model here.
#include <dumux/freeflow/compositional/navierstokesncmodel.hh>

#include"freeflowsubproblem.hh"

namespace Dumux::Properties {

// Create new type tags
namespace TTag {
struct PorousMediumFlowModel { using InheritsFrom = std::tuple<TwoPTwoCNI, CCTpfaModel>; };
// TODO: dumux-course-task 3.A:
// Change the entry in the `FreeflowModel` definition accordingly.
struct FreeflowModel { using InheritsFrom = std::tuple<NavierStokesNCNI, StaggeredFreeFlowModel>; };
} // end namespace TTag

// Set the coupling manager
template<class TypeTag>
struct CouplingManager<TypeTag, TTag::FreeflowModel>
{
    using Traits = StaggeredMultiDomainTraits<TypeTag, TypeTag, Properties::TTag::PorousMediumFlowModel>;
    using type = Dumux::StokesDarcyCouplingManager<Traits>;
};
template<class TypeTag>
struct CouplingManager<TypeTag, TTag::PorousMediumFlowModel>
{
    using Traits = StaggeredMultiDomainTraits<Properties::TTag::FreeflowModel, Properties::TTag::FreeflowModel, TypeTag>;
    using type = Dumux::StokesDarcyCouplingManager<Traits>;
};

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::PorousMediumFlowModel> { using type = Dumux::PorousMediumSubProblem<TypeTag>; };
template<class TypeTag>
struct Problem<TypeTag, TTag::FreeflowModel> { using type = Dumux::FreeFlowSubProblem<TypeTag> ; };

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::PorousMediumFlowModel> { using type = Dune::YaspGrid<2, Dune::TensorProductCoordinates<GetPropType<TypeTag, Properties::Scalar>, 2> >; };
template<class TypeTag>
struct Grid<TypeTag, TTag::FreeflowModel> { using type = Dune::YaspGrid<2, Dune::TensorProductCoordinates<GetPropType<TypeTag, Properties::Scalar>, 2> >; };

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::PorousMediumFlowModel> { using type = FluidSystems::H2OAir<GetPropType<TypeTag, Properties::Scalar>>; };
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::FreeflowModel>
{
  using H2OAir = FluidSystems::H2OAir<GetPropType<TypeTag, Properties::Scalar>>;
  static constexpr auto phaseIdx = H2OAir::gasPhaseIdx; // simulate the air phase
  using type = FluidSystems::OnePAdapter<H2OAir, phaseIdx>;
};

// Use formulation based on mole fractions
template<class TypeTag>
struct UseMoles<TypeTag, TTag::PorousMediumFlowModel> { static constexpr bool value = true; };
template<class TypeTag>
struct UseMoles<TypeTag, TTag::FreeflowModel> { static constexpr bool value = true; };

// The gas component balance (air) is replaced by the total mass balance
template<class TypeTag>
struct ReplaceCompEqIdx<TypeTag, TTag::PorousMediumFlowModel> { static constexpr int value = 3; };
template<class TypeTag>
struct ReplaceCompEqIdx<TypeTag, TTag::FreeflowModel> { static constexpr int value = 3; };


//! Set the default formulation to pw-Sn: This can be over written in the problem.
template<class TypeTag>
struct Formulation<TypeTag, TTag::PorousMediumFlowModel>
{ static constexpr auto value = TwoPFormulation::p1s0; };

template<class TypeTag>
struct SpatialParams<TypeTag, TTag::PorousMediumFlowModel>
{ using type = TwoPSpatialParams<GetPropType<TypeTag, GridGeometry>, GetPropType<TypeTag, Scalar>>; };

template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::FreeflowModel> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::FreeflowModel> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::FreeflowModel> { static constexpr bool value = true; };

} // end namespace Dumux::Properties

#endif
