// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Course contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 *
 * \brief A test problem for the isothermal coupled Stokes/Darcy problem (1p2c/2p2c)
 */
#include <config.h>

#include <iostream>
#include <fstream>

#include <dumux/common/initialize.hh>
#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>

#include <dumux/common/partial.hh>

#include <dumux/linear/istlsolvers.hh>
#include <dumux/linear/linearalgebratraits.hh>
#include <dumux/linear/linearsolvertraits.hh>

#include <dumux/assembly/fvassembler.hh>
#include <dumux/assembly/diffmethod.hh>
#include <dumux/discretization/method.hh>
#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/io/staggeredvtkoutputmodule.hh>
#include <dumux/io/grid/gridmanager_yasp.hh>

#include <dumux/multidomain/staggeredtraits.hh>
#include <dumux/multidomain/fvassembler.hh>
#include <dumux/multidomain/newtonsolver.hh>

#include <dumux/multidomain/boundary/stokesdarcy/couplingmanager.hh>

#include "properties.hh"

int main(int argc, char** argv)
{
    using namespace Dumux;

    // initialize MPI+x, finalize is done automatically on exit
    Dumux::initialize(argc, argv);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    // Define the sub problem type tags
    using FreeflowTypeTag = Properties::TTag::FreeflowModel;
    using PorousMediumTypeTag = Properties::TTag::PorousMediumFlowModel;

    // try to create a grid (from the given grid file or the input file)
    // for both sub-domains
    using PorousMediumGridManager = Dumux::GridManager<GetPropType<PorousMediumTypeTag, Properties::Grid>>;
    PorousMediumGridManager porousMediumGridManager;
    porousMediumGridManager.init("PorousMedium"); // pass parameter group

    using FreeflowGridManager = Dumux::GridManager<GetPropType<FreeflowTypeTag, Properties::Grid>>;
    FreeflowGridManager freeflowGridManager;
    freeflowGridManager.init("Freeflow"); // pass parameter group

    // we compute on the leaf grid view
    const auto& porousMediumGridView = porousMediumGridManager.grid().leafGridView();
    const auto& freeflowGridView = freeflowGridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using FreeflowFVGridGeometry = GetPropType<FreeflowTypeTag, Properties::GridGeometry>;
    auto freeflowFvGridGeometry = std::make_shared<FreeflowFVGridGeometry>(freeflowGridView);
    using PorousMediumFVGridGeometry = GetPropType<PorousMediumTypeTag, Properties::GridGeometry>;
    auto porousMediumFvGridGeometry = std::make_shared<PorousMediumFVGridGeometry>(porousMediumGridView);

    using Traits = StaggeredMultiDomainTraits<FreeflowTypeTag, FreeflowTypeTag, PorousMediumTypeTag>;

    // the coupling manager
    using CouplingManager = StokesDarcyCouplingManager<Traits>;
    auto couplingManager = std::make_shared<CouplingManager>(freeflowFvGridGeometry, porousMediumFvGridGeometry);

    // the indices
    constexpr auto freeflowCellCenterIdx = CouplingManager::stokesCellCenterIdx;
    constexpr auto freeflowFaceIdx = CouplingManager::stokesFaceIdx;
    constexpr auto porousMediumIdx = CouplingManager::darcyIdx;

    // the problem (initial and boundary conditions)
    using FreeflowProblem = GetPropType<FreeflowTypeTag, Properties::Problem>;
    auto freeflowProblem = std::make_shared<FreeflowProblem>(freeflowFvGridGeometry, couplingManager);
    using PorousMediumProblem = GetPropType<PorousMediumTypeTag, Properties::Problem>;
    auto porousMediumProblem = std::make_shared<PorousMediumProblem>(porousMediumFvGridGeometry, couplingManager);

    // initialize the fluidsystem (tabulation)
    GetPropType<FreeflowTypeTag, Properties::FluidSystem>::init();

    // get some time loop parameters
    using Scalar = GetPropType<FreeflowTypeTag, Properties::Scalar>;
    const auto tEnd = getParam<Scalar>("TimeLoop.TEnd");
    const auto maxDt = getParam<Scalar>("TimeLoop.MaxTimeStepSize");
    auto dt = getParam<Scalar>("TimeLoop.DtInitial");

    // instantiate time loop
    auto timeLoop = std::make_shared<TimeLoop<Scalar>>(0, dt, tEnd);
    timeLoop->setMaxTimeStepSize(maxDt);

    // set timeloop for the subproblems, needed for boundary value variations
    freeflowProblem->setTimeLoop(timeLoop);
    porousMediumProblem->setTimeLoop(timeLoop);

    // the solution vector
    Traits::SolutionVector sol;
    sol[freeflowCellCenterIdx].resize(freeflowFvGridGeometry->numCellCenterDofs());
    sol[freeflowFaceIdx].resize(freeflowFvGridGeometry->numFaceDofs());
    sol[porousMediumIdx].resize(porousMediumFvGridGeometry->numDofs());

    auto freeflowSol = partial(sol, freeflowFaceIdx, freeflowCellCenterIdx);

    freeflowProblem->applyInitialSolution(freeflowSol);
    porousMediumProblem->applyInitialSolution(sol[porousMediumIdx]);

    auto solOld = sol;

    couplingManager->init(freeflowProblem, porousMediumProblem, sol);

    // TODO: dumux-course-task 3.A:
    // Update static wall properties

    // TODO: dumux-course-task 3.A:
    // Update dynamic wall properties

    // the grid variables
    using FreeflowGridVariables = GetPropType<FreeflowTypeTag, Properties::GridVariables>;
    auto freeflowGridVariables = std::make_shared<FreeflowGridVariables>(freeflowProblem, freeflowFvGridGeometry);
    freeflowGridVariables->init(freeflowSol);
    using PorousMediumGridVariables = GetPropType<PorousMediumTypeTag, Properties::GridVariables>;
    auto porousMediumGridVariables = std::make_shared<PorousMediumGridVariables>(porousMediumProblem, porousMediumFvGridGeometry);
    porousMediumGridVariables->init(sol[porousMediumIdx]);

    // intialize the vtk output module
    const auto freeflowName = getParam<std::string>("Problem.Name") + "_" + freeflowProblem->name();
    const auto porousMediumName = getParam<std::string>("Problem.Name") + "_" + porousMediumProblem->name();

    StaggeredVtkOutputModule<FreeflowGridVariables, decltype(freeflowSol)> freeflowVtkWriter(*freeflowGridVariables, freeflowSol, freeflowName);
    GetPropType<FreeflowTypeTag, Properties::IOFields>::initOutputModule(freeflowVtkWriter);
    freeflowVtkWriter.write(0.0);

    using PorousMediumSolutionVector = GetPropType<PorousMediumTypeTag, Properties::SolutionVector>;
    VtkOutputModule<PorousMediumGridVariables, PorousMediumSolutionVector> porousMediumVtkWriter(*porousMediumGridVariables, sol[porousMediumIdx], porousMediumName);
    using PorousMediumVelocityOutput = GetPropType<PorousMediumTypeTag, Properties::VelocityOutput>;
    porousMediumVtkWriter.addVelocityOutput(std::make_shared<PorousMediumVelocityOutput>(*porousMediumGridVariables));
    GetPropType<PorousMediumTypeTag, Properties::IOFields>::initOutputModule(porousMediumVtkWriter);
    porousMediumVtkWriter.write(0.0);

    // the assembler with time loop for instationary problem
    using Assembler = MultiDomainFVAssembler<Traits, CouplingManager, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(std::make_tuple(freeflowProblem, freeflowProblem, porousMediumProblem),
                                                 std::make_tuple(freeflowFvGridGeometry->faceFVGridGeometryPtr(),
                                                                 freeflowFvGridGeometry->cellCenterFVGridGeometryPtr(),
                                                                 porousMediumFvGridGeometry),
                                                 std::make_tuple(freeflowGridVariables->faceGridVariablesPtr(),
                                                                 freeflowGridVariables->cellCenterGridVariablesPtr(),
                                                                 porousMediumGridVariables),
                                                 couplingManager,
                                                 timeLoop, solOld);

    // the linear solver
    using LinearSolver = UMFPackIstlSolver<SeqLinearSolverTraits, LinearAlgebraTraitsFromAssembler<Assembler>>;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = MultiDomainNewtonSolver<Assembler, LinearSolver, CouplingManager>;
    NewtonSolver nonLinearSolver(assembler, linearSolver, couplingManager);

    // time loop
    timeLoop->start(); do
    {
        // solve the non-linear system with time step control
        nonLinearSolver.solve(sol, *timeLoop);

        // make the new solution the old solution
        solOld = sol;

        // TODO: dumux-course-task 3.A:
        // Update dynamic wall properties

        // post time step treatment of PorousMedium problem
        porousMediumProblem->postTimeStep(sol[porousMediumIdx], *porousMediumGridVariables, timeLoop->timeStepSize());

        // advance grid variables to the next time step
        freeflowGridVariables->advanceTimeStep();
        porousMediumGridVariables->advanceTimeStep();

        // advance to the time loop to the next step
        timeLoop->advanceTimeStep();

        // write vtk output
        freeflowVtkWriter.write(timeLoop->time());
        porousMediumVtkWriter.write(timeLoop->time());

        // report statistics of this time step
        timeLoop->reportTimeStep();

        // set new dt as suggested by newton solver
        timeLoop->setTimeStepSize(nonLinearSolver.suggestTimeStepSize(timeLoop->timeStepSize()));

    } while (!timeLoop->finished());

    timeLoop->finalize(freeflowGridView.comm());
    timeLoop->finalize(porousMediumGridView.comm());

    Parameters::print();

    return 0;
} // end main
