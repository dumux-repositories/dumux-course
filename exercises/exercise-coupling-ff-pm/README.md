# Exercise Coupling free flow/porous medium flow (DuMuX Course)

The aim of this exercise is to get familiar with setting up coupled free flow/porous medium flow problems.

## Problem set-up

> Note: The following problem setup holds for all sub-exercises 1-3.
>
The model domain consists of two non-overlapping two-dimensional subdomains.
Free flow is modeled in the upper subdomain, while the lower subdomain models a flow within a porous medium.
Both single-phase flow and two-phase flow will be considered in the porous domain.


## 0. Getting familiar with the code

* Navigate to the directory `exercises/exercise-coupling-ff-pm`

There are three sub folders: `interface` (contains Exercise 1), `models` (contains Exercise 2) and `turbulence` (contains Exercise 3).

The folders of the three exercises contain the following files:
* A __main file__ (`interface/main.cc`, `models/main.cc`, `turbulence/main.cc`),
* one __problem file for the free-flow domain__ (`interface/freeflowsubproblem.hh`, `models/freeflowsubproblem.hh`, `turbulence/freeflowsubproblem.hh`),
* one __problem file for the porous medium domain__ (`interface/porousmediumsubproblem.hh`, `models/porousmediumsubproblem.hh`, `turbulence/porousmediumsubproblem.hh`),
* one __properties file__ (`interface/properties.hh`, `models/properties.hh`, `turbulence/properties.hh`),
* and one __input file__ (`interface/params.input`, `models/params.input`, `turbulence/params.input`).
Moreover all the exercises share
* the __spatial parameters files__ (`1pspatialparams.hh` and `2pspatialparams.hh`)

In the [Exercise Mainfiles](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-course/-/blob/master/exercises/exercise-mainfile/README.md) the overall structure of a main-file was already introduced. For the coupled setup we need to get properties related to each of the subproblems (free-flow or porous medium flow).
E.g. in the main file, `TypeTags` for both submodels are defined, `FreeflowTypeTag` and `PorousMediumTypeTag`. These `TypeTags` collect all of the properties associated with each subproblem.
```c++
    // Define the sub problem type tags
    using FreeflowTypeTag = Properties::TTag::FreeflowNC;
    using PorousMediumTypeTag = Properties::TTag::PorousMediumOnePNC;
```
The same applies for types such as `GridManager`,
```c++
    // try to create a grid (from the given grid file or the input file)
    // for both sub-domains
    using PorousMediumGridManager = Dumux::GridManager<GetPropType<PorousMediumTypeTag, Properties::Grid>>;
    PorousMediumGridManager porousMediumGridManager;
    porousMediumGridManager.init("PorousMedium"); // pass parameter group

    using FreeflowGridManager = Dumux::GridManager<GetPropType<FreeflowTypeTag, Properties::Grid>>;
    FreeflowGridManager freeflowGridManager;
    freeflowGridManager.init("Freeflow"); // pass parameter group

    // we compute on the leaf grid view
    const auto& porousMediumGridView = porousMediumGridManager.grid().leafGridView();
    const auto& freeflowGridView = freeflowGridManager.grid().leafGridView();
```
`FVGridGeometry`,
```c++
    // create the finite volume grid geometry
    using FreeflowFVGridGeometry = GetPropType<FreeflowTypeTag, Properties::GridGeometry>;
    auto freeflowFvGridGeometry = std::make_shared<FreeflowFVGridGeometry>(freeflowGridView);
    using PorousMediumFVGridGeometry = GetPropType<PorousMediumTypeTag, Properties::GridGeometry>;
    auto porousMediumFvGridGeometry = std::make_shared<PorousMediumFVGridGeometry>(porousMediumGridView);
```
`Problem`, etc...
```c++
    // the problem (initial and boundary conditions)
    using FreeflowProblem = GetPropType<FreeflowTypeTag, Properties::Problem>;
    auto freeflowProblem = std::make_shared<FreeflowProblem>(freeflowFvGridGeometry, couplingManager);
    using PorousMediumProblem = GetPropType<PorousMediumTypeTag, Properties::Problem>;
    auto porousMediumProblem = std::make_shared<PorousMediumProblem>(porousMediumFvGridGeometry, couplingManager);
```

Since we use a monolithic coupling scheme, there is only one `Assembler` and one `NewtonSolver`, which help to assemble and solve the full coupled problem.

The problem files very much look like the "regular", uncoupled problem files seen in previous exercises, with the exception that they hold a pointer to the `CouplingManager`. E.g.
```c++
    FreeFlowSubProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,std::shared_ptr<CouplingManager> couplingManager)
    : ParentType(fvGridGeometry, "Freeflow"),
    eps_(1e-6),
    couplingManager_(couplingManager)
    {
        //...
    }
```
This allows them to evaluate the coupling conditions and to exchange information between the coupled models.
The coupling conditions are realized technically in terms of boundary conditions. For instance,
in `interface/freeflowsubproblem.hh`, `couplingNeumann` boundary conditions are set, which means that the free flow model evaluates the
mass and momentum fluxes coming from the porous domain and uses these values as boundary conditions at the interface.
```c++
    BoundaryTypes boundaryTypes(const Element& element,
                                const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;

        //...

        // coupling interface
        if(couplingManager().isCoupledEntity(CouplingManager::stokesIdx, scvf))
        {
            values.setCouplingNeumann(Indices::conti0EqIdx);
            values.setCouplingNeumann(Indices::momentumYBalanceIdx);
            values.setDirichlet(Indices::velocityXIdx); // assume no slip on interface
        }

        return values;
    }
```
> Note: Certain checks are performed when combining different models, e.g., the fluid system has to be the same for both domains
and the sub-control-volume faces at the interface have to match.
>
We will use a staggered grid (also called Marker-and-Cell method - MAC) to discretize the free-flow domain and a cell-centered finite volume (CCFV) method for the porous medium domain.
Keep in mind that the staggered grid implementation distinguishes between face variables (velocity components) and cell center variables (all other variables).
For this reason one distinguishes between `CouplingManager::stokesCellCenterIdx` and `CouplingManager::stokesFaceIdx` indices (see `main.cc`), while for the porous medium all variables can be accessed with `CouplingManager::darcyIdx`.

```c++
    // the indices
    constexpr auto freeflowCellCenterIdx = CouplingManager::stokesCellCenterIdx;
    constexpr auto freeflowFaceIdx = CouplingManager::stokesFaceIdx;
    constexpr auto porousMediumIdx = CouplingManager::darcyIdx;
```

__Task__:
Take a closer look at the above listed files before moving to the three exercises below.

## Sub-Exercises

* [**Exercise 1:** Changing the interface between the free- and the porous medium domain](./interface/README.md)
* [**Exercise 2:** Changing the porous medium model](./models/README.md)
* [**Exercise 3:** Introducing a turbulence model in the free flow domain](./turbulence/README.md)
