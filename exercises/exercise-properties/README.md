# Exercise Properties (DuMuX course)
<br>

## Problem set-up

The problem setup is identical to the two-phase incompressible test from DuMu<sup>x</sup>.

## Preparing the exercise

* Navigate to the directory `exercise-properties`

_Exercise Properties_ deals with a two-phase immiscible incompressible problem (__2p__). The goal is to learn how to adapt compile-time parameters by employing the _DuMu<sup>x</sup> property system_.

<br><br>
### Task 1: Getting familiar with the code
<hr>

Locate all the files you will need for this exercise
* The __main file__: `main.cc`
* The __problem file__: `problem.hh`
* The __properties file__: `properties.hh`
* The __spatial parameters file__: `spatialparams.hh`
* The __input file__: `params.input`
* One header file containing:
  * a custom __local residual__ in: `mylocalresidual.hh`


<br><br><br>
### Task 2: Compiling and running the program
<hr>

* Change to the build-directory

```bash
cd ../../build-cmake/exercises/exercise-properties
```

* Compile the executable `exercise_properties`

```bash
make exercise_properties
```

* Run the problem and inspect the result

```bash
./exercise_properties
```
Note: Because the input file has the same name as the executable, DuMu<sup>x</sup> will find it automatically.


<br><br><br>
### Task 3: Implement a custom local residual
<hr>

Types that are properties can be changed on the problem level by using the property system. In the following task, we implement our own 2p local residual, i.e. the class that computes the element residual in every Newton iteration. The file `mylocalresidual.hh` contains a copy of the original local residual class used for all immiscible models renamed to `template<class TypeTag> class MyLocalResidual`.

* Make DuMu<sup>x</sup> use this new local residual by including the header `mylocalresidual.hh` and setting the corresponding property in the `Properties` namespace in the file `properties.hh`

```c++

template<class TypeTag>
struct LocalResidual<TypeTag, TTag::TwoPIncompressible>
{
    using type = MyLocalResidual<TypeTag>;
};
```

Simplify the original local residual by using the assumption that only incompressible fluid phases are present. As a consequence, one can balance phase volume instead of phase mass:

* Eliminate the density from the `computeStorage` and the `computeFlux` methods.

* Adapt the relevant routine in the problem file such that volume instead of mass is injected. The density of the employed NAPL is 1460 kg/m<sup>3</sup> . For a first try, you can use the hardcoded value.

* Generalize your approach by using the component's `liquidDensity(t, p)` function instead of the hardcoded value.
