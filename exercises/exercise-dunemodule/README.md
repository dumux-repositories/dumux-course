# Exercise New Dune Module (DuMuX course)

This exercise describes how to create a new DuMuX module
and how to create a corresponding GitLab project.

This is the suggested
workflow to develop code on top of DuMuX.

## Task 1: Create new dune module

* Execute the following command (bash environment) in the top-folder, i.e. above the dumux folder

```bash
./dune-common/bin/duneproject
```

* Follow the introductions and specify
    * as name of the new module: `dumux-example`
    * as module dependencies: `dumux`
    * a version at your choice (the version of your project, not of dumux.)
    * your email address


## Task 2: Rerun dunecontrol to configure your new project

The following command will configure your new module

```bash
./dune-common/bin/dunecontrol --opts=dumux/cmake.opts --only=<module-name> all
```

You need to run this command in the folder with content dumux, dumux-course, dune-common, dune-geometry, dune-grid, dune-istl, etc. `<module-name>` needs to be replaced (please replace the angle brackets also) by the name of the module, e.g., by `dumux-example`.

## Task 3: Create a new test case within your new DuMuX module

* Create a new folder (in your module folder), e.g., `appl`

```bash
mkdir appl
```

* Copy some test case from the dumux module, e.g., test_1p from test/porousmediumflow/1p/compressible/stationary in your new folder (e.g., `appl`)
* Copy the problem file, spatialparams file, properties file, cc source file and input file

* Adjust the CMakeLists.txt file within the dumux-example (or your module name)-folder to include your new subdirectory

* Add a new CMakeLists.txt in the folder `appl` with the content

```cmake
# add a new finite volume 1p test
dumux_add_test(NAME test_1p_compressible_stationary_tpfa
               SOURCES main.cc
               COMPILE_DEFINITIONS TYPETAG=OnePCompressibleTpfa
               CMD_ARGS params.input)

# add a symlink for the input file
dune_symlink_to_source_files(FILES "params.input")

```

* Reconfigure your module by running in the topmost directory of your new module

```bash
cmake build-cmake
```

* Build and execute the test problem

```bash
cd build-cmake
make build_tests
cd appl
./test_1p_compressible_stationary_tpfa params.input
```


## Task 4: Create a new GitLab project


* Login with your username and password at https://git.iws.uni-stuttgart.de/

Note: If you don't have an account create one. We allow anyone to host repositories
on our GitLab instance as long as it is DuMuX related. If you created a GitLab account recently or just now, one of
the administrators first has to increase the number of permitted personal projects for your account before you can create
your own project.

* Click the **New project** button.

* Then choose to **Create blank project**.

* Specify your project name, e.g., <module-name>, untick the box *Initialize repository with a README* and click the **Create project** button.

* Follow the given instructions for an *existing folder*.

Hint: if you have not done so already, be sure to inform your computer of your git account with the following commands:
```bash
cd <module-name>
git config --global user.name "FIRST_NAME LAST_NAME"
git config --global user.email "YOUR_EMAIL_ADDRESS"
git init --initial-branch=main
git remote add origin https://git.iws.uni-stuttgart.de/<Namespace>/<module-name>.git
```

**Important**: Before executing the `git add .` command, you should add your cmake build folder to `.gitignore`.
The easiest way to do so is to copy the `.gitignore` file from the dumux module into your module path. If everything
worked, executing `git status` should not show `build-cmake` anymore. Never put your executables or other build files
under version control. Only source files (`*.hh`, `*.cc`, `*.input`, `CMakeLists.txt`) should be under version control.

Then you can commit and push your new module to your repository:
```bash
git add .
git commit -m "Initial commit"
git push -u origin main
```
