// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Course contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//

#include <config.h>
#include <type_traits>

#include <dune/grid/yaspgrid.hh>

#include <dumux/common/initialize.hh>
#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/numeqvector.hh>
#include <dumux/common/boundarytypes.hh>
#include <dumux/common/fvproblem.hh>

#include <dumux/io/rasterimagereader.hh>
#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/io/grid/gridmanager_yasp.hh>

#include <dumux/discretization/box.hh>

#include <dumux/linear/linearsolvertraits.hh>
#include <dumux/linear/linearalgebratraits.hh>
#include <dumux/linear/istlsolvers.hh>
#include <dumux/nonlinear/newtonsolver.hh>
#include <dumux/assembly/fvassembler.hh>

// TODO: dumux-course-task 3.1: include the header model.hh

/*!
 * \ingroup NonlinearDiffusion
 * \brief Test problem for image denoising using a nonlinear diffusion model
 */
namespace Dumux {
template<class TypeTag>
class NonlinearDiffusionTestProblem : public FVProblem<TypeTag>
{
    using ParentType = FVProblem<TypeTag>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename GridGeometry::LocalView::Element::Geometry::GlobalCoordinate;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;

public:
    NonlinearDiffusionTestProblem(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    {
        conductance_ = getParam<Scalar>("Problem.Conductance");
    }

    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        values.setAllNeumann();
        return values;
    }

    NumEqVector neumannAtPos(const GlobalPosition& globalPos) const
    { return { 0.0 }; }

    Scalar conductance() const
    { return conductance_; }

private:
    Scalar conductance_;
};
} // end namespace Dumux

// Property tag and specializations
namespace Dumux::Properties::TTag {

struct NonlinearDiffusionTest
{
    // TODO: dumux-course-task 3.1: we need to set our model by replacing ModelTypeTag
    using InheritsFrom = std::tuple</*ModelTypeTag,*/BoxModel>;

    using Scalar = double;
    using Grid = Dune::YaspGrid<2>;

    // Set the above implemented problem
    template<class TypeTag>
    using Problem = NonlinearDiffusionTestProblem<TypeTag>;

    using EnableGridVolumeVariablesCache = std::true_type;
    using EnableGridFluxVariablesCache = std::true_type;
    using EnableGridGeometryCache = std::true_type;
};

} // end namespace Dumux::Properties::TTag


// Create the initial solution
// Given by normalized image pixel data to the range [0,1]
template<class SolutionVector, class GridGeometry, class ImageData>
SolutionVector createInitialSolution(const GridGeometry& gg, const ImageData& data)
{
    SolutionVector sol(gg.numDofs());
    for (int n = 0; n < sol.size(); ++n)
        sol[n][0] = double(data[n])/data.header().maxValue;
    return sol;
}

// the main program
int main(int argc, char** argv)
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    Dumux::initialize(argc, argv);

    // initialize parameter tree
    Parameters::init(argc, argv);

    // define the type tag for this problem.
    using TypeTag = Properties::TTag::NonlinearDiffusionTest;

    using Grid = GetPropType<TypeTag, Properties::Grid>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;

    // Read the image
    const auto imageFileName = getParam<std::string>("ImageFile");
    const auto imageData = NetPBMReader::readPGM(imageFileName);
    std::array<int, 2> cells{{ static_cast<int>(imageData.header().nCols)-1, static_cast<int>(imageData.header().nRows)-1 }};
    Dune::FieldVector<double, 2> upperRight({ double(cells[0]), double(cells[1]) });

    GridManager<Grid> gridManager;
    gridManager.init(upperRight, cells);

    // We, create the finite volume grid geometry from the (leaf) grid view,
    // the problem for the boundary conditions, a solution vector and a grid variables instance.
    auto gridGeometry = std::make_shared<GridGeometry>(gridManager.grid().leafGridView());

    // TODO: dumux-course-task 3.2: uncomment when the model is implemented
    // using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    // using Problem = GetPropType<TypeTag, Properties::Problem>;
    // using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    // using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;

    // auto problem = std::make_shared<Problem>(gridGeometry);
    // auto sol = createInitialSolution<SolutionVector>(*gridGeometry, imageData);
    // auto gridVariables = std::make_shared<GridVariables>(problem, gridGeometry);
    // gridVariables->init(sol);

    // // We initialize the VTK output module and write out the initial concentration field
    // VtkOutputModule<GridVariables, SolutionVector> vtkWriter(*gridVariables, sol, problem->name());
    // vtkWriter.addVolumeVariable([](const auto& vv){ return vv.priVar(0); }, "imageIntensity");
    // vtkWriter.write(0.0);

    // // We instantiate time loop using start and end time as well as
    // // the time step size from the parameter tree (`params.input`)
    // auto timeLoop = std::make_shared<CheckPointTimeLoop<Scalar>>(
    //     getParam<Scalar>("TimeLoop.TStart", 0.0),
    //     getParam<Scalar>("TimeLoop.Dt"),
    //     getParam<Scalar>("TimeLoop.TEnd")
    // );

    // // Next, we choose the type of assembler, linear solver and nonlinear solver
    // // and construct instances of these classes.
    // using Assembler = FVAssembler<TypeTag, DiffMethod::numeric>;
    // using LinearSolver = SSORCGIstlSolver<LinearSolverTraits<GridGeometry>, LinearAlgebraTraitsFromAssembler<Assembler>>;
    // using Solver = Dumux::NewtonSolver<Assembler, LinearSolver>;

    // auto oldSol = sol; // copy the vector to store state of previous time step
    // auto assembler = std::make_shared<Assembler>(problem, gridGeometry, gridVariables, timeLoop, oldSol);
    // auto linearSolver = std::make_shared<LinearSolver>(gridGeometry->gridView(), gridGeometry->dofMapper());
    // Solver solver(assembler, linearSolver);

    // // time loop
    // timeLoop->start(); do
    // {
    //     // assemble & solve
    //     solver.solve(sol);

    //     // make the new solution the old solution
    //     oldSol = sol;
    //     gridVariables->advanceTimeStep();

    //     // advance to the time loop to the next step
    //     timeLoop->advanceTimeStep();

    //     // write VTK output (writes out the concentration field)
    //     vtkWriter.write(timeLoop->time());

    //     // report statistics of this time step
    //     timeLoop->reportTimeStep();

    // } while (!timeLoop->finished());

    // timeLoop->finalize(gridGeometry->gridView().comm());

    return 0;
}// end main
