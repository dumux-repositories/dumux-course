# DuMu<sup>x</sup> course exercises

The DuMu<sup>x</sup> course comprises the following exercises. Each exercise folder contains a detailed description of the tasks
(best viewed online by following the links) and the source files to work on.

### [:open_file_folder: Basics](./exercise-basic/README.md)

Based on a scenario where gas is injected into an aquifer, you learn how to

* compile and run an executable,
* see the difference of an immiscible two-phase model compared to a two-phase two-component model,
* set up a new executable,
* set up a non-isothermal test problem based on the isothermal test problem,
* set boundary conditions.

:tv: [Lecture slides corresponding to exercise](https://pages.iws.uni-stuttgart.de/dumux-repositories/dumux-course/problem.html#/example-application)

### [:open_file_folder: Main file](./exercise-mainfile/README.md)

In this exercise, you learn how to

* find your way in the main file,
* solve a stationary, linear system,
* solve an instationary, linear system,
* solve an instationary, nonlinear system,
* apply analytic differentiation.

:tv: [Lecture slides corresponding to exercise](https://pages.iws.uni-stuttgart.de/dumux-repositories/dumux-course/problem.html#/main-program-and-main-function)

### [:open_file_folder: Runtime parameters](./exercise-runtimeparams/README.md)

This exercise covers the following topics: You learn how to

* use different input files with one executable,
* set variables to collect runtime parameters,
* use and change default values for runtime parameters.

:tv: [Lecture slides corresponding to exercise](https://pages.iws.uni-stuttgart.de/dumux-repositories/dumux-course/params.html#/runtime-parameters)

### [:open_file_folder: Grids](./exercise-grids/README.md)

This exercise guides you through the following tasks:

* Apply a global grid refinement,
* change the grid type,
* apply grid zoning and grading,
* read in structured and unstructured grids from external files.

:tv: [Lecture slides corresponding to exercise](https://pages.iws.uni-stuttgart.de/dumux-repositories/dumux-course/grid.html#/grids)

### [:open_file_folder: Properties](./exercise-properties/README.md)

In this exercise, you learn how to adjust the properties in order to use a customized local residual.

:tv: [Lecture slides corresponding to exercise](https://pages.iws.uni-stuttgart.de/dumux-repositories/dumux-course/properties.html#/title-slide)

### [:open_file_folder: Fluid systems](./exercise-fluidsystem/README.md)

This exercise covers the handling of phases and components in DuMu<sup>x</sup>. You learn

* how to implement and use a new, customized component,
* how to implement a new fluidsystem,
* and how to change the wettability of the porous medium.

:tv: [Lecture slides corresponding to exercise](https://pages.iws.uni-stuttgart.de/dumux-repositories/dumux-course/materialsystem.html#/title-slide)

### [:open_file_folder: Dune module](./exercise-dunemodule/README.md)

You learn how to

* create a new dune module which depends on the dumux module,
* create a new GitLab project.

:tv: [Lecture slides corresponding to exercise](https://pages.iws.uni-stuttgart.de/dumux-repositories/dumux-course/dunemodule.html#/custom-dune-module)


### [:open_file_folder: Model](./exercise-model/README.md)

This exercise covers the implementation of new models in DuMu<sup>x</sup>. You learn

* how to design a nonlinear diffusion equation model from scratch (almost)
* how to implement a local residual using a minimal model design
* use DuMu<sup>x</sup> to denoise an image (!)

:tv: [Lecture slides corresponding to exercise](https://pages.iws.uni-stuttgart.de/dumux-repositories/dumux-course/model.html#/title-slide)

### [:open_file_folder: Coupling free and porous medium flow](./exercise-coupling-ff-pm/README.md)

This exercise is related to the [SFB1313 Project Area A](https://www.sfb1313.uni-stuttgart.de/research-areas/)

You learn how to

* use a coupled problem set-up,
* change coupling conditions between the two domains, porous-medium flow and free flow,
* change the shape of the interface between the two domains,
* change the model in the porous-medium domain,
* change the model in the free-flow domain.

:tv: [Introduction to multidomain](https://pages.iws.uni-stuttgart.de/dumux-repositories/dumux-course/multidomain.html#/title-slide)
:tv: [Lecture slides corresponding to exercise](https://pages.iws.uni-stuttgart.de/dumux-repositories/dumux-course/coupled_ff-pm.html#/title-slide)

### [:open_file_folder: Discrete fracture modeling](./exercise-fractures/README.md)

This exercise is related to the [SFB1313 Project Area B](https://www.sfb1313.uni-stuttgart.de/research-areas/)

You learn how to

* use a problem containing embedded, discrete fractures,
* change the properties of the fractures,
* use domain markers to set internal boundary conditions.

:tv: [Introduction to multidomain](https://pages.iws.uni-stuttgart.de/dumux-repositories/dumux-course/multidomain.html#/title-slide)
:tv: [Lecture slides corresponding to exercise](https://pages.iws.uni-stuttgart.de/dumux-repositories/dumux-course/fractures.html#/title-slide)

### [:open_file_folder: Biomineralization](./exercise-biomineralization/README.md)

This exercise is related to the [SFB1313 Project Area C](https://www.sfb1313.uni-stuttgart.de/research-areas/)

You learn how to

* include a chemical reaction in the problem setup,
* apply the mineralization model, i.e. add balance equations to describe dynamic solid phases.
* compare different simulation results using a programmable filter in Paraview

:tv: [Introduction to multidomain](https://pages.iws.uni-stuttgart.de/dumux-repositories/dumux-course/multidomain.html#/title-slide)
:tv: [Lecture slides corresponding to exercise](https://pages.iws.uni-stuttgart.de/dumux-repositories/dumux-course/biomin.html#/title-slide)
