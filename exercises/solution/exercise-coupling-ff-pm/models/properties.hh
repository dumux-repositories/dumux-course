// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Course contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 *
 * \brief Properties file for coupled models example.
 */
#ifndef DUMUX_EXERCISE_COUPLED_MODELS_PROPERTIES_HH
#define DUMUX_EXERCISE_COUPLED_MODELS_PROPERTIES_HH

// Both Domains
#include <dune/grid/yaspgrid.hh>
#include <dumux/multidomain/staggeredtraits.hh>
#include <dumux/multidomain/boundary/stokesdarcy/couplingmanager.hh>

#include <dumux/io/gnuplotinterface.hh>
#include <dumux/material/fluidsystems/1padapter.hh>
#include <dumux/material/fluidsystems/h2oair.hh>

// Porous medium flow domain
#include <dumux/discretization/cctpfa.hh>
#include <dumux/material/fluidmatrixinteractions/diffusivityconstanttortuosity.hh>

#if EXNUMBER >= 1
#include <dumux/porousmediumflow/2pnc/model.hh>
#include "../2pspatialparams.hh"
#else
#include <dumux/porousmediumflow/1pnc/model.hh>
#include "../1pspatialparams.hh"
#endif

#include "porousmediumsubproblem.hh"

// Free-flow
#include <dumux/discretization/staggered/freeflow/properties.hh>
#include <dumux/freeflow/compositional/navierstokesncmodel.hh>

#include "freeflowsubproblem.hh"

namespace Dumux::Properties {

// Create new type tags
namespace TTag {
struct FreeflowNC { using InheritsFrom = std::tuple<NavierStokesNC, StaggeredFreeFlowModel>; };
#if EXNUMBER >= 1
struct PorousMediumOnePNC { using InheritsFrom = std::tuple<TwoPNC, CCTpfaModel>; };
#else
struct PorousMediumOnePNC { using InheritsFrom = std::tuple<OnePNC, CCTpfaModel>; };
#endif
} // end namespace TTag

// Set the coupling manager
template<class TypeTag>
struct CouplingManager<TypeTag, TTag::FreeflowNC>
{
    using Traits = StaggeredMultiDomainTraits<TypeTag, TypeTag, Properties::TTag::PorousMediumOnePNC>;
    using type = Dumux::StokesDarcyCouplingManager<Traits>;
};
template<class TypeTag>
struct CouplingManager<TypeTag, TTag::PorousMediumOnePNC>
{
    using Traits = StaggeredMultiDomainTraits<Properties::TTag::FreeflowNC, Properties::TTag::FreeflowNC, TypeTag>;
    using type = Dumux::StokesDarcyCouplingManager<Traits>;
};

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::PorousMediumOnePNC> { using type = Dumux::PorousMediumSubProblem<TypeTag>; };
template<class TypeTag>
struct Problem<TypeTag, TTag::FreeflowNC> { using type = Dumux::FreeFlowSubProblem<TypeTag> ; };

// The fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::PorousMediumOnePNC>
{
    using H2OAir = FluidSystems::H2OAir<GetPropType<TypeTag, Properties::Scalar>>;
#if EXNUMBER == 0
    using type = FluidSystems::OnePAdapter<H2OAir, H2OAir::gasPhaseIdx>;
#else
    using type = H2OAir;
#endif
};
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::FreeflowNC>
{
    using H2OAir = FluidSystems::H2OAir<GetPropType<TypeTag, Properties::Scalar>>;
    using type = FluidSystems::OnePAdapter<H2OAir, H2OAir::gasPhaseIdx>;
};

// Use moles
template<class TypeTag>
struct UseMoles<TypeTag, TTag::PorousMediumOnePNC> { static constexpr bool value = true; };
template<class TypeTag>
struct UseMoles<TypeTag, TTag::FreeflowNC> { static constexpr bool value = true; };

// Do not replace one equation with a total mass balance
template<class TypeTag>
struct ReplaceCompEqIdx<TypeTag, TTag::PorousMediumOnePNC> { static constexpr int value = 3; };
template<class TypeTag>
struct ReplaceCompEqIdx<TypeTag, TTag::FreeflowNC> { static constexpr int value = 3; };

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::PorousMediumOnePNC> { using type = Dune::YaspGrid<2>; };
template<class TypeTag>
struct Grid<TypeTag, TTag::FreeflowNC> { using type = Dune::YaspGrid<2, Dune::EquidistantOffsetCoordinates<GetPropType<TypeTag, Properties::Scalar>, 2> >; };

//! Use a model with constant tortuosity for the effective diffusivity
template<class TypeTag>
struct EffectiveDiffusivityModel<TypeTag, TTag::PorousMediumOnePNC>
{ using type = DiffusivityConstantTortuosity<GetPropType<TypeTag, Properties::Scalar>>; };

#if EXNUMBER >= 1
//! Set the default formulation to pw-Sn: This can be over written in the problem.
template<class TypeTag>
struct Formulation<TypeTag, TTag::PorousMediumOnePNC>
{ static constexpr auto value = TwoPFormulation::p1s0; };
#endif

// Set the spatial parameters type
#if EXNUMBER >= 1
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::PorousMediumOnePNC> {
    using type = TwoPSpatialParams<GetPropType<TypeTag, GridGeometry>, GetPropType<TypeTag, Scalar>>;
};
#else
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::PorousMediumOnePNC> {
    using type = OnePSpatialParams<GetPropType<TypeTag, GridGeometry>, GetPropType<TypeTag, Scalar>>;
};
#endif

template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::FreeflowNC> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::FreeflowNC> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::FreeflowNC> { static constexpr bool value = true; };

} // end namespace Dumux::Properties

#endif
