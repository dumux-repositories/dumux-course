// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Course contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 *
 * \brief A simple Darcy test problem (cell-centered finite volume method).
 */
#ifndef DUMUX_POROUSMEDIUMFLOW_SUBPROBLEM_HH
#define DUMUX_POROUSMEDIUMFLOW_SUBPROBLEM_HH

#include <dumux/common/properties.hh>
#include <dumux/common/boundarytypes.hh>
#include <dumux/common/timeloop.hh>
#include <dumux/common/numeqvector.hh>
#include <dumux/io/gnuplotinterface.hh>
#include <dumux/common/metadata.hh>

#include <dumux/porousmediumflow/problem.hh>

namespace Dumux {

/*!
 * \brief The porous medium flow sub problem
 */
template <class TypeTag>
class PorousMediumSubProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using ElementFluxVariablesCache = typename GridVariables::GridFluxVariablesCache::LocalView;

    // copy some indices for convenience
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    // grid and world dimension
    static constexpr int dim = GridView::dimension;
    static constexpr int dimworld = GridView::dimensionworld;

    // primary variable indices
    static constexpr int conti0EqIdx = Indices::conti0EqIdx;
    static constexpr int pressureIdx = Indices::pressureIdx;
#if EXNUMBER >= 3
    static constexpr int saturationIdx = Indices::switchIdx;
    static constexpr int transportCompIdx = Indices::switchIdx;
#elif EXNUMBER >= 1
    static constexpr int transportCompIdx = Indices::switchIdx;
#else
    static constexpr int phaseIdx = 0;
    static constexpr int transportCompIdx = 1;
#endif

    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = Dune::FieldVector<Scalar, dimworld>;

    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;
    using TimeLoopPtr = std::shared_ptr<TimeLoop<Scalar>>;

public:
    PorousMediumSubProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                           std::shared_ptr<CouplingManager> couplingManager)
    : ParentType(fvGridGeometry, "PorousMedium"),
    eps_(1e-7),
    couplingManager_(couplingManager)
    {
#if EXNUMBER >= 3
        saturation_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.Saturation");
#else
        moleFraction_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.MoleFraction");
#endif

        // initialize storage output file (.csv)
        exportStorage_ = getParamFromGroup<bool>(this->paramGroup(), "Problem.ExportStorage", false);
        plotStorage_ = getParamFromGroup<bool>(this->paramGroup(), "Problem.PlotStorage", false);
        if (exportStorage_)
        {
            storageFileName_ = "storage_" + getParam<std::string>("Problem.Name") + "_" + this->name() + ".csv";
            initializeStorageOutput();
        }

        // initialize flux output file (.json)
        exportFluxes_ = getParamFromGroup<bool>(this->paramGroup(), "Problem.ExportFluxes", false);
        if (exportFluxes_)
        {
            fluxFileName_ = "flux_" + getParam<std::string>("Problem.Name");
            simulationKey_ = getParam<std::string>("Problem.Name", "case1");
            initializeFluxOutput();
        }
    }

    void initializeStorageOutput()
    {
        storageFile_.open(storageFileName_);
        storageFile_ << "#Time[s]" << ";"
                     << "WaterMass[kg]" << ";"
                     << "WaterMassLoss[kg]" << ";"
                     << "EvaporationRate[mm/d]"
                     << std::endl;
    }

    void initializeFluxOutput()
    {
        Dumux::MetaData::Collector collector;
        if (Dumux::MetaData::jsonFileExists(fluxFileName_))
            Dumux::MetaData::readJsonFile(collector, fluxFileName_);
        Dumux::MetaData::writeJsonFile(collector, fluxFileName_);
    }

    template<class SolutionVector, class GridVariables>
    void startStorageEvaluation(const SolutionVector& curSol,
                                const GridVariables& gridVariables)
    {
#if EXNUMBER >= 2
        initialWaterContent_ = evaluateWaterMassStorageTerm(curSol, gridVariables);
        lastWaterMass_ = initialWaterContent_;
#endif
    }

    void startFluxEvaluation()
    {
        // Get Interface Data
        std::vector<GlobalPosition> faceLocation;
        std::vector<Scalar> faceArea;

        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);

            for (auto&& scvf : scvfs(fvGeometry))
            {
                if (!couplingManager().isCoupledEntity(CouplingManager::darcyIdx, scvf))
                    continue;
                faceLocation.push_back(scvf.ipGlobal());
                faceArea.push_back(scvf.area());
            }
        }
        std::vector<Scalar> faceEvaporation(faceArea.size(), 0.0);

        Dumux::MetaData::Collector collector;
        if (Dumux::MetaData::jsonFileExists(fluxFileName_))
            Dumux::MetaData::readJsonFile(collector, fluxFileName_);
        collector[simulationKey_]["Time"]= {0.0};
        collector[simulationKey_]["TimeStepIdx"]= {0};
        collector[simulationKey_]["FaceCenterCoordinates"] = faceLocation;
        collector[simulationKey_]["FaceLength"] = faceArea;
        collector[simulationKey_]["FaceEvaporation"] = {faceEvaporation};
        collector[simulationKey_]["FluxOverFullSurface"] = {0.0};
        Dumux::MetaData::writeJsonFile(collector, fluxFileName_);
    }

    template<class SolutionVector, class GridVariables>
    void postTimeStep(const SolutionVector& curSol,
                      const GridVariables& gridVariables)

    {
        evaluateWaterMassStorageTerm(curSol, gridVariables);
        evaluateInterfaceFluxes(curSol, gridVariables);

        if (exportStorage_ && plotStorage_)
        {
            gnuplotStorage_.resetPlot();
            gnuplotStorage_.setDatafileSeparator(';');
            gnuplotStorage_.setXlabel("time [d]");
            gnuplotStorage_.setXRange(0.0, getParam<Scalar>("TimeLoop.TEnd"));
            gnuplotStorage_.setYlabel("evaporation rate [mm/d]");
            gnuplotStorage_.setOption("set yrange [0.0:]");
            gnuplotStorage_.setOption("set y2label 'cumulative mass loss'");
            gnuplotStorage_.setOption("set y2range [0.0:0.5]");
            gnuplotStorage_.setOption("set y2range [0.0:0.5]");
            gnuplotStorage_.addFileToPlot(storageFileName_, "using 1:4 with lines title 'evaporation rate'");
            gnuplotStorage_.addFileToPlot(storageFileName_, "using 1:3 axes x1y2 with lines title 'cumulative mass loss'");
            gnuplotStorage_.plot("StorageOverTime");
        }
    }

    template<class SolutionVector, class GridVariables>
    Scalar evaluateWaterMassStorageTerm(const SolutionVector& curSol,
                                        const GridVariables& gridVariables)

    {
        // compute the mass in the entire domain
        Scalar waterMass = 0.0;

        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);

            auto elemVolVars = localView(gridVariables.curGridVolVars());
            elemVolVars.bindElement(element, fvGeometry, curSol);

            for ([[maybe_unused]] auto&& scv : scvs(fvGeometry))
            {
                // calculation of the water mass over all present phases
                for(int phaseIdx = 0; phaseIdx < FluidSystem::numPhases; ++phaseIdx)
                {
#if EXNUMBER >= 2
                    const auto& volVars = elemVolVars[scv];
                    waterMass += volVars.massFraction(phaseIdx, FluidSystem::H2OIdx) * volVars.density(phaseIdx)
                                 * volVars.saturation(phaseIdx) * volVars.porosity()
                                 * scv.volume() * volVars.extrusionFactor();
#else
                    waterMass += 0.0;
#endif
                }
            }
        }
#if EXNUMBER >= 2
        std::cout << "Mass of water is: " << waterMass << std::endl;
#endif

        Scalar cumMassLoss = initialWaterContent_ - waterMass;
        Scalar evaporationRate = (lastWaterMass_ - waterMass) * 86400
                                 / (this->gridGeometry().bBoxMax()[0] - this->gridGeometry().bBoxMin()[0])
                                 / timeLoop_->timeStepSize();
        lastWaterMass_ = waterMass;

        if (exportStorage_)
        {
            storageFile_ << timeLoop_->time() << ";"
                         << waterMass << ";"
                         << cumMassLoss << ";"
                         << evaporationRate
                         << std::endl;
        }

        return waterMass;
    }

    template<class SolutionVector, class GridVariables>
    void evaluateInterfaceFluxes(const SolutionVector& curSol,
                                 const GridVariables& gridVariables)

    {
        std::vector<Scalar> faceEvaporation;

        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);

            auto elemVolVars = localView(gridVariables.curGridVolVars());
            elemVolVars.bindElement(element, fvGeometry, curSol);

            for (auto&& scvf : scvfs(fvGeometry))
            {
                if (!couplingManager().isCoupledEntity(CouplingManager::darcyIdx, scvf))
                    continue;

#if EXNUMBER >= 2
                NumEqVector flux = couplingManager().couplingData().massCouplingCondition(element, fvGeometry, elemVolVars, scvf)
                                   * scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor() * FluidSystem::molarMass(1) * -1.0 * 86400.0;
#else
                NumEqVector flux(0.0);
#endif
                faceEvaporation.push_back(flux[transportCompIdx]);
            }
        }
        Scalar fluxOverFullSurface = std::accumulate(faceEvaporation.begin(), faceEvaporation.end(), 0.0);

        if (exportFluxes_)
        {
            Dumux::MetaData::Collector collector;
            if (Dumux::MetaData::jsonFileExists(fluxFileName_))
                Dumux::MetaData::readJsonFile(collector, fluxFileName_);
            collector[simulationKey_]["Time"].push_back(time());
            collector[simulationKey_]["TimeStepIdx"].push_back(timeStepIndex());
            collector[simulationKey_]["FaceEvaporation"].push_back(faceEvaporation);
            collector[simulationKey_]["FluxOverFullSurface"].push_back(fluxOverFullSurface);
            Dumux::MetaData::writeJsonFile(collector, fluxFileName_);
            std::cout << "Total flux per local method = " << fluxOverFullSurface << "\n";
        }
    }

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     *
     * \param element The element
     * \param scvf The boundary sub control volume face
     */
    BoundaryTypes boundaryTypes(const Element& element, const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;
        values.setAllNeumann();

        if (couplingManager().isCoupledEntity(CouplingManager::darcyIdx, scvf))
            values.setAllCouplingNeumann();

        return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for a Neumann control volume.
     *
     * \param element The element for which the Neumann boundary condition is set
     * \param fvGeomentry The fvGeometry
     * \param elemVolVars The element volume variables
     * \param scvf The boundary sub control volume face
     *
     * For this method, the \a values variable stores primary variables.
     */
    template<class ElementVolumeVariables>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFluxVariablesCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector values(0.0);

        if (couplingManager().isCoupledEntity(CouplingManager::darcyIdx, scvf))
            values = couplingManager().couplingData().massCouplingCondition(element, fvGeometry, elemVolVars, scvf);

        return values;
    }

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * \param element The element for which the source term is set
     * \param fvGeomentry The fvGeometry
     * \param elemVolVars The element volume variables
     * \param scv The subcontrolvolume
     */
    template<class ElementVolumeVariables>
    NumEqVector source(const Element& element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume& scv) const
    { return NumEqVector(0.0); }

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param element The element
     *
     * For this method, the \a priVars parameter stores primary
     * variables.
     */
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {
        static const Scalar freeflowPressure = getParamFromGroup<Scalar>("Freeflow", "Problem.Pressure");

        PrimaryVariables values(0.0);
        values[pressureIdx] = freeflowPressure;

#if EXNUMBER >= 3
        values.setState(3/*bothPhases*/);
        values[saturationIdx] = saturation_;
#elif EXNUMBER >= 1
        values.setState(2/*secondPhaseOnly*/);
        values[transportCompIdx] = moleFraction_;
#else
        values[transportCompIdx] = moleFraction_;
#endif

        return values;
    }

    //! Set the coupling manager
    void setCouplingManager(std::shared_ptr<CouplingManager> cm)
    { couplingManager_ = cm; }

    //! Get the coupling manager
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

    void setTimeLoop(TimeLoopPtr timeLoop)
    { timeLoop_ = timeLoop; }

    Scalar time() const
    { return timeLoop_->time(); }

    int timeStepIndex() const
    { return timeLoop_->timeStepIndex(); }

private:
    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] < this->gridGeometry().bBoxMin()[0] + eps_; }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] > this->gridGeometry().bBoxMax()[0] - eps_; }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] < this->gridGeometry().bBoxMin()[1] + eps_; }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] > this->gridGeometry().bBoxMax()[1] - eps_; }

    Scalar eps_;
#if EXNUMBER >= 3
    Scalar saturation_;
#else
    Scalar moleFraction_;
#endif

    TimeLoopPtr timeLoop_;
    std::shared_ptr<CouplingManager> couplingManager_;

    // Storage calculation and output
    Scalar initialWaterContent_ = 0.0;
    Scalar lastWaterMass_ = 0.0;
    std::string storageFileName_;
    std::ofstream storageFile_;
    bool exportStorage_;
    bool plotStorage_;
    Dumux::GnuplotInterface<Scalar> gnuplotStorage_;

    // Flux output
    std::string fluxFileName_;
    std::string simulationKey_;
    bool exportFluxes_;
};

} //end namespace Dumux

#endif
