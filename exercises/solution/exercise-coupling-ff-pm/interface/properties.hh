// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Course contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \brief The coupled exercise properties file or the interface case.
 */
#ifndef DUMUX_EXERCISE_COUPLED_INTERFACE_PROPERTIES_HH
#define DUMUX_EXERCISE_COUPLED_INTERFACE_PROPERTIES_HH

// Both Domains
#include <dune/grid/yaspgrid.hh>
#include <dumux/multidomain/staggeredtraits.hh>
#include <dumux/multidomain/boundary/stokesdarcy/couplingmanager.hh>

#if EXNUMBER >= 3
#include <dumux/io/grid/gridmanager_sub.hh>
#endif

#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/components/simpleh2o.hh>

// Free-flow domain
#include <dumux/discretization/staggered/freeflow/properties.hh>
#include <dumux/freeflow/navierstokes/model.hh>

#include "freeflowsubproblem.hh"

// Porous medium flow domain
#include <dumux/discretization/cctpfa.hh>
#include <dumux/porousmediumflow/1p/model.hh>

#include "../1pspatialparams.hh"
#include "porousmediumsubproblem.hh"

namespace Dumux::Properties {

// Create new type tags
namespace TTag {
struct FreeflowOneP { using InheritsFrom = std::tuple<NavierStokes, StaggeredFreeFlowModel>; };
struct PorousMediumFlowOneP { using InheritsFrom = std::tuple<OneP, CCTpfaModel>; };
} // end namespace TTag

// Set the coupling manager
template<class TypeTag>
struct CouplingManager<TypeTag, TTag::FreeflowOneP>
{
    using Traits = StaggeredMultiDomainTraits<TypeTag, TypeTag, Properties::TTag::PorousMediumFlowOneP>;
    using type = Dumux::StokesDarcyCouplingManager<Traits>;
};
template<class TypeTag>
struct CouplingManager<TypeTag, TTag::PorousMediumFlowOneP>
{
    using Traits = StaggeredMultiDomainTraits<Properties::TTag::FreeflowOneP, Properties::TTag::FreeflowOneP, TypeTag>;
    using type = Dumux::StokesDarcyCouplingManager<Traits>;
};

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::PorousMediumFlowOneP> { using type = Dumux::PorousMediumSubProblem<TypeTag>; };
template<class TypeTag>
struct Problem<TypeTag, TTag::FreeflowOneP> { using type = Dumux::FreeFlowSubProblem<TypeTag> ; };

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::PorousMediumFlowOneP>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = FluidSystems::OnePLiquid<Scalar, Dumux::Components::SimpleH2O<Scalar> > ;
};
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::FreeflowOneP>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = FluidSystems::OnePLiquid<Scalar, Dumux::Components::SimpleH2O<Scalar> > ;
};

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::PorousMediumFlowOneP>
{
    static constexpr auto dim = 2;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using TensorGrid = Dune::YaspGrid<2, Dune::TensorProductCoordinates<Scalar, dim> >;

#if EXNUMBER < 3 // use "normal" grid
    using type = TensorGrid;
#else // use dune-subgrid
    using HostGrid = TensorGrid;
    using type = Dune::SubGrid<dim, HostGrid>;
#endif
};
template<class TypeTag>
struct Grid<TypeTag, TTag::FreeflowOneP>
{
    static constexpr auto dim = 2;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using TensorGrid = Dune::YaspGrid<2, Dune::TensorProductCoordinates<Scalar, dim> >;

#if EXNUMBER < 3 // use "normal" grid
    using type = TensorGrid;
#else // use dune-subgrid
    using HostGrid = TensorGrid;
    using type = Dune::SubGrid<dim, HostGrid>;
#endif
};

template<class TypeTag>
struct SpatialParams<TypeTag, TTag::PorousMediumFlowOneP> {
    using type = OnePSpatialParams<GetPropType<TypeTag, GridGeometry>, GetPropType<TypeTag, Scalar>>;
};

template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::FreeflowOneP> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::FreeflowOneP> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::FreeflowOneP> { static constexpr bool value = true; };

template<class TypeTag>
struct NormalizePressure<TypeTag, TTag::FreeflowOneP> { static constexpr bool value = false; };

} //end namespace Dumux::Properties

#endif
