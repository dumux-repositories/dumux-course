// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Course contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 *
 * \brief The two-phase porousmediumflow properties file for exercise runtime parameters
 */

#ifndef DUMUX_EXRUNTIMEPARAMS_PROPERTIES_HH
#define DUMUX_EXRUNTIMEPARAMS_PROPERTIES_HH

#include <dune/grid/yaspgrid.hh>

#include <dumux/discretization/cctpfa.hh>
#include <dumux/porousmediumflow/2p/model.hh>
#include <dumux/material/fluidsystems/h2on2.hh>

#include "spatialparams.hh"
#include "problem.hh"

namespace Dumux::Properties {

// Define the TypeTag for this problem
// with a cell-centered two-point flux approximation spatial discretization.
namespace TTag {
struct Injection2p { using InheritsFrom = std::tuple<TwoP>; };
struct Injection2pCC { using InheritsFrom = std::tuple<Injection2p, CCTpfaModel>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::Injection2p> { using type = Dune::YaspGrid<2>; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::Injection2p> { using type = InjectionProblem2P<TypeTag>; };

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::Injection2p>
{
private:
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
    using type = InjectionSpatialParams<GridGeometry, Scalar>;
};

// Set fluid configuration
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::Injection2p>
{
    using type = FluidSystems::H2ON2< GetPropType<TypeTag, Properties::Scalar>,
                                      FluidSystems::H2ON2DefaultPolicy</*fastButSimplifiedRelations=*/ true> >;
};

} // end namespace Dumux::Properties

#endif
