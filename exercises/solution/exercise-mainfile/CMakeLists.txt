# SPDX-FileCopyrightText: Copyright © DuMux-Course contributors, see AUTHORS.md in root folder
# SPDX-License-Identifier: GPL-3.0-or-later

# the one-phase simulation program
dumux_add_test(NAME exercise_mainfile_a_solution
               SOURCES exercise1pa_solution_main.cc
               COMPILE_DEFINITIONS TYPETAG=OnePIncompressible)

# add a symlink for each input file
add_input_file_links()
