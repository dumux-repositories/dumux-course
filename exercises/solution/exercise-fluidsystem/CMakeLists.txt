# SPDX-FileCopyrightText: Copyright © DuMux-Course contributors, see AUTHORS.md in root folder
# SPDX-License-Identifier: GPL-3.0-or-later

# executables for exercise part a & b
#part a: 2pproblem
dumux_add_test(NAME exercise_fluidsystem_a_solution
               SOURCES main.cc
               CMD_ARGS aparams.input -Output.PlotDensity false
               COMPILE_DEFINITIONS TYPETAG=ExerciseFluidsystemTwoP)

#part b: 2p2cproblem
dumux_add_test(NAME exercise_fluidsystem_b_solution
               SOURCES main.cc
               CMD_ARGS bparams.input
               COMPILE_DEFINITIONS TYPETAG=ExerciseFluidsystemTwoPTwoC)

# add a symlink for each input file
add_input_file_links()
