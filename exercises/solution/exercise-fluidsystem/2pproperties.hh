// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Course contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 *
 * \brief The properties file for two-phase exercise-fluidsystem.
 */
#ifndef DUMUX_EXERCISE_FLUIDSYSTEM_A_PROPERTIES_HH
#define DUMUX_EXERCISE_FLUIDSYSTEM_A_PROPERTIES_HH

// The grid manager
#include <dune/grid/yaspgrid.hh>

// The numerical model
#include <dumux/porousmediumflow/2p/model.hh>

// The box discretization
#include <dumux/discretization/box.hh>

// Spatially dependent parameters
#include "spatialparams.hh"

// The components that will be created in this exercise
#include "components/myincompressiblecomponent.hh"
#include "components/mycompressiblecomponent.hh"

// We will only have liquid phases here
#include <dumux/material/fluidsystems/1pliquid.hh>

// The two-phase immiscible fluid system
#include <dumux/material/fluidsystems/2pimmiscible.hh>

// The interface to create plots during simulation
#include <dumux/io/gnuplotinterface.hh>

// The problem file, where setup-specific boundary and initial conditions are defined
#include"2pproblem.hh"

namespace Dumux::Properties {

// Create new type tags
namespace TTag {
struct ExerciseFluidsystemTwoP { using InheritsFrom = std::tuple<TwoP, BoxModel>; };
} // end namespace TTag

// Set the "Problem" property
template<class TypeTag>
struct Problem<TypeTag, TTag::ExerciseFluidsystemTwoP> { using type = ExerciseFluidsystemProblemTwoP<TypeTag>; };

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::ExerciseFluidsystemTwoP>
{
private:
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
    using type = ExerciseFluidsystemSpatialParams<GridGeometry, Scalar>;
};

// Set grid to be used
template<class TypeTag>
struct Grid<TypeTag, TTag::ExerciseFluidsystemTwoP> { using type = Dune::YaspGrid<2>; };

// We use the immiscible fluid system here
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::ExerciseFluidsystemTwoP>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using TabulatedH2O = Components::TabulatedComponent<Components::H2O<Scalar>>;
    using LiquidWaterPhase = typename FluidSystems::OnePLiquid<Scalar, TabulatedH2O>;
    /*!
     * TODO: dumux-course-task 2.2:
     * Uncomment first line and comment second line for using the incompressible component
     * Uncomment second line and comment first line for using the compressible component
     */
     using LiquidMyComponentPhase = typename FluidSystems::OnePLiquid<Scalar, MyIncompressibleComponent<Scalar> >;
     //using LiquidMyComponentPhase = typename FluidSystems::OnePLiquid<Scalar, MyCompressibleComponent<Scalar> >;

public:
    using type = typename FluidSystems::TwoPImmiscible<Scalar, LiquidWaterPhase, LiquidMyComponentPhase>;
};

} // end namespace Dumux::Properties

#endif
