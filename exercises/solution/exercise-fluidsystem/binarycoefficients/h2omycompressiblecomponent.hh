// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Course contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 *
 * \brief Binary coefficients for water and a fictitious component implemented in exercise-fluidsystem 2.1.
 */
#ifndef DUMUX_BINARY_COEFF_H2O_MYCOMPRESSIBLECOMPONENT_HH
#define DUMUX_BINARY_COEFF_H2O_MYCOMPRESSIBLECOMPONENT_HH

namespace Dumux
{
namespace BinaryCoeff
{

/*!
 * \brief Binary coefficients for water and a fictitious component implemented in exercise-fluidsystem 2.1.
 */
class H2O_MyCompressibleComponent
{
public:
    /*!
     * \brief Henry coefficient \f$[N/m^2]\f$ for the fictitous component in liquid water.
     */
    template <class Scalar>
    static Scalar henryMyCompressibleComponentInWater(Scalar temperature)
    {
        Scalar dumuxH = 1.5e-1 / 101.325; // [(mol/m^3)/Pa]
        dumuxH *= 18.02e-6;  //multiplied by molar volume of reference phase = water
        return 1.0/dumuxH; // [Pa]
    }

    /*!
     * \brief Henry coefficient \f$[N/m^2]\f$ for water in the fictitious component.
     */
    template <class Scalar>
    static Scalar henryWaterInMyCompressibleComponent(Scalar temperature)
    {
        // arbitrary
        return 1.0e8; // [Pa]
    }

    /*!
     * \brief Diffusion coefficient [m^2/s] for my fictitious component in liquid water or vice versa.
     */
    template <class Scalar>
    static Scalar liquidDiffCoeff(Scalar temperature, Scalar pressure)
    {
        // arbitrary
        return 1.e-9; // [m^2/s]
    }
};

}
} // end namespace

#endif
