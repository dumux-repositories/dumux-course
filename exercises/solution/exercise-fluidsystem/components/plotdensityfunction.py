#!usr/bin/env python
# SPDX-FileCopyrightText: Copyright © DuMux-Course contributors, see AUTHORS.md in root folder
# SPDX-License-Identifier: GPL-3.0-or-later

import numpy as np
import matplotlib.pyplot as plt

# Function to calculate rho dependent on pressure.
rho_min = 1440.0;
rho_max = 1480.0;
k = 5.0e-7;

def rho(p):
    return rho_min + (rho_max - rho_min)/(1 + rho_min*np.exp(-1.0*k*(rho_max - rho_min)*p));

# Sample pressure in range (1e4, 1e7) and compute corresponding densities.
p = np.logspace(4, 7, 100)
r = rho(p)

# Plot density vs. pressure.
plt.semilogx(p, r)
plt.show()
