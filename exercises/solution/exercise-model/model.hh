// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Course contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//

#ifndef DUMUX_EXERCISE_NONLINEAR_DIFFUSION_MODEL_HH
#define DUMUX_EXERCISE_NONLINEAR_DIFFUSION_MODEL_HH

// In the file `model.hh`, we define the volume variables, the model equations, and
// set all default model properties.

#include <dune/common/fvector.hh>
#include <dumux/common/math.hh>
#include <dumux/common/properties.hh>
#include <dumux/common/numeqvector.hh>
#include <dumux/common/volumevariables.hh>
#include <dumux/discretization/method.hh>
#include <dumux/discretization/defaultlocaloperator.hh>

// The property tag is simply an empty struct with the name `NonlinearDiffusionModel`
namespace Dumux::Properties::TTag {
//! The nonlinear diffusion model tag that we can specialize properties for
struct NonlinearDiffusionModel {};
} // end namespace Dumux::Properties::TTag

namespace Dumux {

/*!
 * \ingroup NonlinearDiffusionModel
 * \brief Volume averaged quantities required by the nonlinear diffusion model.
 * This contains the quantities which are related to a control volume
 */
template <class Traits>
class ImageDenoisingVolumeVariables
{
    using Scalar = typename Traits::PrimaryVariables::value_type;
public:
    //! export the type used for the primary variables
    using PrimaryVariables = typename Traits::PrimaryVariables;
    using Indices = typename Traits::ModelTraits::Indices;

    template<class ElementSolution, class Problem, class Element, class SubControlVolume>
    void update(const ElementSolution& elemSol,
                const Problem& problem,
                const Element& element,
                const SubControlVolume& scv)
    {
        priVars_ = elemSol[scv.indexInElement()];
    }

    /*!
     * \brief Returns the image intensity of the control volume.
     */
    Scalar imageIntensity() const
    { return priVars_[Indices::imageIntensityIdx]; }

    Scalar priVar(const int pvIdx) const
    { return priVars_[pvIdx]; }

    const PrimaryVariables& priVars() const
    { return priVars_; }

    Scalar extrusionFactor() const
    { return 1.0; }

private:
    PrimaryVariables priVars_;
};

/*!
 * \ingroup NonlinearDiffusionModel
 * \brief Element-wise calculation of the residual and its derivatives
 *        for a nonlinear diffusion problem.
 */
template<class TypeTag>
class NonlinearDiffusionModelLocalResidual
: public DiscretizationDefaultLocalOperator<TypeTag>
{
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using ParentType = DiscretizationDefaultLocalOperator<TypeTag>;

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using NumEqVector = Dumux::NumEqVector<GetPropType<TypeTag, Properties::PrimaryVariables>>;

    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using VolumeVariables = typename GridVariables::GridVolumeVariables::VolumeVariables;
    using ElementVolumeVariables = typename GridVariables::GridVolumeVariables::LocalView;
    using ElementFluxVariablesCache = typename GridVariables::GridFluxVariablesCache::LocalView;

    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;

    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
    using Indices = typename ModelTraits::Indices;
    static constexpr int dimWorld = GridView::dimensionworld;

public:
    using ParentType::ParentType;

    NumEqVector computeStorage(const Problem& problem,
                               const SubControlVolume& scv,
                               const VolumeVariables& volVars) const
    {
        NumEqVector storage;
        storage[Indices::diffusionEqIdx] = volVars.imageIntensity();
        return storage;
    }

    NumEqVector computeFlux(const Problem& problem,
                            const Element& element,
                            const FVElementGeometry& fvGeometry,
                            const ElementVolumeVariables& elemVolVars,
                            const SubControlVolumeFace& scvf,
                            const ElementFluxVariablesCache& elemFluxVarsCache) const
    {
        static_assert(DiscretizationMethods::isCVFE<typename GridGeometry::DiscretizationMethod>,
            "This local residual is hard-coded to control-volume finite element schemes");

        // Compute ∇c at the integration point of this sub control volume face.
        const auto& fluxVarCache = elemFluxVarsCache[scvf];
        Dune::FieldVector<Scalar, dimWorld> gradConcentration(0.0);
        for (const auto& scv : scvs(fvGeometry))
        {
            const auto& volVars = elemVolVars[scv];
            // v.axpy(a, w) means v <- v + a*w
            gradConcentration.axpy(
                volVars.imageIntensity(),
                fluxVarCache.gradN(scv.indexInElement())
            );
        }

        NumEqVector flux;

        const auto K = problem.conductance();
        const auto D = 1.0/(1.0 + gradConcentration.two_norm2()/(K*K));

        // Compute the flux with `vtmv` (vector transposed times matrix times vector) or -n^T D ∇c A.
        // The diffusion coefficient comes from the `problem` (see Part 2 of the example).
        flux[Indices::diffusionEqIdx] = -1.0*vtmv(
            scvf.unitOuterNormal(), D, gradConcentration
        )*scvf.area();

        return flux;
    }
};
} // end namespace Dumux

// The model properties
namespace Dumux::Properties {

// The type of the local residual is the class defined above
template<class TypeTag>
struct LocalResidual<TypeTag, TTag::NonlinearDiffusionModel>
{ using type = NonlinearDiffusionModelLocalResidual<TypeTag>; };

// The default scalar type is double
// we compute with double precision floating point numbers
template<class TypeTag>
struct Scalar<TypeTag, TTag::NonlinearDiffusionModel>
{ using type = double; };

// The model traits specify some information about our equation system.
template<class TypeTag>
struct ModelTraits<TypeTag, TTag::NonlinearDiffusionModel>
{
    struct type
    {
        struct Indices
        {
            static constexpr int imageIntensityIdx = 0;
            static constexpr int diffusionEqIdx = 0;
        };

        static constexpr int numEq() { return 1; }
    };
};

// The primary variable vector has entries of type `Scalar` and is
// as large as the number of equations
template<class TypeTag>
struct PrimaryVariables<TypeTag, TTag::NonlinearDiffusionModel>
{
    using type = Dune::FieldVector<
        GetPropType<TypeTag, Properties::Scalar>,
        GetPropType<TypeTag, Properties::ModelTraits>::numEq()
    >;
};

// The volume variables that are used in our model
template<class TypeTag>
struct VolumeVariables<TypeTag, TTag::NonlinearDiffusionModel>
{
    struct Traits
    {
        using ModelTraits
            = GetPropType<TypeTag, Properties::ModelTraits>;
        using PrimaryVariables
            = GetPropType<TypeTag, Properties::PrimaryVariables>;
    };

    using type = ImageDenoisingVolumeVariables<Traits>;
};

} // end namespace Dumux::Properties

#endif
