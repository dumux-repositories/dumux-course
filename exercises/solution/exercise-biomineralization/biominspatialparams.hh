// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Course contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \brief Spatial parameters for the bio mineralization problem where urea is used to
 * precipitate calcite.
 */
#ifndef DUMUX_BIOMIN_SPATIAL_PARAMETERS_HH
#define DUMUX_BIOMIN_SPATIAL_PARAMETERS_HH

#include <dumux/porousmediumflow/fvspatialparamsmp.hh>
#include <dumux/material/fluidmatrixinteractions/2p/linearmaterial.hh>
#include <dumux/material/fluidmatrixinteractions/2p/brookscorey.hh>
#include <dumux/material/fluidmatrixinteractions/porosityprecipitation.hh>
// TODO: dumux-course-task 6:
// include the new permeability law (power law) instead of Kozeny-Carman
#include "fluidmatrixinteractions/permeabilitypowerlaw.hh" //the power-law porosity-permeability relation

#include <dumux/discretization/method.hh>

namespace Dumux {

/*!
 * \brief Definition of the spatial parameters for the biomineralisation problem
 * with geostatistically distributed initial permeability.
 */
template<class GridGeometry, class Scalar, int numFluidComps, int numActiveSolidComps>
class BioMinSpatialparams
: public FVPorousMediumFlowSpatialParamsMP<GridGeometry, Scalar, BioMinSpatialparams<GridGeometry, Scalar, numFluidComps, numActiveSolidComps>>
{
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using ParentType = FVPorousMediumFlowSpatialParamsMP<GridGeometry, Scalar, BioMinSpatialparams<GridGeometry, Scalar, numFluidComps, numActiveSolidComps>>;

    using GridView = typename GridGeometry::GridView;
    using CoordScalar = typename GridView::ctype;
    static constexpr int dimWorld = GridView::dimensionworld;
    using Element = typename GridView::template Codim<0>::Entity;

    using GlobalPosition = Dune::FieldVector<CoordScalar, dimWorld>;
    using Tensor = Dune::FieldMatrix<CoordScalar, dimWorld, dimWorld>;

    using PoroLaw = PorosityPrecipitation<Scalar, numFluidComps, numActiveSolidComps>;

    using PcKrSwCurve = FluidMatrix::BrooksCoreyDefault<Scalar>;

public:
    using PermeabilityType = Tensor;

    /*!
    * \brief The constructor
    *
    * \param gridView The grid view
    */
    BioMinSpatialparams(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    , pcKrSwCurve_("SpatialParams")
    , aquitardPcKrSwCurve_("SpatialParams.Aquitard")
    {
        //! set initial aquifer params
        initialPorosity_ = getParam<Scalar>("SpatialParams.InitialPorosity");
        initialPermeability_ = getParam<Scalar>("SpatialParams.InitialPermeability");

        // set main diagonal entries of the permeability tensor to a value
        // setting to one value means: isotropic, homogeneous

        //! hard code specific params for aquitard layer
        aquitardPorosity_ = 0.1;
        aquitardPermeability_ = 1e-15;
    }

    template<class SolidSystem, class ElementSolution>
    Scalar inertVolumeFraction(const Element& element,
                               const SubControlVolume& scv,
                               const ElementSolution& elemSol,
                               int compIdx) const
    {
        return 1-referencePorosity(element, scv);
    }

    /*!
     *  \brief Define the initial porosity \f$[-]\f$ distribution
     *  For this special case, the initialPorosity is precalculated value
     *  as a referencePorosity due to an initial volume fraction of biofilm.
     *
     *  \param element The finite element
     *  \param scv The sub-control volume
     */
    Scalar referencePorosity(const Element& element,
                             const SubControlVolume &scv) const
    {
        const auto eIdx = this->gridGeometry().elementMapper().index(element);
        return referencePorosity_[eIdx][scv.indexInElement()];
    }

    /*!
     * \brief Compute the reference porosity which is needed to set the correct initialPorosity.
     *  This value calculates the correct porosity that needs to be set
     *  in for the initialPorosity when an initial volume fraction of
     *  biofilm or other precipitate is in the system.
     *  This function makes use of evaluatePorosity from the porosityprecipitation law.
     *  The reference porosity is calculated as:
     *  \f[referencePorosity = initialPorosity + \sum \phi \f]
     *  which in making use of already available functions is
     *
     *  \f[referencePorosity = 2 \cdot initialPorosity - evaluatePorosity() \f]
     *
     * \param gridGeometry The gridGeometry
     * \param sol The (initial) solution vector
     */
    template<class SolutionVector>
    void computeReferencePorosity(const GridGeometry& gridGeometry,
                                  const SolutionVector& sol)
    {
        referencePorosity_.resize(gridGeometry.gridView().size(0));
        for (const auto& element : elements(gridGeometry.gridView()))
        {
            auto fvGeometry = localView(gridGeometry);
            fvGeometry.bindElement(element);

            const auto eIdx = this->gridGeometry().elementMapper().index(element);

            auto elemSol = elementSolution(element, sol, gridGeometry);
            referencePorosity_[eIdx].resize(fvGeometry.numScv());
            for (const auto& scv : scvs(fvGeometry))
            {
                const auto& dofPosition = scv.dofPosition();
                const bool isInAquitardNotFaultZone = isInAquitard_(dofPosition) && !isFaultZone_(dofPosition);
                auto phi = isInAquitardNotFaultZone ? aquitardPorosity_ : initialPorosity_;

                auto phiEvaluated = poroLaw_.evaluatePorosity(element, scv, elemSol, phi);
                referencePorosity_[eIdx][scv.indexInElement()] = calculatephiRef(phi, phiEvaluated);
            }
        }
    }

    /*!
     *  \brief Return the actual recent porosity \f$[-]\f$ accounting for
     *  clogging caused by mineralization.
     *
     *  \param element The finite element
     *  \param scv The sub-control volume
     */
    template<class ElementSolution>
    Scalar porosity(const Element& element,
                    const SubControlVolume& scv,
                    const ElementSolution& elemSol) const
    {
        const auto refPoro = referencePorosity(element, scv);
        return poroLaw_.evaluatePorosity(element, scv, elemSol, refPoro);
    }

    /*!
     *  \brief Define the reference permeability \f$[m^2]\f$ distribution
     *
     *  \param element The finite element
     *  \param scv The sub-control volume
     */
    PermeabilityType referencePermeability(const Element& element,
                                           const SubControlVolume &scv) const
    {
        const auto eIdx = this->gridGeometry().elementMapper().index(element);
        return referencePermeability_[eIdx][scv.indexInElement()];
    }

    /*!
     * \brief Compute the reference permeability which depends on the porosity.
     *  This value calculates the correct permeability that needs to be set
     *  for the initialPermeability when an initial volume fraction of
     *  biofilm or other precipitate is in the system.
     *  This function makes use of evaluatePermeability function
     *  from the Kozeny-Carman permeability law.
     *  The reference permeability is calculated as:
     *  \f[referencePorosity = initialPermeability
     *  \cdot f(refPoro / initPoro) - evaluatePermeability \cdot f(refPoro/initPoro) \f]
     *  which in making use of already available functions is
     *
     *  \f[referencePermeability = initialPermeability^2 / evaluatePermeability() \f]
     *
     * \param gridGeometry The gridGeometry
     * \param sol The (initial) solution vector
     */
    template<class SolutionVector>
    void computeReferencePermeability(const GridGeometry& gridGeometry,
                                      const SolutionVector& sol)
    {
        referencePermeability_.resize(gridGeometry.gridView().size(0));
        for (const auto& element : elements(gridGeometry.gridView()))
        {
            auto fvGeometry = localView(gridGeometry);
            fvGeometry.bindElement(element);

            const auto eIdx = this->gridGeometry().elementMapper().index(element);

            auto elemSol = elementSolution(element, sol, gridGeometry);
            referencePermeability_[eIdx].resize(fvGeometry.numScv());
            for (const auto& scv : scvs(fvGeometry))
            {
                const auto& dofPosition = scv.dofPosition();
                const bool isInAquitardNotFaultZone = isInAquitard_(dofPosition) && !isFaultZone_(dofPosition);
                auto kInit = isInAquitardNotFaultZone ? aquitardPermeability_ : initialPermeability_;
                PermeabilityType K (0.0);
                for (int i = 0; i < dimWorld; ++i)
                    K[i][i] = kInit;
                const auto refPoro = referencePorosity(element, scv);
                const auto poro = porosity(element, scv, elemSol);
                auto kEvaluated = permLaw_.evaluatePermeability(K, refPoro, poro);
                K *= K[0][0]/kEvaluated[0][0];
                referencePermeability_[eIdx][scv.indexInElement()] = K;
            }
        }
    }

    /*! Intrinsic permeability tensor K \f$[m^2]\f$ depending
     *  on the position in the domain
     *
     *  \param element The finite volume element
     *  \param scv The sub-control volume
     *
     *  Solution dependent permeability function.
     */
    template<class ElementSolution>
    PermeabilityType permeability(const Element& element,
                                  const SubControlVolume& scv,
                                  const ElementSolution& elemSol) const
    {
        const auto refPerm = referencePermeability(element, scv);
        const auto refPoro = referencePorosity(element, scv);
        const auto poro = porosity(element, scv, elemSol);
        return permLaw_.evaluatePermeability(refPerm, refPoro, poro);
    }

    /*!
     * \brief Returns the fluid-matrix interaction law at a given location
     * \param globalPos The global coordinates for the given location
     */
    auto fluidMatrixInteractionAtPos(const GlobalPosition& globalPos) const
    {
        if (isInAquitard_(globalPos))
            return makeFluidMatrixInteraction(aquitardPcKrSwCurve_);
        return makeFluidMatrixInteraction(pcKrSwCurve_);
    }

    // define which phase is to be considered as the wetting phase
    template<class FluidSystem>
    int wettingPhaseAtPos(const GlobalPosition& globalPos) const
    { return FluidSystem::liquidPhaseIdx; }

    /*!
     * \brief Returns the temperature at the domain at the given position
     * \param globalPos The position in global coordinates where the temperature should be specified
     */
    Scalar temperatureAtPos(const GlobalPosition& globalPos) const
    {
        return 300;
    }

private:
    static constexpr Scalar eps_ = 1e-6;

    Scalar calculatephiRef(Scalar phiInit, Scalar phiEvaluated)
    { return 2*phiInit - phiEvaluated;}

    bool isInAquitard_(const GlobalPosition &globalPos) const
    { return globalPos[dimWorld-1] > 8 - eps_ && globalPos[dimWorld-1] < 10 + eps_;}

    bool isFaultZone_(const GlobalPosition &globalPos) const
    { return globalPos[dimWorld-2] > 2 - eps_ && globalPos[dimWorld-2] < 3 + eps_;}

    // TODO: dumux-course-task 6:
    // define the power law as the permeability law
    PermeabilityPowerLaw<PermeabilityType> permLaw_;
    PoroLaw poroLaw_;

    Scalar initialPorosity_;
    std::vector< std::vector<Scalar> > referencePorosity_;
    Scalar initialPermeability_;
    std::vector< std::vector<PermeabilityType> > referencePermeability_;

    Scalar aquitardPorosity_;
    Scalar aquitardPermeability_;

    const PcKrSwCurve pcKrSwCurve_;
    const PcKrSwCurve aquitardPcKrSwCurve_;
};

} // end namespace Dumux

#endif
