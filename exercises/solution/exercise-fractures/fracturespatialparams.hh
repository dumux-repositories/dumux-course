// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Course contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \ingroup MultiDomain
 * \ingroup MultiDomainFacet
 * \ingroup TwoPTests
 * \brief The spatial parameters for the fracture sub-domain in the exercise
 *        on two-phase flow in fractured porous media.
 */
#ifndef DUMUX_COURSE_FRACTURESEXERCISE_FRACTURE_SPATIALPARAMS_HH
#define DUMUX_COURSE_FRACTURESEXERCISE_FRACTURE_SPATIALPARAMS_HH

#include <dumux/io/grid/griddata.hh>

#include <dumux/porousmediumflow/fvspatialparamsmp.hh>
#include <dumux/material/fluidmatrixinteractions/2p/vangenuchten.hh>

namespace Dumux {

/*!
 * \ingroup MultiDomain
 * \ingroup MultiDomainFacet
 * \ingroup TwoPTests
 * \brief The spatial params the two-phase facet coupling test
 */
template< class GridGeometry, class Scalar >
class FractureSpatialParams
: public FVPorousMediumFlowSpatialParamsMP< GridGeometry, Scalar, FractureSpatialParams<GridGeometry, Scalar> >
{
    using ThisType = FractureSpatialParams< GridGeometry, Scalar >;
    using ParentType = FVPorousMediumFlowSpatialParamsMP< GridGeometry, Scalar, ThisType >;

    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using GridView = typename GridGeometry::GridView;
    using Grid = typename GridView::Grid;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    // use a van-genuchten fluid matrix interaction relationship
    using PcKrSwCurve = FluidMatrix::VanGenuchtenDefault<Scalar>;

    // we identify those fractures as barriers, that have a domain marker
    // of 2. This is what is set in the grid file (see grids/complex.geo)
    static constexpr int barriersDomainMarker = 2;

public:
    //! export the type used for permeabilities
    using PermeabilityType = Scalar;

    //! the constructor
    FractureSpatialParams(std::shared_ptr<const GridGeometry> gridGeometry,
                          std::shared_ptr<const Dumux::GridData<Grid>> gridData,
                          const std::string& paramGroup)
    : ParentType(gridGeometry)
    , gridDataPtr_(gridData)
    , isExercisePartA_(getParamFromGroup<bool>(paramGroup, "Problem.IsExercisePartA"))
    , isExercisePartB_(getParamFromGroup<bool>(paramGroup, "Problem.IsExercisePartB"))
    , isExercisePartC_(getParamFromGroup<bool>(paramGroup, "Problem.IsExercisePartC"))
    , pcKrSwCurve_("Fracture.SpatialParams")
    , PcKrSwCurveBarrier_("Fracture.SpatialParams.Barrier")
    {
        porosity_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Porosity");
        porosityBarrier_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.PorosityBarrier");
        permeability_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Permeability");
        permeabilityBarrier_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.PermeabilityBarrier");
        aperture_ = getParamFromGroup<Scalar>(paramGroup, "SpatialParams.Aperture");
    }

    //! Function for defining the (intrinsic) permeability \f$[m^2]\f$.
    template< class ElementSolution >
    PermeabilityType permeability(const Element& element,
                                  const SubControlVolume& scv,
                                  const ElementSolution& elemSol) const
    {
        // in the unmodified state and exercise part a return the non-barrier parameters
        if (!isExercisePartB_ && !isExercisePartC_)
            return permeability_;

        // in exercise part b always return the barrier parameters
        else if (isExercisePartB_)
            return permeabilityBarrier_;

        // in exercise part 3 return parameters depending on domain marker
        else
        {
            if (getElementDomainMarker(element) == barriersDomainMarker)
                return permeabilityBarrier_;
            else
                return permeability_;
        }
    }

    //! Return the porosity
    template< class ElementSolution >
    Scalar porosity(const Element& element,
                    const SubControlVolume& scv,
                    const ElementSolution& elemSol) const
    {
        // in the unmodified state and exercise part a return the non-barrier parameters
        if (!isExercisePartB_ && !isExercisePartC_)
            return porosity_;

        // in exercise part b always return the barrier parameters
        else if (isExercisePartB_)
            return porosityBarrier_;

        // in exercise part 3 return parameters depending on domain marker
        else
        {
            if (getElementDomainMarker(element) == barriersDomainMarker)
                return porosityBarrier_;
            else
                return porosity_;
        }
    }

    /*!
     * \brief Returns the fluid-matrix interaction law for the sub-control volume
     *
     * \param element The current finite element
     * \param scv The sub-control volume
     * \param elemSol The current element solution
     */
    template<class ElementSolution>
    auto fluidMatrixInteraction(const Element& element,
                                const SubControlVolume& scv,
                                const ElementSolution& elemSol) const
    {
        // in the unmodified state and exercise part a return the non-barrier parameters
        if (!isExercisePartB_ && !isExercisePartC_)
            return makeFluidMatrixInteraction(pcKrSwCurve_);

        // in exercise part b always return the barrier parameters
        else if (isExercisePartB_)
            return makeFluidMatrixInteraction(PcKrSwCurveBarrier_);

        // in exercise part 3 return parameters depending on domain marker
        else
        {
            if (getElementDomainMarker(element) == barriersDomainMarker)
                return makeFluidMatrixInteraction(PcKrSwCurveBarrier_);
            else
                return makeFluidMatrixInteraction(pcKrSwCurve_);
        }
    }

    //! Water is the wetting phase
    template< class FluidSystem >
    int wettingPhaseAtPos(const GlobalPosition& globalPos) const
    {
        // we set water as the wetting phase here
        // which is phase0Idx in the H2oN2 fluid system
        return FluidSystem::phase0Idx;
    }

    /*!
     * \brief Returns the temperature at the domain at the given position
     * \param globalPos The position in global coordinates where the temperature should be specified
     */
    Scalar temperatureAtPos(const GlobalPosition& globalPos) const
    {
        return 283.15;
    }

    //! Set the aperture as extrusion factor.
    Scalar extrusionFactorAtPos(const GlobalPosition& globalPos) const
    {
        // We treat the fractures as lower-dimensional in the grid,
        // but we have to give it the aperture as extrusion factor
        // such that the dimensions are correct in the end.
        return aperture_;
    }

    //! returns the domain marker for an element
    int getElementDomainMarker(const Element& element) const
    { return gridDataPtr_->getElementDomainMarker(element); }

private:
    //! pointer to the grid data (contains domain markers)
    std::shared_ptr<const Dumux::GridData<Grid>> gridDataPtr_;

    bool isExercisePartA_;
    bool isExercisePartB_;
    bool isExercisePartC_;

    const PcKrSwCurve pcKrSwCurve_;
    const PcKrSwCurve PcKrSwCurveBarrier_;

    Scalar porosity_;
    Scalar porosityBarrier_;
    Scalar aperture_;
    PermeabilityType permeability_;
    PermeabilityType permeabilityBarrier_;
};

} // end namespace Dumux

#endif
