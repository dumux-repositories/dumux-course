// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Course contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 *
 * \brief The properties file for two phase-two component exercise-fluidsystem. 
 */
#ifndef DUMUX_EXERCISE_FLUIDSYSTEM_B_PROPERTIES_HH
#define DUMUX_EXERCISE_FLUIDSYSTEM_B_PROPERTIES_HH

// The grid manager
#include <dune/grid/yaspgrid.hh>

// The numerical model
#include <dumux/porousmediumflow/2p2c/model.hh>

// The box discretization
#include <dumux/discretization/box.hh>

// Spatially dependent parameters
#include "spatialparams.hh"

// The fluid system that is created in this exercise
#include "fluidsystems/h2omycompressiblecomponent.hh"

// The problem file, where setup-specific boundary and initial conditions are defined
#include"2p2cproblem.hh"

namespace Dumux::Properties {

// Create new type tags
namespace TTag {
struct ExerciseFluidsystemTwoPTwoC { using InheritsFrom = std::tuple<TwoPTwoC, BoxModel>; };
} // end namespace TTag

// Set the "Problem" property
template<class TypeTag>
struct Problem<TypeTag, TTag::ExerciseFluidsystemTwoPTwoC> { using type = ExerciseFluidsystemProblemTwoPTwoC<TypeTag>; };

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::ExerciseFluidsystemTwoPTwoC>
{
private:
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
    using type = ExerciseFluidsystemSpatialParams<GridGeometry, Scalar>;
};

// Set grid and the grid creator to be used
template<class TypeTag>
struct Grid<TypeTag, TTag::ExerciseFluidsystemTwoPTwoC> { using type = Dune::YaspGrid<2>; };

 // The fluid system property
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::ExerciseFluidsystemTwoPTwoC>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
    using type = FluidSystems::H2OMyCompressibleComponent<Scalar>;
};

} // end namespace Dumux::Properties

#endif
