# Exercise Fluidsystem (DuMuX Course)

The aim of this exercise is to get familiar with the _DuMu<sup>x</sup>_ way of implementing new components and using them in fluid systems (immiscible/mixture). In the scope of this exercise, different two-phase (liquid-liquid) fluid systems are implemented. A new fictitious component is implemented and used to describe a immiscible liquid phase. The second phase will only consist of water (Section 2). Furthermore, the mixture of the two components is implemented as two miscible phases (Section 3).

## Problem set-up

The domain has a size of 60 m x 60 m and contains two low-permeable lenses. Initially, the domain is fully water saturated and the fictitious component is injected through the middle portion of the upper boundary by means of a Neumann boundary condition. The remaining parts of the upper and the entire lower boundary are Neumann no-flow while on the two lateral sides Dirichlet boundary conditions are applied (hydrostatic conditions for the pressure and zero saturation).

<img src="https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-course/raw/master/slides/img/exercise_fluidsystem_setup.png" width="700">


## Task 1: Getting familiar with the code

* Navigate to the directory `dumux-course/exercises/exercise-fluidsystem`.

Locate all the files you will need for this exercise
* The shared __main file__ : `main.cc`
* The __input file__ for part a: `aparams.input`
* The __problem file__ for part a: `2pproblem.hh`
* The __properties file__ for part a: `2pproperties.hh`
* The __input file__ for part b: `bparams.input`
* The __problem file__ for part b: `2p2cproblem.hh`
* The __properties file__ for part b: `2p2cproperties.hh`
* The __spatial parameters file__: `spatialparams.hh`

Furthermore you will find the following folders:
* `binarycoefficients`: Stores headers containing data/methods on binary mixtures.
* `components`: Stores headers containing data/methods on pure components.
* `fluidsystems`: Stores headers containing data/methods on mixtures of pure components. Uses methods from `binarycoefficients`.

To see more components, fluid systems and binarycoefficients implementations, have a look at the folder `dumux/material`.

## Task 2: Implement a new component - Part a

In the following, the basic steps required to set the desired fluid system are outlined. Here, this is done in the __properties file__, i.e. for this part of the exercise the code shown below is taken from the `2pproperties.hh` file.

In this part of the exercise we will consider a system consisting of two immiscible phases. Therefore, the _TypeTag_ for this problem (`ExerciseFluidsystemTwoP`) derives from
the `TwoP` _TypeTag_ (immiscible two-phase model properties) and the `BoxModel` _TypeTag_ (specifies properties of the discretization scheme).

```c++
// Create new type tags
namespace TTag {
struct ExerciseFluidsystemTwoP { using InheritsFrom = std::tuple<TwoP, BoxModel>; };
} // end namespace TTag
```

In order to be able to derive from these _TypeTags_, the declarations of the `TwoP` _TypeTag_ and `BoxModel` _TypeTag_ have to be included.
The `TwoP` _TypeTag_ can be found in the `2p/model.hh` header:

```c++
// The numerical model
#include <dumux/porousmediumflow/2p/model.hh>
```

while the `BoxModel` _TypeTag_ can be found in the `discretization/box.hh` header:

```c++
// The box discretization
#include <dumux/discretization/box.hh>
```

For a cell-centered scheme, you could derive from `CCTpfaModel` or `CCMpfaModel` instead (and, of course, include the right headers).

One of the two phases should only contain the component water. We want to precompute tables on which the properties are then interpolated in order to save computational time. We need the properties of the component water in the properties file and in the problem file. We include the following two headers in our problem file, i.e. `2pproblem.hh` file. By including the problem file in the properties file, it can be accessed through it.

```c++
// The water component
#include <dumux/material/components/tabulatedcomponent.hh>
#include <dumux/material/components/h2o.hh>
```
The other phase that will be created only contains our new component, where we want to implement an incompressible (Section 2.1) and a compressible (Section 2.2) variant.
The respective headers are prepared, but still incomplete. The compressible variant is still commented so that compilation does not fail when finishing the incompressible variant.

```c++
// The components that will be created in this exercise
#include "components/myincompressiblecomponent.hh"
// #include "components/mycompressiblecomponent.hh"
```
As mentioned above, we want to simulate two non-mixing phases (2pimmiscible). The respective fluid system is found in:

```c++
// The two-phase immiscible fluid system
#include <dumux/material/fluidsystems/2pimmiscible.hh>
```

This fluid system expects __phases__ as input and so far we have only included the components, which contain data on the pure component for all physical states. Thus, we need to include

```c++
// We will only have liquid phases here
#include <dumux/material/fluidsystems/1pliquid.hh>
```

which creates a _liquid phase_ from a given component. Finally, using all of the included classes, we set the fluid system property. The fluid system consists of two liquids, water (derived from the tabulated water component) and the new incompressible fictitious component. Both components need to be defined as liquid (`OnePLiquid`). The two liquid components are then form an immiscible fluid system (`TwoPImmiscible`).

```c++
// we use the immiscible fluid system here
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::ExerciseFluidsystemTwoP>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using TabulatedH2O = Components::TabulatedComponent<Components::H2O<Scalar>>;
    using LiquidWaterPhase = typename FluidSystems::OnePLiquid<Scalar, TabulatedH2O>;

    // TODO: dumux-course-task 2.2:
    // Select the corresponding component for the task
    // Comment first line and uncomment second line for using the compressible component
    using LiquidMyComponentPhase = typename FluidSystems::OnePLiquid<Scalar, MyIncompressibleComponent<Scalar> >;
    // using LiquidMyComponentPhase = typename FluidSystems::OnePLiquid<Scalar, MyCompressibleComponent<Scalar> >;

public:
    using type = typename FluidSystems::TwoPImmiscible<Scalar, LiquidWaterPhase, LiquidMyComponentPhase>;
};
```

### Task 2.1: Incompressible component

Open the file `myincompressiblecomponent.hh`.  A component should always derive from the _Base_ class - thus we include `dumux/material/components/base.hh`-, which defines the interface of a _DuMuX_ component with the possibly required functions to be overloaded by the actual implementation. Additionally, it is required that liquids are derived from the _Liquid_ class (see `dumux/material/components/liquid.hh`), gases from the _Gas_ class (see `dumux/material/components/gas.hh`) and solids from the _Solid_ class (see `dumux/material/components/solid.hh`), with functions specific to liquid, gas or solid.

```c++
/*!
 * \ingroup Components
 * \brief A ficitious component to be implemented in exercise-fluidsystem 2.1.
 *
 * \tparam Scalar The type used for scalar values.
 */
template <class Scalar>
class MyIncompressibleComponent
: public Components::Base<Scalar, MyIncompressibleComponent<Scalar> >
, public Components::Liquid<Scalar, MyIncompressibleComponent<Scalar> >
```

__Task__:

Implement an incompressible component into the file `myincompressiblecomponent.hh`, which has the following specifications:

| Parameter | unit | value |
| -----| --------| -------- |
| $`M`$ | $`kg/mol`$   | $`131.39 \cdot 10^{-3}`$ |
| $`\rho_{liquid}`$ | $`kg/m^3`$   | $`1460.0`$   |
| $`\mu_{liquid}`$ | $`Pa \cdot s`$   | $`5.7 \cdot 10^{-4}`$   |

In order to do so, have a look at the files `dumux/material/components/base.hh` and `dumux/material/components/liquid.hh` to see how the interfaces are defined and overload them accordingly.

In order to execute the program, change to the build directory and compile and execute the program by typing

```bash
cd build-cmake/exercises/exercise-fluidsystem
make exercise_fluidsystem_a
./exercise_fluidsystem_a aparams.input
```

The saturation distribution of the nonwetting phase, here S$`_{napl}`$ (the phase consisting of our fictitious incompressible component), at the final simulation time should look like this:

![](../../slides/img/exercise_fluidsystem_a_solution.png)

### Task 2.2: Compressible component

We now want to implement a pressure-dependent density for our component. Open the file `mycompressiblecomponent.hh` and copy in the functions you implemented for the incompressible variant. Now substitute the method that returns the density by the following expression:

$`\displaystyle \rho_{MyComp} = \rho_{min} + \frac{ \rho_{max} - \rho_{min} }{ 1 + \rho_{min}*e^{-1.0*k*(\rho_{max} - \rho_{min})*p} } `$

where $`p`$ is the pressure and $`\rho_{min} = 1440.0 \, kg/m^3`$, $`\rho_{max} = 1480.0 \, kg/m^3`$ and $`k = 5 \cdot 10^{-7} `$. Also, make sure the header is included in the `2pproperties.hh` file by uncommenting the corresponding line. Furthermore, the new component has to be set as a liquid phase in the fluid system. To do so, search for `TODO: dumux-course-task 2.2`. Comment out the corresponding line and uncomment the other. The density distribution of this phase (rho$`_{napl}`$) at the final simulation time should look like this:

![](../../slides/img/exercise_fluidsystem_a_solution2.png)

You can plot the density of the phase consisting of your compressible component by setting `PlotDensity` in `aparams.input` to `true` and starting the simulation again.
Compare the gnuplot output to the following plot of the density function from above:

<img src="https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-course/raw/master/slides/img/exercise_fluidsystem_a_densityfunction.png" width="500">

## Task 3: Implement a new fluid system - Part b

The problem file and properties file for this part of the exercise are `2p2cproblem.hh` and `2p2cproperties.hh`, respectively.
We now want to implement a new fluid system, which still consists of two liquid phases. However, one phase consists mainly of water and the other consists mainly of the previously implemented compressible component. We will now consider compositional effects, which is why we have to derive our _TypeTag_ (`ExerciseFluidsystemTwoPTwoC`) from a _TypeTag_ (`TwoPTwoC`) that holds the miscible two-phase two-component model properties:

```c++
// The numerical model
#include <dumux/porousmediumflow/2p2c/model.hh>
```

```c++
// Create new type tags
namespace TTag {
struct ExerciseFluidsystemTwoPTwoC { using InheritsFrom = std::tuple<TwoPTwoC, BoxModel>; };
} // end namespace TTag
```

The new fluid system is to be implemented in the file `fluidsystems/h2omycompressiblecomponent.hh`. This is already included in the problem and the fluid system property is set accordingly.

```c++
// The fluid system that is created in this exercise
#include "fluidsystems/h2omycompressiblecomponent.hh"
```

```c++
// The fluid system property
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::ExerciseFluidsystemTwoPTwoC>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
    using type = FluidSystems::H2OMyCompressibleComponent<Scalar>;
};
```

In the `fluidsystems/h2omycompressiblecomponent.hh` file, your implemented compressible component and the binary coefficient files are already included.

```c++
// The ficitious component that was created in exercise-fluidsystem 2.2
#include <exercises/exercise-fluidsystem/components/mycompressiblecomponent.hh>

// The binary coefficients corresponding to this fluid system
#include <exercises/exercise-fluidsystem/binarycoefficients/h2omycompressiblecomponent.hh>
```

__Task__:

Under the assumption that one molecule of `MyCompressibleComponent` displaces exactly one molecule of water, the density of the mostly-water-phase can be expressed as follows:

$` \rho_{w} = \frac{ \rho_{w, pure} }{ M_{H_2O} }*(M_{H_2O}*x_{H_2O} + M_{MyComponent}*x_{MyComponent}) `$

Implement this dependency in the `density()` method in the fluid system. In order to compile and execute the program, run the following commands:

```bash
cd build-cmake/exercises/exercise-fluidsystem
make exercise_fluidsystem_b
./exercise_fluidsystem_b bparams.input
```

You will observe an error message and an abortion of the program. This is due to the fact that in order for the constraint solver and other mechanisms in the two-phase two-component model to work, an additional functionality in the component has to be implemented: the model has to know the vapour pressure. As in the previous exercise, check the `dumux/material/components/base.hh` file for this function and implement it into `mycompressiblecomponent.hh`. For the vapour pressure, use a value of $`3900.0 \, Pa`$.

## Task 4: Change wettability of the porous medium - Part b

In the `spatialparams.hh` file, we can find the following function, with which we can specify which phase of the fluid system is to be considered as the wetting phase at a given position within the domain:

```cpp
/*!
 * \brief Function for defining which phase is to be considered as the wetting phase.
 *
 * \return the wetting phase index
 * \param globalPos The position of the center of the element
 */
template<class FluidSystem>
int wettingPhaseAtPos(const GlobalPosition& globalPos) const
{
    // Our fluid system is H2OMyCompressibleComponent.
    // We want to define water as the wetting phase in
    // the entire domain (see fluid system for the phase indices).

    // TODO: dumux-course-task 4:
    // Adapt the following line so that the phase of our new component is
    // the wetting phase, only within the lenses.
    return FluidSystem::phase0Idx;
}
```

Change this function such that the phase of our mostly-new-component-phase is the wetting phase __only__ within the lenses. Execute the program of task 3 again:

```bash
cd build-cmake/exercises/exercise-fluidsystem
make exercise_fluidsystem_b
./exercise_fluidsystem_b bparams.input
```
