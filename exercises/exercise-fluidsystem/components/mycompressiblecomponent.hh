// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Course contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \ingroup Components
 * \brief A fictitious component to be implemented in exercise 2.2.
 */
#ifndef DUMUX_MYCOMPRESSIBLECOMPONENT_HH
#define DUMUX_MYCOMPRESSIBLECOMPONENT_HH

#include <dumux/material/idealgas.hh>

#include <dumux/material/components/base.hh>
#include <dumux/material/components/liquid.hh>

namespace Dumux
{
/*!
 * \ingroup Components
 * \brief A fictitious component to be implemented in exercise 2.2.
 *
 * \tparam Scalar The type used for scalar values
 */
template <class Scalar>
class MyCompressibleComponent
: public Components::Base<Scalar, MyCompressibleComponent<Scalar> >
, public Components::Liquid<Scalar, MyCompressibleComponent<Scalar> >
{

public:
    /*!
     * \brief A human readable name for MyCompressibleComponent.
     */
    static std::string name()
    { return "MyCompressibleComponent"; }

      /*!
      * \brief Returns true if the liquid phase is assumed to be compressible.
      */
     static constexpr bool liquidIsCompressible()
     { return true; }

     /*!
      * \brief The molar mass in \f$\mathrm{[kg/mol]}\f$ of the component.
      */
     static Scalar molarMass()
     {
        // TODO: dumux-course-task 2.2:
        // Copy the methods implemented in MyIncompressibleComponent and substitute
        // the density calculation by the expression given in the exercise description.
        // Replace the line below by a meaningful return statement.
        DUNE_THROW(Dune::NotImplemented, "Todo: implement molar mass");
     }

     /*!
      * \brief The density \f$\mathrm{[kg/m^3]}\f$ of the liquid component at a given pressure in
      *          \f$\mathrm{[Pa]}\f$ and temperature in \f$\mathrm{[K]}\f$.
      *
      * \param temperature temperature of component in \f$\mathrm{[K]}\f$
      * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
      */
     static Scalar liquidDensity(Scalar temperature, Scalar pressure)
     {
         // TODO: dumux-course-task 2.2:
         // Replace the line below by a meaningful return statement.
         DUNE_THROW(Dune::NotImplemented, "Todo: implement liquid density");
     }

     /*!
      * \brief The molar density of MyCompressibleComponent in \f$\mathrm{[mol/m^3]}\f$ at a given pressure and temperature.
      *
      * \param temperature temperature of component in \f$\mathrm{[K]}\f$
      * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
      *
      */
     static Scalar liquidMolarDensity(Scalar temperature, Scalar pressure)
     {
         // compute molar density by dividing the density by the molar mass
         return liquidDensity(temperature, pressure)/molarMass();
     }

     /*!
      * \brief The dynamic liquid viscosity \f$\mathrm{[Pa*s]}\f$ of the pure component.
      *
      * \param temperature temperature of component in \f$\mathrm{[K]}\f$
      * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
      */
     static Scalar liquidViscosity(Scalar temperature, Scalar pressure)
     {
         // TODO: dumux-course-task 2.2:
         // Replace the line below by a meaningful return statement.
         DUNE_THROW(Dune::NotImplemented, "Todo: implement liquid viscosity");
     }

     /*!
      * \brief The vapor pressure in \f$\mathrm{[Pa]}\f$ of the component at a given
      *        temperature in \f$\mathrm{[K]}\f$.
      *
      * \param T temperature of the component in \f$\mathrm{[K]}\f$
      */
     static Scalar vaporPressure(Scalar t)
     {
         // TODO: dumux-course-task 3:
         // Replace the line below by a meaningful return statement.
         DUNE_THROW(Dune::NotImplemented, "Todo: implement vapour pressure");
     }
};

} // end namespace

#endif
