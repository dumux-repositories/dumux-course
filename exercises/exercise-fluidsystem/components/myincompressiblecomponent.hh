// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
//
// SPDX-FileCopyrightText: Copyright © DuMux-Course contributors, see AUTHORS.md in root folder
// SPDX-License-Identifier: GPL-3.0-or-later
//
/*!
 * \file
 * \ingroup Components
 * \brief A fictitious component to be implemented in exercise-fluidsystem 2.1.
 */
#ifndef DUMUX_MYINCOMPRESSIBLECOMPONENT_HH
#define DUMUX_MYINCOMPRESSIBLECOMPONENT_HH

#include <dumux/material/idealgas.hh>

#include <dumux/material/components/base.hh>
#include <dumux/material/components/liquid.hh>

namespace Dumux
{
/*!
 * \ingroup Components
 * \brief A fictitious component to be implemented in exercise-fluidsystem 2.1.
 *
 * \tparam Scalar The type used for scalar values.
 */
template <class Scalar>
class MyIncompressibleComponent
: public Components::Base<Scalar, MyIncompressibleComponent<Scalar> >
, public Components::Liquid<Scalar, MyIncompressibleComponent<Scalar> >
{
public:
    /*!
     * \brief A human readable name for MyIncompressibleComponent.
     */
    static std::string name()
    { return "MyIncompressibleComponent"; }

    /*!
     * \brief Returns true if the liquid phase is assumed to be compressible.
     */
    static constexpr bool liquidIsCompressible()
    { return false; }

    /*!
     * \brief The molar mass in \f$\mathrm{[kg/mol]}\f$ of the component.
     */
    static Scalar molarMass()
    {
        // TODO: dumux-course-task 2.1:
        // Implement the methods for the component data given in the exercise description.
        // Replace the line below by a meaningful return statement.
        DUNE_THROW(Dune::NotImplemented, "Todo: implement molarMass()");
    }

    /*!
     * \brief The density \f$\mathrm{[kg/m^3]}\f$ of the liquid component at a given pressure in
     *          \f$\mathrm{[Pa]}\f$ and temperature in \f$\mathrm{[K]}\f$.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     */
    static Scalar liquidDensity(Scalar temperature, Scalar pressure)
    {
        // TODO: dumux-course-task 2.1:
        // Implement the methods for the component data given in the exercise description.
        // Replace the line below by a meaningful return statement.
        DUNE_THROW(Dune::NotImplemented, "Todo: implement liquidDensity()");
    }

    /*!
     * \brief The molar density of MyIncompressibleComponent in \f$\mathrm{[mol/m^3]}\f$ at a given pressure and temperature.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     *
     */
    static Scalar liquidMolarDensity(Scalar temperature, Scalar pressure)
    {
        // compute molar density by dividing the density by the molar mass
        return liquidDensity(temperature, pressure)/molarMass();
    }

    /*!
     * \brief The dynamic liquid viscosity \f$\mathrm{[Pa*s]}\f$ of the pure component.
     *
     * \param temperature temperature of component in \f$\mathrm{[K]}\f$
     * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
     */
    static Scalar liquidViscosity(Scalar temperature, Scalar pressure)
    {
        // TODO: dumux-course-task 2.1:
        // Implement the methods for the component data given in the exercise description.
        // Replace the line below by a meaningful return statement.
        DUNE_THROW(Dune::NotImplemented, "Todo: implement liquidViscosity()");
    }
};

} // end namespace

#endif
