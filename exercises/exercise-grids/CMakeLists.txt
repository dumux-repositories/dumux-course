# SPDX-FileCopyrightText: Copyright © DuMux-Course contributors, see AUTHORS.md in root folder
# SPDX-License-Identifier: GPL-3.0-or-later

# the grid exercise simulation program
dumux_add_test(NAME exercise_grids
               SOURCES main.cc)

# add a symlink for each input file
add_input_file_links()

# add a symlink for the grids folder
dune_symlink_to_source_files(FILES grids)
