The DuMux Course contributors are

| Year                | Name                    |
|---------------------|-------------------------|
| 2018                | Sina Ackermann          |
| 2023-2024           | Ivan Buntic             |
| 2018-2019           | Beatrix Becker          |
| 2019,2023           | Holger Class            |
| 2018-2023           | Edward 'Ned' Coltman    |
| 2018,2020           | Simon Emmert            |
| 2018                | Thomas Fetzer           |
| 2018-2021,2023-2024 | Bernd Flemisch          |
| 2023                | Tufan Ghosh             |
| 2018,2022-2023      | Dennis Gläser           |
| 2018-2019,2022      | Katharina Heck          |
| 2019-2024           | Johannes Hommel         |
| 2023-2024           | Leon Keim               |
| 2020,2022-2024      | Mathis Kelm             |
| 2023-2024           | Stefanie Kiemle         |
| 2018-2024           | Timo Koch               |
| 2023-2024           | Anna Mareike Kostelecky |
| 2018-2019           | Melanie Lipp            |
| 2024                | Stefan Meggendorfer     |
| 2019                | Maren Mittelbach        |
| 2020                | Farid Mohammadi         |
| 2023-2024           | Hamza Oukili            |
| 2018-2019,2023-2024 | Martin Schneider        |
| 2018-2019,2021      | Theresa Schollenberger  |
| 2022                | Axel Schumacher         |
| 2018-2020           | Gabriele Seitz          |
| 2020                | Maziar Veyskarami       |
| 2020,2022-2023      | Yue Wang                |
| 2018-2020           | Kilian Weishaupt        |
| 2018                | Felix Weinhardt         |
| 2018                | David Werner            |
| 2020,2023-2024      | Roman Winter            |
| 2021                | Hanchuan Wu             |
