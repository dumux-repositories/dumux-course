---
title: DuMuX for modelling fractures in porous media
---

# What are fractures?

## Joints

<img src="img/fractures_joints.jpeg" width="600"/><br>
<small>Credit: [flickr.com/photos/dietmardownunder/44733263071](https://www.flickr.com/photos/dietmardownunder/44733263071), licensed under [CC-BY-2.0](https://creativecommons.org/licenses/by/2.0/).</small>

## Faults

<img src="img/fractures_fault.jpeg" width="600"/><br>
<small>Credit: [Wikipedia](https://upload.wikimedia.org/wikipedia/commons/4/48/Piqiang_Fault%2C_China_detail.jpg).</small>


# Why are fractures important?

## Example applications I

Hydraulic fracturing

<img src="img/fractures_fracking.jpeg" width="500"/><br>
<small>Credit: [Wikipedia](https://de.wikipedia.org/wiki/Hydraulic_Fracturing#/media/Datei:Hydraulic_Fracturing-Related_Activities.jpg).</small>


## Example applications II

Geothermal energy production

<img src="img/fractures_geothermal.jpeg" width="500"/><br>
<small>Credit: [Wikipedia](https://en.wikipedia.org/wiki/Geothermal_energy#/media/File:NesjavellirPowerPlant_edit2.jpg).</small>

## Hydraulic effects

(Results from the DuMuX fracture exercise)
<img src=img/fractures_pressure_withfracs.png height=380>
<img src=img/fractures_pressure_nofracs_legend.png height=380>

## Capillary effects

(Results from the DuMuX fracture exercise)
<img src=img/fractures_saturation_withfracs.png width="40%">
<img src=img/fractures_saturation_nofracs.png width="40%">


# Model concept

## Discretization

<img src=img/fractures_grid_equi.jpg width="32%">
<img src=img/fractures_grid_ld.png width="32%">
<img src=img/fractures_grid_nonmatch.png width="32%">

## Problem Abstraction

<img src=img/fractures_modeldomain.png width="70%">

## Problem Formulation
:::::: {.columns}
::: {.column width=33%}
<div style="display: flex; align-items: center; height: 100%;">
  <img src=img/fractures_modeldomain.png>
</div>
:::
::: {.column width=67%}
$$
\small
\begin{aligned}
    \mathbf{u}_i &= - \mathbf{K}_i \nabla p_i, \\
    \nabla \cdot \mathbf{u}_i &= q_i, &&\mathrm{in} \, \Omega_i, \\
    \mathbf{U}_f &=- a \mathbf{K}_{f, \tau} \nabla_\tau P_f, \\
    \nabla_\tau \cdot\mathbf{U}_f &= q_f + \left( \mathbf{u}_1 \cdot \mathbf{n}_1 + \mathbf{u}_2 \cdot \mathbf{n}_2 \right), &&\mathrm{in} \, \Omega_f, \\
    \mathbf{u}_i \cdot \mathbf{n}_i &= - \frac{2 k_\eta}{a} ( P_f - p_i ), &&\mathrm{in} \, \gamma_N, \\
    P_f &= p_i, &&\mathrm{in} \, \gamma_D.
\end{aligned}
$$
:::
::::::

# Fracture exercise

## Buoyancy-driven gas migration

<img src=img/fractures_exercise_solution.gif width="50%">


## Tasks

- Change boundary conditions
- Change fracture properties
- Use different coupling conditions
- Make use of domain markers
