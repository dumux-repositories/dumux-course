---
title: Python bindings
---

# DuMu^x^ Python bindings

## Using DuMu^x^ from Python

* Based on just-in-time (JIT) compilation of Python binding code
* Experimental feature: only a small subset of functionality is available
* See [Python test apps](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux/-/tree/master/test/python)

## Configuration

* Enabled and built by default!
* `dune-common` configures a [Python virtual environment](https://docs.python.org/3/library/venv.html) (`dune-common/build-cmake/dune-env`)
* Automatically created and set up when running `dunecontrol`
* On Ubuntu systems you need to install `python3-venv` (e.g. `sudo apt install python3-venv`) for `venv` to work properly.
* To activate:
  ```sh
  source dune-common/build-cmake/dune-env/bin/activate
  ```

## Configuration

For more control, follow

```
./dune-common/bin/dunecontrol bexec rm -r CMakeFiles CMakeCache.txt
python3 -m venv venv
source venv/bin/activate
./dune-common/bin/dunecontrol --opts=dumux/cmake.opts configure
./dune-common/bin/dunecontrol --opts=dumux/cmake.opts make -j
```

which installs Python bindings into the activated virtual environment.

## Example Python code

* See [dumux/test/python/test_1p.py](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux/-/blob/master/test/python/test_1p.py)
* With virtual environment activated, run

```
python3 test_1p.py
```

* First time takes very long, due to a lot of JIT
* Second run is much faster

## Small exercise

* Increase the number of grid cells to $100 \times 100$
* Add a constant source term of $10.0$
* Note that you don't have to recompile
* Visualization: `pip install pyvista` and append

```py
import pyvista as pv
mesh = pv.read("test_1p_box_numeric-00000.vtu")
mesh.plot(show_edges=True, full_screen=True)
```

## Further resources

* [DuMu^x^ documentation](https://dumux.org/docs/doxygen/master/python-bindings.html)
