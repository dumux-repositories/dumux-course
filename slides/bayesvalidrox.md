---
title: BayesValidRox </br><small>Surrogate-assisted Bayesian validation of computational models</small>
---

# SFB Project Area D: </br><small>Development and realization of uncertainty-aware validation benchmarks</small>

## Motivation

Compare models against data and each other

## Motivation

<img src="img/bayesvalidrox_balance.png">

## Motivation

<img src="img/bayesvalidrox_missionstatement.png"/></br>

# BayesValidRox

## BayesValidRox

<img src="img/bayesvalidrox_contentworkflow.png"/>

## Applications

<img src="img/bayesvalidrox_example_results.png"/>
<small><small>Mohammadi, Farid, et al. "A surrogate-assisted uncertainty-aware Bayesian validation framework and its application to coupling free flow and porous-medium flow." Computational Geosciences 27.4 (2023): 663-686.</small></small>

# Examples and more

## Want to learn more?
[Analytical example](https://git.iws.uni-stuttgart.de/inversemodeling/bayesvalidrox/-/tree/examples/DuMuXcourse2024/examples/analytical-function)  
[Model comparison](https://git.iws.uni-stuttgart.de/inversemodeling/bayesvalidrox/-/tree/examples/DuMuXcourse2024/examples/model-comparison)  
[Example interface DuMuX](https://git.iws.uni-stuttgart.de/inversemodeling/bayesvalidrox/-/tree/examples/DuMuXcourse2024/examples/model_interfaces)

## Find us at

<img src="img/bayesvalidrox_codewebsite.png"/>
