---
title: The DuMu^x^ Material System
---

# Challenge

## Flow in porous media

* Highly heterogeneous distribution of parameters and complex nonlinear material laws
* Strong interconnection of properties $\rightarrow$ difficult to achieve __modularity__

$\rightarrow$ this also applies to other complex nonlinear equation systems.

# How to achieve modularity?

## How to achieve modularity?

* User-defined parameters and functional relationships:
    - Components / Fluid system / Binary coefficients
    - Solid system
    - Fluid-matrix interactions
    - Chemistry
* State representations and solvers:
    - Fluid states (data)
    - Solid states (data)
    - Constraint solvers / "flash" (algorithm)

## How to achieve modularity?

There is a library of `Components`,
`FluidSystems`, `BinaryCoefficients`
available in DuMu^x^. Can be used as is or can
be a good start for a user implementation.

More resources can be found [in the code documentation](https://dumux.org/docs/doxygen/master/group___material.html).

# Components

## Components

* Define **thermodynamic relations** (e.g. molar mass, vapor pressure, density) of a **single chemical species** or a fixed mixture of species
* Provide a convenient way to access these quantities

## Example implementations

* _H2O_ : pure water properties by IAPWS-97
* _SimpleH2O_ : a simple water implementation at standard conditions
* _Brine_ : water with a given salt concentration
* Many more, see [DuMu^x^ components docs](https://dumux.org/docs/doxygen/master/namespace_dumux_1_1_components.html)

## Example interfaces

```cpp
static Scalar gasDensity(Scalar temperature, Scalar pressure)
{
    // Assume an ideal gas
    return IdealGas::density(molarMass(), temperature, pressure);
}
```
<span style="font-size: 0.4em; position: relative; top: -38px; color: gray;">File: `dumux/dumux/material/components/air.hh`</span>

## Example interfaces

```cpp
static Scalar gasHeatCapacity(Scalar T, Scalar pressure)
{
    // apply Shomate method
    const auto cp = shomateMethod.heatCapacity(T); // J/(mol K)
    return cp / molarMass(); // J/(kg K)
}

```
<span style="font-size: 0.4em; position: relative; top: -38px; color: gray;">File: `dumux/dumux/material/components/ch4.hh`</span>

# Fluid systems

## Fluid systems

Express the **thermodynamic relations between fluid quantities** (e.g. calculation of density or viscosity based on composition; fugacity coefficient based on temperature and pressure...)

<img src="img/fluidsystem.png" width="400"/>


## Example implementations

* _TwoPImmiscible_ : two immiscible fluid phases
* _H2OAir_ : gas and liquid phase with components water and air
* Many more, see [DuMu^x^ fluid systems docs](https://dumux.org/docs/doxygen/master/namespace_dumux_1_1_fluid_systems.html)

## Example interface

```cpp
template <class FluidState>
static Scalar heatCapacity(const FluidState& fluidState, int phaseIdx)
{
    const Scalar temperature = fluidState.temperature(phaseIdx);
    const Scalar pressure = fluidState.pressure(phaseIdx);
    if (phaseIdx == liquidPhaseIdx)
        return H2O::liquidHeatCapacity(temperature, pressure); // neglect air
    else if (phaseIdx == gasPhaseIdx)
        return Air::gasHeatCapacity(temperature, pressure)
                   * fluidState.moleFraction(gasPhaseIdx, AirIdx)
               + H2O::gasHeatCapacity(temperature, pressure)
                   * fluidState.moleFraction(gasPhaseIdx, H2OIdx);
    ...
}
```
<span style="font-size: 0.4em; position: relative; top: -38px; color: gray;">File: `dumux/dumux/material/fluidsystems/brine.hh`</span>

# Binary coefficients

## Binary coefficients

  **Contain** data and equations required for binary mixtures, for instance, binary diffusion coefficients or coefficients needed for constitutive relationships (e.g. Henry coefficient)

<img src="img/binarycoefficients.png" width="500"/>


## Example implementations

* _H2O_Air_ : Henry coefficient, gas diffusion coefficient, liquid diffusion coefficent for water and air
* More, see [DuMu^x^ binary coefficients docs](https://dumux.org/docs/doxygen/master/namespace_dumux_1_1_binary_coeff.html)

## Example interface

```cpp
template <class Scalar>
static Scalar gasDiffCoeff(Scalar temperature, Scalar pressure)
{
    // _H2O_Air_
    constexpr Scalar theta = 1.8;
    constexpr Scalar Daw = 2.13e-5;  /* reference value */
    constexpr Scalar pg0 = 1.e5;     /* reference pressure */
    constexpr Scalar T0 = 273.15;    /* reference temperature */
    using std::pow;
    return Daw*(pg0/pressure)*pow((temperature/T0), theta);
}
```
<span style="font-size: 0.4em; position: relative; top: -38px; color: gray;">File: `dumux/dumux/material/binarycoefficients/h2o_air.hh`</span>

# Solid systems

## Solid systems

Express the **thermodynamic properties of the solid matrix** (e.g. calculation of the solid density and solid heat capacity based on the composition)

<img src="img/solidsystem.png" width="400"/>

##

_Specifying a solid system is only necessary if you work with a non-isothermal or mineralization model. If no solid system is specified in the problem file, the default is the inert solid phase with the constant component. For the constant component you can set properties in the input file._


## Implementations

* _OneCSolid_ : inert solid matrix of one solid component (e.g. granite)
* _CompositionalSolidPhase_ : composed solid matrix of inert or reactive components (e.g. NaCl and granite)


# Fluid-matrix interactions

## Fluid-matrix interactions

* Description of the **interaction of the fluid phases with the porous medium** (e.g. capillary pressure-saturation and relative permeability relationships)
* Through modular adapters, regularization schemes can be imposed for extreme values

<img src="img/fluidmatrixinteractions.png" width="250"/>


## Example implementations

* Capillary pressure-saturation relation after _Van Genuchten_
* Capillary pressure-saturation relation after _Brooks and Corey_
* Effective diffusivity after _Millington and Quirk_

## Van Genuchten

$\begin{equation}
p_c = \frac{1}{\alpha}\left(S_e^{-1/m} -1\right)^{1/n}
\end{equation}$

<img src="img/pc-Sw_VanGenuchten.png" width="500"/> 
<!-- plot created with VanGenuchtenN=9 and VanGenuchtenAlpha=0.0008 -->

$\rightarrow$ the empirical parameters $\alpha$ and $n$ have to be specified

## Brooks-Corey

$\begin{equation}
p_c = p_d S_e^{-1/\lambda}
\end{equation}$

<img src="img/pc-Sw_BrooksCorey.png" width="500"/>
<!-- plot created with BrooksCoreyPcEntry=1e4 and BrooksCoreyLambda=2 -->

$\rightarrow$ the empirical parameters $p_d$ and $\lambda$ have to be specified


# Fluid states

## Fluid states

* **Store** the complete thermodynamic configuration of a system at a given spatial and temporal position (e.g. saturation, mole fraction, enthalpy)
* **Provide access** methods to all thermodynamic quantities (e.g. saturation, mole fraction, enthalpy)

<img src="img/fluidstate.png" width="800"/>


## Example implementations

* _ImmiscibleFluidState_ : assumes immiscibility of the fluid phases. Phase compositions and fugacity coefficients do not need to be stored explicitly.
* _CompositionalFluidState_ : assumes thermodynamic equilibrium, only a single temperature needs to be stored.

# Solid states

## Solid states

* **Store** the complete solid configuration of a system at a given spatial and temporal position (e.g. solid volume fractions, solid heat capacity)
* **Provide** access methods to all solid quantities (e.g. porosity, density, temperature)

<img src="img/solidstate.png" width="800"/>

## Example implementations

* _InertSolidState_ : assumes an inert solid phase. Solid volume fractions do not change. This is the **default**.
* _CompositionalSolidState_ : assumes a solid matrix composed out of multiple components. The volume fractions can change and properties such as heat capacity are adapted.

# Constraint Solvers

## Constraint solvers

**Connect** the thermodynamic relations expressed by fluid systems with the thermodynamic quantities stored by fluid states (e.g. mole fraction, density)

<img src="img/constraintsolver.png" width="800"/>


## Example implementation

_CompositionFromFugacities_ : takes all component fugacities, the temperature and pressure of a phase as input and calculates the phase composition


# Example: From components to fluid system

## Components $\rightarrow$ fluid system

<img src="img/component-fluidsystem.png" width="500"/>

## Example: 2 phases, miscible

* Components: _H2O_, _Air_
* Fluid system: _TwoPTwoC_

## Relevant headers

```cpp
// Predefined fluid system for components water and air
#include <dumux/material/fluidsystems/h2oair.hh>

// H2OAir allows to customize the water phase. Here, we want
// to use tabulated H2O for fast evaluation of the properties.
#include <dumux/material/components/h2o.hh>
#include <dumux/material/components/tabulatedcomponent.hh>
```

## Setting the `FluidSystem` property

```cpp
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::DrainageProblem>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Liquid = Components::TabulatedComponent<Components::H2O<Scalar>>;
    using Policy = FluidSystems::H2OAirDefaultPolicy<
        /*fastButSimplifiedRelations=*/true
    >;
public:
    using type = FluidSystems::H2OAir<
        Scalar, Liquid, Policy, /*useKelvinVapourPressure*/true
    >;
};
```
<span style="font-size: 0.4em; position: relative; top: -38px; color: gray;">File: `dumux/test/porenetwork/2pnc/properties.hh`</span>


# Example: From components to solid system

## Example: 2 phases, miscible

* Components: _CaO_, _CaO2H2_ (slaked lime)
* Solid system: _OnePNCMin_

## Specify solid system in properties file:

```cpp
// The solid system
template<class TypeTag>
struct SolidSystem<TypeTag, TTag::ThermoChem>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using ComponentOne = Components::ModifiedCaO<Scalar>;
    using ComponentTwo = Components::CaO2H2<Scalar>;
    using type = SolidSystems::CompositionalSolidPhase<
        Scalar, ComponentOne, ComponentTwo
    >;
};
```
<span style="font-size: 0.4em; position: relative; top: -38px; color: gray;">File: `dumux/test/porousmediumflow/1pncmin/nonisothermal/properties.hh`</span>

# Exercise

## Tasks:

1. Get familiar with the code
2. 2p model: Implement a new component (incompressible and compressible)
3. 2p2c model: Implement a new fluid system
4. Change wettability of the porous medium
5. Advanced: Use van Genuchten relationship with parameters: $\alpha = 0.0037$ and $\alpha_\mathrm{lense} = 0.00045$, $n = 4.7$ and $n_\mathrm{lense} = 7.3$

## First step:

Go to [Fluidsystem exercise](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-course/-/tree/master/exercises/exercise-fluidsystem#exercise-fluidsystem-dumux-course)
