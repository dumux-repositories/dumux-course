---
title: Runtime Parameters
---

# Runtime Parameters

## Runtime Parameters

* Configuration at runtime (avoid recompiling)
* We will look at:
  - Parameters in input files
  - Command line parameters
  - Default parameters

## Initialize Parameter Tree

* Parameters are initialized by `Parameters::init(...)`
* `Parameters::init(...)` constructs a parameter tree [singleton](https://en.wikipedia.org/wiki/Singleton_pattern) from which parameters can be obtained
* The parameter tree stores strings (key-value) and parameter trees (subtrees, groups)

## Initialize Parameter Tree

```cpp
#include <dumux/common/parameters.hh>
int main(int argc, char** argv)
{
    Dumux::Parameters::init(argc, argv);
    return 0;
}
```
<span style="font-size: 0.4em; position: relative; top: -38px; color: gray;">File: `dumux-course/exercises/exercise-basic/2pmain.cc`</span>

## Initialize Parameter Tree

Explicitly specify parameter file (default: `params.input`)
```cpp
#include <dumux/common/parameters.hh>
int main(int argc, char** argv)
{
    Dumux::Parameters::init(argc, argv, "params.input");
    return 0;
}
```
<span style="font-size: 0.4em; position: relative; top: -38px; color: gray;">File: `dumux/test/common/parameters/test_loggingparametertree.cc`</span>

## Initialize Parameter Tree

Explicitly specify default parameters
```cpp
#include <dune/common/parametertree.hh>
#include <dumux/common/parameters.hh>
int main(int argc, char** argv)
{
    Dumux::Parameters::init(argc, argv, [](Dune::ParameterTree& p){
        p["key"] = "value";
        p["group.key"] = "value2";
        ...
    });
    return 0;
}
```
<span style="font-size: 0.4em; position: relative; top: -38px; color: gray;">File: `dumux/test/linear/test_parallel_amg_smoothers.cc`</span>

## Initialize Parameter Tree

Without reading command-line arguments
```cpp
#include <dumux/common/parameters.hh>
int main(int argc, char** argv)
{
    Dumux::Parameters::init("params.input");
    return 0;
}
```
<span style="font-size: 0.4em; position: relative; top: -38px; color: gray;">File: `dumux/dumux/common/parameters.cc`</span>

## Reading Runtime Parameters

* Read parameters from the parameter tree with `getParam` (converts `std::string` to requested type)
  ```cpp
  paramname_ = getParam<TYPE>("GROUPNAME.PARAMNAME", default);
  ```
* Examples:
  ```cpp
  bool enableGravity = getParam<bool>("Problem.EnableGravity", true);
  ```
  ```cpp
  auto upperRight
    = getParam<Dune::FieldVector<double, 3>>("FreeFlow.Grid.UpperRight");
  ```
* Signature without default: throws `ParameterException` if parameter doesn't exist.

## Reading Runtime Parameters

* `getParamFromGroup` variant traverses the parameter tree
  ```cpp
  paramname_ = getParamFromGroup<TYPE>("GROUPNAME", "PARAMNAME", default);
  ```
* Example
  ```cpp
  bool enableGravity = getParamFromGroup<bool>("FreeFlow", "Problem.Gravity");
  ```
  - first looks for `FreeFlow.Problem.Gravity`
  - then looks for `Problem.Gravity`
* Useful when configuring multiple simulation components or multi-domain problem
via the single parameter tree


## Reading Runtime Parameters

* Remark: Reading parameters is a slow operation
* Read parameters in constructors of high-level classes
* Never read parameters in functions called for all elements

## Checking for Runtime Parameters

* Check: Does parameter exist in the parameter tree?
  ```cpp
  if (hasParam("GROUPNAME.PARAMNAME"))
      // do something with parameter
  ```
  <span style="font-size: 0.4em; position: relative; top: -38px; color: gray;">File: `dumux/examples/biomineralization/main.cc`</span>
* With group prefix lookup
  ```cpp
  if (hasParamInGroup("GROUPNAME","PARAMNAME"))
  ```
  <span style="font-size: 0.4em; position: relative; top: -38px; color: gray;">File: `dumux/dumux/freeflow/rans/problem.hh`</span>

## Parameter tree logs usage

```cpp
#include <dumux/common/parameters.hh>
int main(int argc, char** argv)
{
    Dumux::Parameters::init(argc, argv);
    ...
    Dumux::Parameters::print(); // print report
    return 0;
}
```
<span style="font-size: 0.4em; position: relative; top: -38px; color: gray;">File: `dumux-course/exercises/exercise-basic/2pmain.cc`</span>

Reports unused parameters. Great for detecting typos in
configuration file.

## Parameter input file

Dune INI syntax (`[Group]` and `Key = Value` pairs)

`params.input`

```cpp
[Grid]
LowerLeft = 0 0
UpperRight = 60 40
Cells = 24 16

[Problem]
Name = test

[FreeFlow.Problem]
Name = test_ff
```
<span style="font-size: 0.4em; position: relative; top: -38px; color: gray;">File: `dumux-course/exercises/exercise-basic/params.input`</span>

## Command-line arguments

```sh
./executable params.input -Key Value -Key2 Value2 -Key3 "a b c"
```

Examples:
```sh
./executable -Grid.Refinement 2
./executable params_alt.input -Grid.Refinement 2
./executable -Grid.Refinement 2 -Grid.Cells "10 10"
```

## Parameter precedence

1. Command-line arguments overwrite
2. Input file arguments overwrite
3. User-default arguments overwrite
4. Global defaults

## Parameter list

An overview of all available parameters can be found in the [doxygen documentation](https://dumux.org/docs/doxygen/master/parameterlist_8txt.html).

<img src=img/parameterlist.png>

# Exercise
## Exercise

* Go to [Runtime parameter exercise](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-course/tree/master/exercises/exercise-runtimeparams#exercise-runtime-parameters-dumux-course)
