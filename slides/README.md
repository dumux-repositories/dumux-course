# DuMuX course slides

The slides are automatically built and deployed in the CI and can be viewed at

:tv: [DuMuX course homepage](https://pages.iws.uni-stuttgart.de/dumux-repositories/dumux-course/index.html)

## Slide overview and direct links

The link above will lead to a single slide deck from which all presentation in the course can be reached.
To reach a specific lecture directly, we have compiled some direct links:

* [Introduction to DuMuX](https://pages.iws.uni-stuttgart.de/dumux-repositories/dumux-course/intro.html#/title-slide)
* Part I: Basics ([overview](https://pages.iws.uni-stuttgart.de/dumux-repositories/dumux-course/index.html#/section))
    - [A first test application](https://pages.iws.uni-stuttgart.de/dumux-repositories/dumux-course/problem.html#/title-slide)
    - [Runtime parameters](https://pages.iws.uni-stuttgart.de/dumux-repositories/dumux-course/params.html#/title-slide)
    - [Grid managers](https://pages.iws.uni-stuttgart.de/dumux-repositories/dumux-course/grid.html#/title-slide)
    - [Python bindings](https://pages.iws.uni-stuttgart.de/dumux-repositories/dumux-course/python.html#/title-slide)

* Part II: Customization, working with the code ([overview](https://pages.iws.uni-stuttgart.de/dumux-repositories/dumux-course/index.html#/section-1))
    - [Properties](https://pages.iws.uni-stuttgart.de/dumux-repositories/dumux-course/properties.html#/title-slide)
    - [Constitutive laws and material system](https://pages.iws.uni-stuttgart.de/dumux-repositories/dumux-course/materialsystem.html#/title-slide)
    - [Creating a new Dune module](https://pages.iws.uni-stuttgart.de/dumux-repositories/dumux-course/dunemodule.html#/title-slide)
    - [Implementing a new model PDE](https://pages.iws.uni-stuttgart.de/dumux-repositories/dumux-course/model.html#/title-slide)

* Part III: Advanced exercises, multidomain models ([overview](https://pages.iws.uni-stuttgart.de/dumux-repositories/dumux-course/index.html#/section-2))
    - [Introduction to Multidomain simulations](https://pages.iws.uni-stuttgart.de/dumux-repositories/dumux-course/multidomain.html#/title-slide)
    - [Coupled free-flow, porous media systems](https://pages.iws.uni-stuttgart.de/dumux-repositories/dumux-course/coupled_ff-pm.html#/title-slide)
    - [Discrete fracture modeling](https://pages.iws.uni-stuttgart.de/dumux-repositories/dumux-course/fractures.html#/title-slide)
    - [Biomineralization modeling](https://pages.iws.uni-stuttgart.de/dumux-repositories/dumux-course/biomin.html#/title-slide)
    - [Uncertain model validation](https://pages.iws.uni-stuttgart.de/dumux-repositories/dumux-course/bayesvalidrox.html#/title-slide)

## Developing the slides

In order to add new slides, simply add your `.md` file in this folder, and add
an entry to `index.md` such that your slides are linked from the landing page.
__Important__: In `index.md`, link to your slides using `./YOUR_FILENAME.html`, and make sure to put your link at the right position in the list.

To build one slide set locally, you need to have a current version of pandoc installed, see https://pandoc.org/installing.html. If your slide set is `my_slide_set.md`, you can translate it via
```bash
pandoc -t revealjs -s --mathjax -o test.html my_slide_set.md -V revealjs-url=https://unpkg.com/reveal.js/ -V theme=serif --include-in-header=dumux_course_header.html
```
and point your browser to the resulting `test.html`.
