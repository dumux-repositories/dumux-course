---
title: Making my own Dune-Module
---

# Custom Dune-Module

## My own workspace?

We all want to implement our own models and create our own test cases.

But where?

## The Dune/Dumux Suite
<img src=img/dependenciesCore.png width="100%">

## The Dune/Dumux Suite
<img src=img/dependenciesOptional.png width="100%">

## The Dune/Dumux Suite
<img src=img/dependenciesMyModule.png width="100%">

## How are these modules linked?
Modules are linked via the `dune.module` file, which states dependencies and suggests optional modules.

```ini
Module: dumux-appl
Version: 1.0
Maintainer: user@dumux.de
# Required build dependencies
Depends: dumux dune-alugrid dune-foamgrid dune-uggrid
# Optional build dependencies
#Suggests:
```
<span style="font-size: 0.4em; position: relative; top: -38px; color: gray;">File: `dumux/dune.module`</span>

## Creating a new dune module
Start the script `duneproject` to initiate creating a new module:

```bash
./dune-common/bin/duneproject
```

## How are these modules built?
Configuring and compiling the stack of modules is done via the script `dunecontrol`:

```bash
./dune-common/bin/dunecontrol --opts=dumux/cmake.opts all
```

## Adding test cases to an `appl` folder

- Adjust the
    - source (.cc) file,
    - input parameter file,
    - problem file,
    - properties file,
    - (spatialparams file).
- Adjust the `CMakeLists.txt` file.
- Reconfigure your module.

## Pushing an existing folder to an empty git repository

```bash
cd my-module-folder
git config --global user.name "FIRST_NAME LAST_NAME"
git config --global user.email "YOUR_EMAIL_ADDRESS"
git init --initial-branch=main
git remote add origin https://git.iws.uni-stuttgart.de/Namespace/my-module.git
git add .
git commit -m "Initial commit"
git push -u origin main
```

## [Exercises](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-course/tree/master/exercises/exercise-dunemodule):

- Create a new module with the duneproject script
- Run dune control
- Create an appl folder
- Upload to a git repository
