---
title: Grids
---

# Grids

## Grid implementations I

Implementations of the Dune grid interface

* **YASPGrid** (structured, n-dim, parallel tensorproduct grid)
* **UGGrid** (2D/3D, unstructured, parallel, multi-geometry)
* **ALUGrid** (2D/3D, unstructured, parallel, simplex/cube)
* **FOAMGrid** (1D-2D, Dynamic, Multi-dimension/Network Problems)

## Grid implementations II

Implementations of the Dune grid interface

* **OPMGrid** (Cornerpoint grids)
* **OneDGrid** (1D adaptive grid)
* **SPGrid** (structured, n-dim, parallel tensorproduct grid)
* **SubGrid** (meta-grid, turn subdomain into grid)
* **GeometryGrid** (meta-grid, grid by coordinate transformation)

[Overview of grid features](https://www.dune-project.org/doc/grids/)

## Create grid

* Set grid types via properties specialization:
  ```cpp
  #include <dune/grid/yaspgrid.hh>
  ...
  template<class TypeTag>
  struct Grid<TypeTag, TTag::Injection2p>{
      using type = Dune::YaspGrid<2>;
  };
  ```
  <span style="font-size: 0.4em; position: relative; top: -38px; color: gray;">File: `dumux-course/exercises/exercise-grids/properties.hh`</span>

* Include the matching grid manager header files in main file and create the grid via a grid manager instance
  ```cpp
  #include <dumux/io/grid/gridmanager_yasp.hh>
  ...
      using Grid = GetPropType<TypeTag, Properties::Grid>;
      GridManager<Grid> gridManager;
      gridManager.init();
  ```
  <span style="font-size: 0.4em; position: relative; top: -38px; color: gray;">File: `dumux/test/porousmediumflow/2p/incompressible/main.cc`</span>

## Create grid

The grid manager looks for grid information in the runtime parameter tree

## Structured Grid

* Grid from basic parameters
  ```ini
  [Grid]
  LowerLeft = 0 0 # x, y entry (3 entries for 3D)
  UpperRight = 1 1
  Cells = 10 5
  ```
  <span style="font-size: 0.4em; position: relative; top: -38px; color: gray;">File: `dumux/test/porousmediumflow/2p/incompressible/params.input`</span>

<img src=img/params_grid.png width="200">

## Grid from file

* Read in grid from file (specified in input-file) using a relative or absolute path to the grid file
  ```ini
  [Grid]
  File = ./grids/heterogeneousSmall.dgf
  ```
  <span style="font-size: 0.4em; position: relative; top: -38px; color: gray;">File: `dumux/test/io/gridmanager/test_gridmanager_dgf.input`</span>

* Supported grid file formats
  * DGF (Dune Grid Format)
  * Gmsh (MSH format version 2)
  * Cornerpoint grid format (`*.grdecl` file, via opm-grid)

## Constructing grid without grid manager

Consult the documentation of the grid implementation, e.g.
constructing a `Dune::YaspGrid`:

```cpp
constexpr int dim = 2;
using Yasp = Dune::YaspGrid<dim, Dune::EquidistantOffsetCoordinates<
  double, dim>>;
std::array<int, dim> cells; cells.fill(30);
Dune::FieldVector<double, dim> lowerLeft(1.1), upperRight(2.2);
Yasp yasp(lowerLeft, upperRight, cells);
```
<span style="font-size: 0.4em; position: relative; top: -38px; color: gray;">File: `dumux/test/geometry/test_intersectingentity_cartesiangrid.cc`</span>

# Exercise
## Exercise

* Go to [Grid exercise](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-course/tree/master/exercises/exercise-grids#exercise-grids-dumux-course)
