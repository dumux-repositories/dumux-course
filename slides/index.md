---
title: DuMu^x^ course slides
subtitle: Overview
---
**Introduction**

* [Introduction to DuMu^x^](./intro.html)
* [Part I](#section) Building DuMu^x^, configure and run applications
* [Part II](#section-1) Customization, working with the code
* [Part III](#section-2) Advanced exercises, multidomain models

<img src="img/qr-code.svg" width="30%"/>

##

**[Part I]** Building DuMu^x^, configure and run applications

* [Test problem/application](./problem.html)
* [Runtime Parameters](./params.html)
* [Grid](./grid.html)
* [Python bindings](./python.html)

##

**[Part II]** Customization, working with the code

* [Property System](./properties.html)
* [Constitutive models (material)](./materialsystem.html)
* [New Dune module](./dunemodule.html)
* [Implementing a new model in DuMu^x^](./model.html)

##

**[Part III]** Advanced exercises, multidomain models

* [Introduction to Multidomain](./multidomain.html)
* [Free-Flow Porous-Medium Flow Systems](./coupled_ff-pm.html)
* [Discrete Fracture Modeling](./fractures.html)
* [Biomineralization Modeling](./biomin.html)
* [Uncertain Model Validation](./bayesvalidrox.html)

