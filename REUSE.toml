version = 1
SPDX-PackageName = "dumux-course"
SPDX-PackageDownloadLocation = "https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-course"

[[annotations]]
path = "**/**.input"
precedence = "aggregate"
SPDX-FileCopyrightText = "DuMux-Course contributors, see AUTHORS.md in root folder"
SPDX-License-Identifier = "GPL-3.0-or-later"

[[annotations]]
path = "**.md"
precedence = "aggregate"
SPDX-FileCopyrightText = "DuMux-Course contributors, see AUTHORS.md in root folder"
SPDX-License-Identifier = "CC-BY-4.0"

[[annotations]]
path = "**.pdf"
precedence = "aggregate"
SPDX-FileCopyrightText = "DuMux-Course contributors, see AUTHORS.md in root folder"
SPDX-License-Identifier = "CC-BY-4.0"

[[annotations]]
path = ".patches/**"
precedence = "aggregate"
SPDX-FileCopyrightText = "DuMux-Course contributors, see AUTHORS.md in root folder"
SPDX-License-Identifier = "CC0-1.0"

[[annotations]]
path = "slides/img/*"
SPDX-FileCopyrightText = "DuMux-Course contributors, see AUTHORS.md in root folder"
SPDX-License-Identifier = "CC-BY-4.0"

[[annotations]]
path = "exercises/**"
SPDX-FileCopyrightText = "DuMux-Course contributors, see AUTHORS.md in root folder"
SPDX-License-Identifier = "CC-BY-4.0"

[[annotations]]
path = "slides/img/03_uio_full_logo_no_pos.png"
SPDX-FileCopyrightText = "University of Oslo, 2024"
SPDX-License-Identifier = "CC-BY-ND-4.0"

[[annotations]]
path = "slides/img/baw.png"
SPDX-FileCopyrightText = "Bundesanstalt für Wasserbau, 2024"
SPDX-License-Identifier = "CC-BY-ND-4.0"

[[annotations]]
path = "slides/img/bund_logo_en.svg"
SPDX-FileCopyrightText = "The Federal Government of Germany, 2024"
SPDX-License-Identifier = "CC-BY-ND-4.0"

[[annotations]]
path = "slides/img/dfg.png"
SPDX-FileCopyrightText = "Deutsche Forschungsgemeinschaft, 2024"
SPDX-License-Identifier = "CC-BY-ND-4.0"

[[annotations]]
path = "slides/img/dumux.png"
SPDX-FileCopyrightText = "DuMux, 2024"
SPDX-License-Identifier = "CC-BY-ND-4.0"

[[annotations]]
path = "slides/img/eu-flag190.jpeg"
SPDX-FileCopyrightText = "The Council of Europe, 2024"
SPDX-License-Identifier = "CC-BY-ND-4.0"

[[annotations]]
path = "slides/img/gwk.svg"
SPDX-FileCopyrightText = "Gemeinsame Wissenschaftskonferenz, 2023"
SPDX-License-Identifier = "CC-BY-ND-4.0"

[[annotations]]
path = "slides/img/logo-bawue.png"
SPDX-FileCopyrightText = "State of Baden-Württemberg, 2023"
SPDX-License-Identifier = "CC-BY-ND-4.0"

[[annotations]]
path = "slides/img/nfdi5ing_logo.svg"
SPDX-FileCopyrightText = "Nationale Forschungsdateninfrastruktur, 2023"
SPDX-License-Identifier = "CC-BY-ND-4.0"

[[annotations]]
path = "slides/img/sfb1313.png"
SPDX-FileCopyrightText = "Sonderforschungsbereich 1313, 2023"
SPDX-License-Identifier = "CC-BY-ND-4.0"

[[annotations]]
path = "slides/img/simtech.jpg"
SPDX-FileCopyrightText = "Cluster of Excellence SimTech, 2023"
SPDX-License-Identifier = "CC-BY-ND-4.0"

[[annotations]]
path = "slides/img/unistuttgart_logo.png"
SPDX-FileCopyrightText = "University of Stuttgart, 2023"
SPDX-License-Identifier = "CC-BY-ND-4.0"

[[annotations]]
path = "slides/img/biomin_intro_microbes.png"
SPDX-FileCopyrightText = "James Connolly, Montana State University, 2023"
SPDX-License-Identifier = "CC-BY-4.0"

[[annotations]]
path = "slides/img/biomin_model-*"
SPDX-FileCopyrightText = [
    "Ebigbo et al., WRR, 2012",
    "Johannes Hommel, 2023"
]
SPDX-License-Identifier = "CC-BY-4.0"

[[annotations]]
path = "slides/img/biomin_model-*"
SPDX-FileCopyrightText = [
    "Ebigbo et al., WRR 2012",
    "Johannes Hommel, 2023"
]
SPDX-License-Identifier = "CC-BY-4.0"

[[annotations]]
path = "slides/img/FFPM-SoilWaterEvapField.png"
SPDX-FileCopyrightText = "Shahraeeni et al., 2012"
SPDX-License-Identifier = "CC-BY-4.0"

[[annotations]]
path = "slides/img/FFPM-evapStages.png"
SPDX-FileCopyrightText = "Or et al., 2013"
SPDX-License-Identifier = "CC-BY-4.0"

[[annotations]]
path = [
    "slides/img/FFPM-numericalmodel.png",
    "slides/img/FFPM_evaporation_setup.png",
    "slides/img/FFPM_evaporation.gif"
]
SPDX-FileCopyrightText = "Thomas Fetzer, 2018"
SPDX-License-Identifier = "CC-BY-4.0"

[[annotations]]
path = "slides/img/fractures_joints.jpeg"
SPDX-FileCopyrightText = "Dietmar Down Under, 2018"
SPDX-License-Identifier = "CC-BY-4.0"

[[annotations]]
path = [
    "slides/img/fractures_fault.jpeg",
    "slides/img/fractures_fracking.jpeg",
    "slides/img/fractures_geothermal.jpeg"
]
SPDX-FileCopyrightText = "Wikipedia, 2023"
SPDX-License-Identifier = "CC0-1.0"

[[annotations]]
path = "slides/img/lh2_2021.jpg"
SPDX-FileCopyrightText = "LH2, University of Stuttgart"
SPDX-License-Identifier = "LicenseRef-private"
