#!/bin/bash
# SPDX-FileCopyrightText: Copyright © DuMux-Course contributors, see AUTHORS.md in root folder
# SPDX-License-Identifier: GPL-3.0-or-later


set -eu
# Redirect all output and errors to log.txt
exec &> >(tee "installcourse.log")
exec 2>&1

echo "Installing necessary ubuntu-packages."
# Update package list
sudo apt update -y

# Upgrade installed packages (optional)
sudo apt upgrade -y

# Add the multiverse repository
sudo add-apt-repository multiverse -y

# Install the required packages
sudo apt install -y git wget vim build-essential cmake pkg-config libopenmpi-dev openmpi-bin gfortran g++ gcc python3-dev python3-venv libsuitesparse-dev

echo "All necessary ubuntu-packages have been installed successfully."

mkdir -p dumux_course

cd dumux_course

echo "Cloning dumux:"
git clone --branch releases/3.8 --depth=1 https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git

echo "Cloning dumux-course:"
git clone --branch master https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-course.git

for module in common geometry grid localfunctions istl; do
    echo "Cloning dune-$module:"
    git clone --branch releases/2.9 --depth=1 https://gitlab.dune-project.org/core/dune-$module.git
done

for module in alugrid subgrid foamgrid; do
    echo "Cloning dune-$module:"
    git clone --branch releases/2.9 --depth=1 https://gitlab.dune-project.org/extensions/dune-$module.git
done

echo "Finished cloning, building everything"

./dune-common/bin/dunecontrol --opts=./dumux/cmake.opts all
