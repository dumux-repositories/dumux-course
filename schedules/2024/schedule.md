### Wednesday, July 17

| Time        | Title                                  |
|-------------|----------------------------------------|
| 10:00-10:30 | Welcome Coffee                         |
| 10:30-12:30 | Introduction to DuMux                  |
| 12:30-13:30 | Lunch break                            |
| 13:30-15:30 | Setting up a test problem / application|
| 15:30-16:00 | Coffee break                           |
| 16:00-17:00 | Runtime parameters and grids           |
| 17:00-17:30 | Flash presentations                    |

### Thursday, July 18

| Time        | Title                                  |
|-------------|----------------------------------------|
| 09:00-10:30 | The DuMux property system              |
| 10:30-11:00 | Break                                  |
| 11:00-12:30 | Material system                        |
| 12:30-13:30 | Lunch Break                            |
| 13:30-14:30 | Setting up a new Dune module           |
| 14:30-15:30 | Implement a new DuMux model            |
| 15:30-16:00 | Break                                  |
| 16:00-17:00 | Implement a new DuMux model (ctd.)     |
| 17:00-17:30 | Flash presentations                    |
| 18:00-(...) | Dinner                                 |

### Friday, July 19

| Time        | Title                                  |
|-------------|----------------------------------------|
| 09:00-10:00 | The DuMux multidomain framework        |
| 10:00-10:30 | Project areas of the SFB 1313 intro    |
| 10:30-11:00 | Break                                  |
| 11:00-11:30 | Work on participants Dumux problem or exercises related to our group |
| 11:30-12:30 | Work on participants Dumux problem or exercises related to our group |
| 12:30-13:30 | Lunch Break                            |
| 13:30-15:30 | Work on participants Dumux problem or exercises related to our group |
| 15:30-16:00 | Coffee, feedback round and farewell    |
