## Schedule of DuMux Course, April 3-5, 2023 

### Monday, April 3

| Time | Title | 
|------|-------|
| 10:00-10:30 | Welcome coffee |
| 10:30-12:30 | Introduction to DuMux |
| 12:30-13:30 | Lunch break |
| 13:30-15:30 | Setting up a test problem / application |
| 15:30-16:00 | Coffee break |
| 16:00-17:00 | Runtime parameters and grids |
| 17:00-17:30 | Python bindings |
| from 19:00  | Dinner at [Wirtshaus Lautenschlager](https://goo.gl/maps/t2dJSdr9wHQGYWYRA) |

### Tuesday, April 4

| Time | Title |
|------|-------|
| 09:00-10:30 | The DuMux property system |
| 10:30-11:00 | Coffee break |
| 11:00-12:30 | Material system |
| 12:30-13:30 | Lunch break |
| 13:30-14:30 | Setting up a new Dune module |
| 14:30-15:30 | Implementing a new DuMux model |
| 15:30-16:00 | Coffee break |
| 16:00-17:00 | Implementing a new DuMux model (ctd.) |
| 17:00-17:30 | Discussion on participants' wishes |

### Wednesday, April 5

| Time | Title |
|------|-------|
| 09:00-10:00 | The DuMux multidomain framework |
| 10:00-10:30 | Project areas of the SFB 1313 |
| 10:30-11:00 | Coffee break |
| 11:00-11:30 | Project areas of the SFB 1313 (ctd.) |
| 11:30-12:30 | Individualized exercises and group work |
| 12:30-13:30 | Lunch break |
| 13:30-15:30 | Individualized exercises and group work |
| 15:30-16:00 | Feedback round and farewell |

