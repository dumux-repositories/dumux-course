Licensing Information
=====================

Copyright holders
-----------------

| Year       | Name                       |
|------------|----------------------------|
| 2018       | LH2, IWS, University of Stuttgart |

For educational use only. Please [contact us](http://www.hydrosys.uni-stuttgart.de/index.en.php) for license information.
