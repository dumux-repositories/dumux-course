#!/bin/bash
# SPDX-FileCopyrightText: Copyright © DuMux-Course contributors, see AUTHORS.md in root folder
# SPDX-License-Identifier: GPL-3.0-or-later


exerciseFolder=exercises/

# Iterate over all subdirectories
for exercise in $(find $exerciseFolder -maxdepth 1 -type d -name "exercise-*")
do
  #crop path to get exercise name
  exerciseName=${exercise%/} # this removes the trailing slash of the path - % removes smallest suffix matching the given pattern
  exerciseName=${exercise#exercises/} # this remove the leading exercises/ of the path - # removes the smallest prefix matching the given pattern

  #for all exercises, apply the respective patches within the respective exercise folder
  patch -s -p2 -d $exercise < .patches/$exerciseName/$exerciseName.patch

  #for all exercises, apply the compare the created solution via patching and the already existing solution in the solution folder
  diff -ruN $exercise exercises/solution/$exerciseName > $exercise/$exerciseName.patch

  #if the patch file is empty, everything went ok and the stored patch matches the existing exercise and solution. If it was forgotten to update the patch, the created patch file for comparing the solutions will not be empty.
  if [ ! -s $exercise/$exerciseName.patch ]
  then
    echo "The solution for $exerciseName seems to be up to date."
  else
    echo "You forgot to update the exercise or the solution for $exerciseName! Please look at the script `.create_single_patch.sh` and its respective instructions to update the patches which map the exercise state to the solution state. This shell script is a hidden file located in the root folder of dumux-course."
    exit 1
  fi
done
