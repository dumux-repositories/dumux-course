<img src="https://git.iws.uni-stuttgart.de/dumux-repositories/dumux/raw/master/doc/logo/dumux_logo_hires_whitebg.png?inline=false" alt="dumux logo" width="400"/>

# DuMu<sup>x</sup> course material

The material is organized as follows

* __Exercises__: click [here](./exercises/README.md) to go to the exercise description
* __Slides__: click [here](./slides/README.md) to view the slide overview
* __Links__: click [here](./links/README.md) to go to a list of useful links

## Installation of the course

Before undertaking the course, please be sure to check on the expected [prerequisites](prerequisites.md).
To get started with the course, you have two options.
Depending on whether you already have DuMu<sup>x</sup> installed or not, choose the one that fits your needs.
If you plan to use Ubuntu on Windows via WSL, we have a brief [installation documentation](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux/-/wikis/Installation-of-DuMux-inside-the-Windows-Subsystem-for-Linux-(WSL)) for the installation of dumux.

### Install DuMu<sup>x</sup> together with the course

The easiest way to install everything you need is to use the install script `installcourse.sh`.
The script will install all needed ubuntu-packages, clone the required git repositories and build the dumux-course.

It is also recommended to have a look inside the script, for further understanding.

Open a terminal (inside Ubuntu) and use the following commands:

```bash
wget https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-course/-/raw/master/installcourse.sh
bash installcourse.sh
```

If you don't have `wget` and don't want to install it, you can just manually download the
`installcourse.sh` script from the git repository.

In case you encounter an error, have a look at the Troubleshooting section.

### Install the course into your existing DuMu<sup>x</sup> project

If you already have an installation of DuMu<sup>x</sup>,
navigate to your DuMu<sup>x</sup> root directory
and then run the following commands:

```bash
python3 dumux/bin/installexternal.py dumux-course
./dune-common/bin/dunecontrol --opts=./dumux/cmake.opts --only=dumux-course all
```

This will download and then configure the course repository using `dunecontrol`.
If there is an error with the build setup try deleting the CMake cache and reconfigure by running

```bash
./dune-common/bin/dunecontrol bexec rm -r CMakeFiles CMakeCache.txt
./dune-common/bin/dunecontrol --opts=./dumux/cmake.opts all
```

After this, you can start with the [exercises](https://git.iws.uni-stuttgart.de/dumux-repositories/dumux-course/-/tree/master/exercises).

### Requirements

For a successful installation and the execution of all exercises, the following
software packages are needed:

  * gcc >= 9.3
  * cmake >= 3.14
  * git
  * pkg-config
  * paraview (to visualize the results)
  * gnuplot (to plot some curves)
  * wget (to download some config files during the installation)

For some excercises you might need UMFPack. UMFPack can be installed via Suitesparse.
For instance on Ubuntu use `apt-get install libsuitesparse-dev`.

## Troubleshooting

If the `installcourse.sh` script fails, the terminal output can be found in `installcourse.log`.

On a recent mac with silicon chip you may get an error similar to `clang: error: the clang compiler does not support '-march=native'`.
In this case copy configure options file cmake.opts by running

```
cd dumux
cp dumux/cmake.opts mycmake.opts
```

In `mycmake.opts` delete the line containing `-march=native \`. Rerun `dunecontrol` using `mycmake.opts` :

```
./dune-common/bin/dunecontrol bexec rm -r CMakeFiles CMakeCache.txt
./dune-common/bin/dunecontrol --opts=mycmake.opts configure
./dune-common/bin/dunecontrol --opts=mycmake.opts make -j
```
If you cannot resolve installation-problems, write an email with the attached `installcourse.log` file to the dumux mailing list (dumux@listserv.uni-stuttgart.de).

## When was this course last updated?

Last updated for: __DuMu<sup>x</sup> course 2024__.

## Funding acknowledgement

The 2024 update of the dumux-course was financially supported by the German Research Foundation (DFG) within the Collaborative Research Center on Interface-Driven Multi-Field Processes in Porous Media (SFB 1313, Project No. 327154368).
T. Koch acknowledges funding by the European Union's Horizon 2020 research and innovation programme under the Marie Skłodowska-Curie grant agreement No 801133. D. Gläser thanks the Federal Government and the Heads of Government of the
Länder, as well as the Joint Science Conference (GWK), for their funding and support within the
framework of the NFDI4Ing consortium; funded by the German Research Foundation (DFG), Project No. 442146713.

<img src=slides/img/dfg.png width="10%">&nbsp;&nbsp;
<img src=slides/img/sfb1313.png width="10%">&nbsp;&nbsp;
<img src=slides/img/eu-flag190.jpeg width="10%">&nbsp;&nbsp;
<img src=slides/img/bund_logo_en.svg width="13%">&nbsp;&nbsp;
<img src=slides/img/gwk.svg width="13%">&nbsp;&nbsp;
<img src=slides/img/nfdi4ing_logo.svg width="10%">
