#!/bin/bash
# SPDX-FileCopyrightText: Copyright © DuMux-Course contributors, see AUTHORS.md in root folder
# SPDX-License-Identifier: GPL-3.0-or-later


#This is to be executed at the root folder of dumux-course. Via the necessary option -x you name the exercise folder you would like to create a patch on. E.g.:
#./.create_single_patch.sh -x exercise-basic

#read in the parameter -x from the terminal
while getopts "x:" opt
do
  case $opt in
    x)
      exerciseName=$OPTARG
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
  esac
done

#if -x option is empty, throw an error
if [ -z "$exerciseName" ]
then
  echo "ERROR: -x option must be set"
  exit 1
fi

#check if the exercise-folder exists. Should always exist, as we iterate over them
if [ ! -d exercises/$exerciseName ]
then
  echo "exercises/$exerciseName does NOT exist. Terminating."
  exit 1
fi

#check if the respective solution to the exercise exists. If not, check if it is required. In any case where the answer is no, set the flag to false to only generate an empty patch
if [ ! -d exercises/solution/$exerciseName ]
then
  echo "ERROR: exercises/$exerciseName exists but exercises/solution/$exerciseName does NOT exist."

  # Prompt the user for input - is this solution required?
  while true; do
    read -p "Does $exerciseName require a solution (y/n)?" yn
    case $yn in
      [Yy]* ) echo "Please create a solution to the exercise first."; exit;;
      [Nn]* ) echo "Continuing..."; break;;
      * ) echo "Please answer y or n.";;
    esac
  done
fi

diff -ruN exercises/$exerciseName/ exercises/solution/$exerciseName > .patches/$exerciseName/$exerciseName.patch
