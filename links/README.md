# DuMu<sup>x</sup> course useful links

* DuMu<sup>x</sup> documentation: https://dumux.org/docs/doxygen/master/
* DuMu<sup>x</sup> website: http://www.dumux.org/
* DuMu<sup>x</sup> GitLab repository: https://git.iws.uni-stuttgart.de/dumux-repositories/dumux
